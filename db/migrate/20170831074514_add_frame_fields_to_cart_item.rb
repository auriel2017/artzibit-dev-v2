class AddFrameFieldsToCartItem < ActiveRecord::Migration
  def change
    add_column :cart_items, :frame, :boolean
    add_reference :cart_items, :fixed_size_frame_price, index: true
    add_foreign_key :cart_items, :fixed_size_frame_prices
  end
end
