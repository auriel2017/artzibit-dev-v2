class AddCurrentStatusToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :current_status, :string
  end
end
