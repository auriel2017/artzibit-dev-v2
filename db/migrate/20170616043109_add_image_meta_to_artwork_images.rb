class AddImageMetaToArtworkImages < ActiveRecord::Migration
  def change
    add_column :artwork_images, :image_meta, :text
  end
end