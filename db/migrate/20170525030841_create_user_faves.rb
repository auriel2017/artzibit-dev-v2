class CreateUserFaves < ActiveRecord::Migration
  def change
    create_table :user_favorites do |t|
      t.references :user, index: true
      t.references :artwork, index: true
      t.boolean :deleted

      t.timestamps null: false
    end
    add_foreign_key :user_favorites, :users
    add_foreign_key :user_favorites, :artworks
  end
end
