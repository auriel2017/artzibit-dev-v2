class AddFixedFieldsToCartItem < ActiveRecord::Migration
  def change
    add_reference :cart_items, :fixed_size, index: true
    add_foreign_key :cart_items, :fixed_sizes
    add_reference :cart_items, :fixed_size_price, index: true
    add_foreign_key :cart_items, :fixed_size_prices
  end
end
