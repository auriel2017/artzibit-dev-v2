class CreateFrameTypes < ActiveRecord::Migration
  def change
    create_table :frame_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
