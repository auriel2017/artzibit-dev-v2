class AddDefaultValueToCartItemAttributes < ActiveRecord::Migration
  def change
    change_column :cart_items, :installation, :boolean, default: true, allow_null: false
    change_column :cart_items, :frame, :boolean, default: true, allow_null: false
  end
end
