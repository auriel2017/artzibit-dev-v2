class AddColumnsWebRepository < ActiveRecord::Migration
   def change
    add_column :web_repositories, :category, :string
    add_column :web_repositories, :page, :string
  end
end
