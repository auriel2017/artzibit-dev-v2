class CreateUserCheckedIns < ActiveRecord::Migration
  def change
    create_table :user_checked_ins do |t|
      t.references :event_batch, index: true
      t.references :user, index: true
      t.datetime :checked_in_at

      t.timestamps null: false
    end
    add_foreign_key :user_checked_ins, :event_batches
    add_foreign_key :user_checked_ins, :users
  end
end
