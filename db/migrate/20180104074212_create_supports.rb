class CreateSupports < ActiveRecord::Migration
  def change
    create_table :supports do |t|
      t.references :user, index: true
      t.string :contact_no
      t.text :content

      t.timestamps null: false
    end
    add_foreign_key :supports, :users
  end
end
