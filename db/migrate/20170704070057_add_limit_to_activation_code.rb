class AddLimitToActivationCode < ActiveRecord::Migration
  def change
    add_column :activation_codes, :limit, :integer
  end
end
