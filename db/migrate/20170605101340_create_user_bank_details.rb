class CreateUserBankDetails < ActiveRecord::Migration
  def change
    create_table :user_bank_details do |t|
      t.references :user, index: true
      t.string :stripe_id
      t.timestamps null: false
    end
    add_foreign_key :user_bank_details, :users
  end
end
