class AddPriceToMarkUpPrice < ActiveRecord::Migration
  def change
    add_column :mark_up_prices, :price, :decimal
  end
end
