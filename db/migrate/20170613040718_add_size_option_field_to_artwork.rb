class AddSizeOptionFieldToArtwork < ActiveRecord::Migration
  def change
    add_column :artworks, :size_option_id, :integer
  end
end
