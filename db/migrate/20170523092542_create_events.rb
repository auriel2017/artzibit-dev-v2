class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.text :location
      t.float :long
      t.float :lat
      t.timestamps null: false
    end
  end
end
