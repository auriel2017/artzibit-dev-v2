class AddLimitedEditionFieldsToArtwork < ActiveRecord::Migration
  def change
    add_column :artworks, :limited_edition, :boolean
    add_column :artworks, :quantity, :integer
  end
end
