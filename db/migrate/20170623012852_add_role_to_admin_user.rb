class AddRoleToAdminUser < ActiveRecord::Migration
  def change
    add_column :admin_users, :name, :string
    add_column :admin_users, :admin, :boolean
    add_column :admin_users, :printer, :boolean
    add_column :admin_users, :installer, :boolean
  end
end
