class AddInstallationRangeToInstallationPrice < ActiveRecord::Migration
  def change
    add_reference :installation_prices, :installation_range, index: true
    add_foreign_key :installation_prices, :installation_ranges
  end
end
