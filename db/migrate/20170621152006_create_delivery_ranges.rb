class CreateDeliveryRanges < ActiveRecord::Migration
  def change
    create_table :delivery_ranges do |t|
      t.decimal :min_size
      t.decimal :max_size
      t.boolean :with_installation
      t.timestamps null: false
    end
  end
end
