class AddWebsitePortfolioToUser < ActiveRecord::Migration
  def change
    add_column :users, :website_portfolio, :string
  end
end
