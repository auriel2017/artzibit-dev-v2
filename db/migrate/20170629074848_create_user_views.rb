class CreateUserViews < ActiveRecord::Migration
  def change
    create_table :user_views do |t|
      t.references :user, index: true
      t.references :artwork, index: true
      t.integer :counter

      t.timestamps null: false
    end
    add_foreign_key :user_views, :users
    add_foreign_key :user_views, :artworks
  end
end
