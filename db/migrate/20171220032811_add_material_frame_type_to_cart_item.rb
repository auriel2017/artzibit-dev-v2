class AddMaterialFrameTypeToCartItem < ActiveRecord::Migration
  def change
    add_reference :cart_items, :material, index: true
    add_foreign_key :cart_items, :materials
    add_reference :cart_items, :frame_type, index: true
    add_foreign_key :cart_items, :frame_types
  end
end
