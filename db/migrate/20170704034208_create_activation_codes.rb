class CreateActivationCodes < ActiveRecord::Migration
  def change
    create_table :activation_codes do |t|
      t.string :code
      t.boolean :reusable
      t.timestamps null: false
    end
  end
end
