class CreatePricePerUnits < ActiveRecord::Migration
  def change
    create_table :price_per_units do |t|
      t.decimal :number_per_unit
      t.decimal :price
      t.references :artwork, index: true
      t.datetime :effectivity_date

      t.timestamps null: false
    end
    add_foreign_key :price_per_units, :artworks
  end
end
