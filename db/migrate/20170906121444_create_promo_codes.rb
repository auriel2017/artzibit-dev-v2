class CreatePromoCodes < ActiveRecord::Migration
  def change
    create_table :promo_codes do |t|
      t.string :code
      t.integer :limit
      t.datetime :start_date
      t.datetime :end_date
      t.integer :percentage_off
      t.decimal :price_off

      t.timestamps null: false
    end
  end
end
