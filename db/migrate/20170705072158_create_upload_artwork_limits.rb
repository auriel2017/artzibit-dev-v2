class CreateUploadArtworkLimits < ActiveRecord::Migration
  def change
    create_table :upload_artwork_limits do |t|
      t.integer :from_counter
      t.integer :to_counter
      t.integer :limit

      t.timestamps null: false
    end
  end
end
