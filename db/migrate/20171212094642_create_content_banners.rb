class CreateContentBanners < ActiveRecord::Migration
  def change
    create_table :content_banners do |t|
      t.string :name
      t.boolean :active
      t.integer :sequence

      t.timestamps null: false
    end
  end
end
