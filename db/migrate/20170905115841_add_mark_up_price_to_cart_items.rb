class AddMarkUpPriceToCartItems < ActiveRecord::Migration
  def change
    add_reference :cart_items, :mark_up_price, index: true
    add_foreign_key :cart_items, :mark_up_prices
  end
end
