class AddDeliveryPriceToCart < ActiveRecord::Migration
  def change
    add_reference :carts, :delivery_price, index: true
    add_foreign_key :carts, :delivery_prices
  end
end
