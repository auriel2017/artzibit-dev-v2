class CreateWebRepositories < ActiveRecord::Migration
  def change
    create_table :web_repositories do |t|
      t.string :name
      t.text :content_html

      t.timestamps null: false
    end
  end
end
