class AddAttachmentToArtworkStyle < ActiveRecord::Migration
  def change
    add_attachment :artwork_styles, :banner_image
  end
end
