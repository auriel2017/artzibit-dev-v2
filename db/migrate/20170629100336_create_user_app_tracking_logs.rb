class CreateUserAppTrackingLogs < ActiveRecord::Migration
  def change
    create_table :user_app_tracking_logs do |t|
      t.references :user, index: true
      t.references :artwork, index: true
      t.datetime :start_time
      t.datetime :end_time
      t.integer :duration

      t.timestamps null: false
    end
    add_foreign_key :user_app_tracking_logs, :users
    add_foreign_key :user_app_tracking_logs, :artworks
  end
end
