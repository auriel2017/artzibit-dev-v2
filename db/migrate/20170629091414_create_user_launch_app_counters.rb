class CreateUserLaunchAppCounters < ActiveRecord::Migration
  def change
    create_table :user_launch_app_counters do |t|
      t.references :user, index: true
      t.integer :counter

      t.timestamps null: false
    end
    add_foreign_key :user_launch_app_counters, :users
  end
end
