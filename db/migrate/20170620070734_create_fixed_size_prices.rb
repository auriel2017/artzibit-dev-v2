class CreateFixedSizePrices < ActiveRecord::Migration
  def change
    create_table :fixed_size_prices do |t|
      t.decimal :price
      t.datetime :effectivity_date
      t.references :fixed_size, index: true

      t.timestamps null: false
    end
    add_foreign_key :fixed_size_prices, :fixed_sizes
  end
end
