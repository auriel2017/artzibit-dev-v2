class AddSizeTypeToArtwork < ActiveRecord::Migration
  def change
    add_column :artworks, :size_type, :string
  end
end
