class AddActiveToUserShippingDetail < ActiveRecord::Migration
  def change
    add_column :user_shipping_details, :active, :boolean, default: true
  end
end
