class AddExpirationDateToArtworkApprovalHdr < ActiveRecord::Migration
  def change
    add_column :artwork_approval_hdrs, :expiration_date, :datetime
  end
end
