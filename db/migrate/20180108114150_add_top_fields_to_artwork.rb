class AddTopFieldsToArtwork < ActiveRecord::Migration
  def change
    add_column :artworks, :top_pick, :boolean, default: false, allow_nil: false
    add_column :artworks, :sequence, :integer
  end
end
