class CreateArtworkApprovalDtls < ActiveRecord::Migration
  def change
    create_table :artwork_approval_dtls do |t|
      t.references :artwork_approval_hdr, index: true
      t.references :admin_user, index: true
      t.datetime :processed_at
      t.string :status
      t.string :approve_token

      t.timestamps null: false
    end
    add_foreign_key :artwork_approval_dtls, :artwork_approval_hdrs
    add_foreign_key :artwork_approval_dtls, :admin_users
  end
end
