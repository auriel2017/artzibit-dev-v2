class CreateMarkUpPrices < ActiveRecord::Migration
  def change
    create_table :mark_up_prices do |t|
      t.references :artwork, index: true
      t.integer :percentage
      t.datetime :effectivity_date

      t.timestamps null: false
    end
    add_foreign_key :mark_up_prices, :artworks
  end
end
