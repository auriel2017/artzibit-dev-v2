class AddCardStripIdToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :card_stripe_id, :string
  end
end
