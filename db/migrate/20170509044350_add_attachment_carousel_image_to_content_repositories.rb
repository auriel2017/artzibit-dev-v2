class AddAttachmentCarouselImageToContentRepositories < ActiveRecord::Migration
  def self.up
    change_table :content_repositories do |t|
      t.attachment :carousel_image
    end
  end

  def self.down
    remove_attachment :content_repositories, :carousel_image
  end
end
