class AddTokenFieldsToUserActivationCode < ActiveRecord::Migration
  def change
    add_column :user_activation_codes, :reserved_token, :string
    add_column :user_activation_codes, :expiration, :datetime
  end
end
