class AddFieldToArtworkImage < ActiveRecord::Migration
  def change
    add_column :artwork_images, :name, :string
  end
end
