class AddImageProcessingToArtworkImage < ActiveRecord::Migration
  def self.up
    add_column :artwork_images, :image_processing, :boolean
  end

  def self.down
    remove_column :artwork_images, :avatar_primage_processingocessing
  end
end