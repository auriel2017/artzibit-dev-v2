class AddArtworkToReview < ActiveRecord::Migration
  def change
    add_reference :reviews, :artwork, index: true
    add_foreign_key :reviews, :artworks
  end
end
