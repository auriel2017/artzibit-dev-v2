class AddFieldToUserShippingDetail < ActiveRecord::Migration
  def change
    add_column :user_shipping_details, :is_default, :boolean
  end
end
