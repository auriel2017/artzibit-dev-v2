class CreateOrderAssignments < ActiveRecord::Migration
  def change
    create_table :order_assignments do |t|
      t.references :order, index: true
      t.references :admin_user, index: true

      t.timestamps null: false
    end
    add_foreign_key :order_assignments, :orders
    add_foreign_key :order_assignments, :admin_users
  end
end
