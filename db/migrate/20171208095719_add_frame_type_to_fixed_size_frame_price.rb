class AddFrameTypeToFixedSizeFramePrice < ActiveRecord::Migration
  def change
    add_reference :fixed_size_frame_prices, :frame_type, index: true
    add_foreign_key :fixed_size_frame_prices, :frame_types
  end
end
