class CreateInstallationPrices < ActiveRecord::Migration
  def change
    create_table :installation_prices do |t|
      t.decimal :price
      t.datetime :effectivity_date

      t.timestamps null: false
    end
  end
end
