class CreateArtworkSizes < ActiveRecord::Migration
  def change
    create_table :artwork_sizes do |t|
      t.references :artwork
      t.decimal :height
      t.decimal :width
      t.datetime :available_until

      t.timestamps
    end
    add_index :artwork_sizes, :artwork_id
  end
end
