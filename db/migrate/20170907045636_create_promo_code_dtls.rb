class CreatePromoCodeDtls < ActiveRecord::Migration
  def change
    create_table :promo_code_dtls do |t|
      t.references :promo_code, index: true
      t.integer :promo_codable_id
      t.string :promo_codable_type

      t.timestamps null: false
    end
    add_foreign_key :promo_code_dtls, :promo_codes
  end
end
