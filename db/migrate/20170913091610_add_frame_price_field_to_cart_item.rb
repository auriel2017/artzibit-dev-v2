class AddFramePriceFieldToCartItem < ActiveRecord::Migration
  def change
    add_column :cart_items, :min_height_fixed_size_frame_price_id, :integer
    add_column :cart_items, :min_width_fixed_size_frame_price_id, :integer
    add_column :cart_items, :next_height_fixed_size_frame_price_id, :integer
    add_column :cart_items, :next_width_fixed_size_frame_price_id, :integer
  end
end
