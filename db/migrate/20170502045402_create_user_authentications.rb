class CreateUserAuthentications < ActiveRecord::Migration
  def change
    create_table :user_authentications do |t|
      t.references :user
      t.string :provider
      t.string :uid
      t.timestamps
    end
  end
end
