class AddMaterialToFixedSizePrice < ActiveRecord::Migration
  def change
    add_reference :fixed_size_prices, :material, index: true
    add_foreign_key :fixed_size_prices, :materials
  end
end
