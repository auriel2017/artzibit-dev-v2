class CreateTaggables < ActiveRecord::Migration
  def change
    create_table :taggables do |t|
      t.references :tag, index: true
      t.references :artwork, index: true

      t.timestamps null: false
    end
    add_foreign_key :taggables, :tags
    add_foreign_key :taggables, :artworks
  end
end
