class AddUserShippingAndBankDetailsToCart < ActiveRecord::Migration
  def change
    add_reference :carts, :user_shipping_detail, index: true
    add_foreign_key :carts, :user_shipping_details
    add_reference :carts, :user_bank_detail, index: true
    add_foreign_key :carts, :user_bank_details
  end
end
