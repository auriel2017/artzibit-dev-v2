class AddPostalCodeToUserShippingDetail < ActiveRecord::Migration
  def change
    add_column :user_shipping_details, :postal_code, :string
  end
end
