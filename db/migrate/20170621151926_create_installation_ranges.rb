class CreateInstallationRanges < ActiveRecord::Migration
  def change
    create_table :installation_ranges do |t|
      t.decimal :min_size
      t.decimal :max_size

      t.timestamps null: false
    end
  end
end
