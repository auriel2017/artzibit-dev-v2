class AddPromoCodeFieldsToCart < ActiveRecord::Migration
  def change
    add_reference :carts, :promo_code, index: true
    add_foreign_key :carts, :promo_codes
    add_column :carts, :discount_amount, :decimal
  end
end
