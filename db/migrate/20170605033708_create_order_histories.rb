class CreateOrderHistories < ActiveRecord::Migration
  def change
    create_table :order_histories do |t|
      t.references :order, index: true
      t.string :status
      t.datetime :processed_at
      t.timestamps null: false
    end
    add_foreign_key :order_histories, :orders
  end
end
