class CreateDeliveryPrices < ActiveRecord::Migration
  def change
    create_table :delivery_prices do |t|
      t.decimal :price
      t.datetime :effectivity_date
      t.references :delivery_range, index: true

      t.timestamps null: false
    end
    add_foreign_key :delivery_prices, :delivery_ranges
  end
end
