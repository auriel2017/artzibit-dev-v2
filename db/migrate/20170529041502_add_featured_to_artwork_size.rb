class AddFeaturedToArtworkSize < ActiveRecord::Migration
  def change
    add_column :artwork_sizes, :featured, :boolean
  end
end
