class CreateBasePrices < ActiveRecord::Migration
  def change
    create_table :base_prices do |t|
      t.integer :percentage
      t.datetime :effectivity_date
      t.timestamps null: false
    end
  end
end
