class AddByPassActivationCodeToUser < ActiveRecord::Migration
  def change
    add_column :users, :by_pass_activation_code, :boolean
  end
end
