class CreateOrderCouriers < ActiveRecord::Migration
  def change
    create_table :order_couriers do |t|
      t.references :order, index: true
      t.string :name
      t.string :tracking_no

      t.timestamps null: false
    end
    add_foreign_key :order_couriers, :orders
  end
end
