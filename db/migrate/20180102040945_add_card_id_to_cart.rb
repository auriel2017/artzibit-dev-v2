class AddCardIdToCart < ActiveRecord::Migration
  def change
    add_column :carts, :card_id, :string
  end
end
