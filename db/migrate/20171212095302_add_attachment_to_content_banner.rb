class AddAttachmentToContentBanner < ActiveRecord::Migration
  def change
    add_attachment :content_banners, :banner_image
  end
end
