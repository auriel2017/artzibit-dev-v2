class CreateArtworkPrices < ActiveRecord::Migration
  def change
    create_table :artwork_prices do |t|
      t.references :artwork
      t.references :artwork_size
      t.decimal :price
      t.datetime :effectivity_date
      t.timestamps
    end
    add_index :artwork_prices, :artwork_id
  end
end
