class CreateContentBannerDtls < ActiveRecord::Migration
  def change
    create_table :content_banner_dtls do |t|
      t.references :content_banner, index: true
      t.string :contentable_type
      t.integer :contentable_id

      t.timestamps null: false
    end
    add_foreign_key :content_banner_dtls, :content_banners
  end
end
