# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180117060952) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activation_codes", force: :cascade do |t|
    t.string   "code"
    t.boolean  "reusable"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "limit"
  end

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
    t.boolean  "admin"
    t.boolean  "printer"
    t.boolean  "installer"
    t.boolean  "vetter"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "artists", force: :cascade do |t|
    t.string   "name"
    t.text     "bio"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "artwork_approval_dtls", force: :cascade do |t|
    t.integer  "artwork_approval_hdr_id"
    t.integer  "admin_user_id"
    t.datetime "processed_at"
    t.string   "status"
    t.string   "approve_token"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "artwork_approval_dtls", ["admin_user_id"], name: "index_artwork_approval_dtls_on_admin_user_id", using: :btree
  add_index "artwork_approval_dtls", ["artwork_approval_hdr_id"], name: "index_artwork_approval_dtls_on_artwork_approval_hdr_id", using: :btree

  create_table "artwork_approval_hdrs", force: :cascade do |t|
    t.integer  "artwork_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.datetime "expiration_date"
  end

  add_index "artwork_approval_hdrs", ["artwork_id"], name: "index_artwork_approval_hdrs_on_artwork_id", using: :btree

  create_table "artwork_histories", force: :cascade do |t|
    t.integer  "artwork_id"
    t.string   "field_name"
    t.string   "old_value"
    t.string   "new_value"
    t.integer  "admin_user_id"
    t.integer  "user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "artwork_histories", ["admin_user_id"], name: "index_artwork_histories_on_admin_user_id", using: :btree
  add_index "artwork_histories", ["artwork_id"], name: "index_artwork_histories_on_artwork_id", using: :btree
  add_index "artwork_histories", ["user_id"], name: "index_artwork_histories_on_user_id", using: :btree

  create_table "artwork_images", force: :cascade do |t|
    t.integer  "artwork_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "name"
    t.string   "orientation"
    t.text     "image_meta"
    t.integer  "crop_x"
    t.integer  "crop_y"
    t.integer  "crop_w"
    t.integer  "crop_h"
    t.boolean  "image_processing"
  end

  add_index "artwork_images", ["artwork_id"], name: "index_artwork_images_on_artwork_id", using: :btree

  create_table "artwork_prices", force: :cascade do |t|
    t.integer  "artwork_id"
    t.integer  "artwork_size_id"
    t.decimal  "price"
    t.datetime "effectivity_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "artwork_prices", ["artwork_id"], name: "index_artwork_prices_on_artwork_id", using: :btree

  create_table "artwork_sizes", force: :cascade do |t|
    t.integer  "artwork_id"
    t.decimal  "height"
    t.decimal  "width"
    t.datetime "available_until"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "featured"
  end

  add_index "artwork_sizes", ["artwork_id"], name: "index_artwork_sizes_on_artwork_id", using: :btree

  create_table "artwork_styles", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "banner_image_file_name"
    t.string   "banner_image_content_type"
    t.integer  "banner_image_file_size"
    t.datetime "banner_image_updated_at"
  end

  create_table "artworks", force: :cascade do |t|
    t.string   "name"
    t.integer  "category_id"
    t.integer  "artwork_style_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "artist_id"
    t.string   "size_type"
    t.integer  "size_option_id"
    t.integer  "user_id"
    t.boolean  "limited_edition"
    t.integer  "quantity"
    t.string   "privacy"
    t.boolean  "active",            default: true
    t.integer  "total_pieces_sold"
    t.decimal  "total_sales"
    t.integer  "total_favorites"
    t.boolean  "top_pick",          default: false
    t.integer  "sequence"
  end

  add_index "artworks", ["artwork_style_id"], name: "index_artworks_on_artwork_style_id", using: :btree
  add_index "artworks", ["category_id"], name: "index_artworks_on_category_id", using: :btree

  create_table "base_prices", force: :cascade do |t|
    t.integer  "percentage"
    t.datetime "effectivity_date"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "cart_items", force: :cascade do |t|
    t.integer  "cart_id"
    t.integer  "artwork_id"
    t.integer  "artwork_size_id"
    t.integer  "artwork_price_id"
    t.integer  "price_per_unit_id"
    t.decimal  "height"
    t.decimal  "width"
    t.integer  "quantity"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.boolean  "installation",                          default: true
    t.integer  "installation_price_id"
    t.integer  "fixed_size_id"
    t.integer  "fixed_size_price_id"
    t.integer  "base_price_id"
    t.boolean  "frame",                                 default: true
    t.integer  "fixed_size_frame_price_id"
    t.integer  "min_height_fixed_size_price_id"
    t.integer  "next_height_fixed_size_price_id"
    t.integer  "min_width_fixed_size_price_id"
    t.integer  "next_width_fixed_size_price_id"
    t.integer  "mark_up_price_id"
    t.integer  "min_height_fixed_size_frame_price_id"
    t.integer  "min_width_fixed_size_frame_price_id"
    t.integer  "next_height_fixed_size_frame_price_id"
    t.integer  "next_width_fixed_size_frame_price_id"
    t.integer  "material_id"
    t.integer  "frame_type_id"
  end

  add_index "cart_items", ["artwork_id"], name: "index_cart_items_on_artwork_id", using: :btree
  add_index "cart_items", ["artwork_price_id"], name: "index_cart_items_on_artwork_price_id", using: :btree
  add_index "cart_items", ["artwork_size_id"], name: "index_cart_items_on_artwork_size_id", using: :btree
  add_index "cart_items", ["base_price_id"], name: "index_cart_items_on_base_price_id", using: :btree
  add_index "cart_items", ["cart_id"], name: "index_cart_items_on_cart_id", using: :btree
  add_index "cart_items", ["fixed_size_frame_price_id"], name: "index_cart_items_on_fixed_size_frame_price_id", using: :btree
  add_index "cart_items", ["fixed_size_id"], name: "index_cart_items_on_fixed_size_id", using: :btree
  add_index "cart_items", ["fixed_size_price_id"], name: "index_cart_items_on_fixed_size_price_id", using: :btree
  add_index "cart_items", ["frame_type_id"], name: "index_cart_items_on_frame_type_id", using: :btree
  add_index "cart_items", ["mark_up_price_id"], name: "index_cart_items_on_mark_up_price_id", using: :btree
  add_index "cart_items", ["material_id"], name: "index_cart_items_on_material_id", using: :btree
  add_index "cart_items", ["price_per_unit_id"], name: "index_cart_items_on_price_per_unit_id", using: :btree

  create_table "carts", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at",                                       null: false
    t.string   "status"
    t.decimal  "total_price",             precision: 10, scale: 2
    t.datetime "updated_at",                                       null: false
    t.integer  "delivery_price_id"
    t.integer  "promo_code_id"
    t.decimal  "discount_amount",         precision: 10, scale: 2
    t.integer  "user_shipping_detail_id"
    t.integer  "user_bank_detail_id"
    t.string   "card_id"
  end

  add_index "carts", ["delivery_price_id"], name: "index_carts_on_delivery_price_id", using: :btree
  add_index "carts", ["promo_code_id"], name: "index_carts_on_promo_code_id", using: :btree
  add_index "carts", ["user_bank_detail_id"], name: "index_carts_on_user_bank_detail_id", using: :btree
  add_index "carts", ["user_id"], name: "index_carts_on_user_id", using: :btree
  add_index "carts", ["user_shipping_detail_id"], name: "index_carts_on_user_shipping_detail_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "cover_image_file_name"
    t.string   "cover_image_content_type"
    t.integer  "cover_image_file_size"
    t.datetime "cover_image_updated_at"
    t.integer  "sequence"
  end

  create_table "content_banner_dtls", force: :cascade do |t|
    t.integer  "content_banner_id"
    t.string   "contentable_type"
    t.integer  "contentable_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "content_banner_dtls", ["content_banner_id"], name: "index_content_banner_dtls_on_content_banner_id", using: :btree

  create_table "content_banners", force: :cascade do |t|
    t.string   "name"
    t.boolean  "active"
    t.integer  "sequence"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "banner_image_file_name"
    t.string   "banner_image_content_type"
    t.integer  "banner_image_file_size"
    t.datetime "banner_image_updated_at"
  end

  create_table "content_repositories", force: :cascade do |t|
    t.string   "title"
    t.string   "subtitle"
    t.text     "description"
    t.integer  "sequence_no"
    t.text     "html_content"
    t.string   "redirect_to"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "carousel_image_file_name"
    t.string   "carousel_image_content_type"
    t.integer  "carousel_image_file_size"
    t.datetime "carousel_image_updated_at"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "delivery_prices", force: :cascade do |t|
    t.decimal  "price"
    t.datetime "effectivity_date"
    t.integer  "delivery_range_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "delivery_prices", ["delivery_range_id"], name: "index_delivery_prices_on_delivery_range_id", using: :btree

  create_table "delivery_ranges", force: :cascade do |t|
    t.decimal  "min_size"
    t.decimal  "max_size"
    t.boolean  "with_installation"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "event_artworks", force: :cascade do |t|
    t.integer  "event_id"
    t.integer  "artwork_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "event_artworks", ["artwork_id"], name: "index_event_artworks_on_artwork_id", using: :btree
  add_index "event_artworks", ["event_id"], name: "index_event_artworks_on_event_id", using: :btree

  create_table "event_batches", force: :cascade do |t|
    t.integer  "event_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "event_batches", ["event_id"], name: "index_event_batches_on_event_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.text     "location"
    t.float    "long"
    t.float    "lat"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "fixed_size_frame_prices", force: :cascade do |t|
    t.decimal  "price"
    t.datetime "effectivity_date"
    t.integer  "fixed_size_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "frame_type_id"
  end

  add_index "fixed_size_frame_prices", ["fixed_size_id"], name: "index_fixed_size_frame_prices_on_fixed_size_id", using: :btree
  add_index "fixed_size_frame_prices", ["frame_type_id"], name: "index_fixed_size_frame_prices_on_frame_type_id", using: :btree

  create_table "fixed_size_prices", force: :cascade do |t|
    t.decimal  "price"
    t.datetime "effectivity_date"
    t.integer  "fixed_size_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "material_id"
  end

  add_index "fixed_size_prices", ["fixed_size_id"], name: "index_fixed_size_prices_on_fixed_size_id", using: :btree
  add_index "fixed_size_prices", ["material_id"], name: "index_fixed_size_prices_on_material_id", using: :btree

  create_table "fixed_sizes", force: :cascade do |t|
    t.string   "label"
    t.string   "size_type"
    t.decimal  "height"
    t.decimal  "width"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "frame_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "installation_prices", force: :cascade do |t|
    t.decimal  "price"
    t.datetime "effectivity_date"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "installation_range_id"
  end

  add_index "installation_prices", ["installation_range_id"], name: "index_installation_prices_on_installation_range_id", using: :btree

  create_table "installation_ranges", force: :cascade do |t|
    t.decimal  "min_size"
    t.decimal  "max_size"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mark_up_prices", force: :cascade do |t|
    t.integer  "artwork_id"
    t.integer  "percentage"
    t.datetime "effectivity_date"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.decimal  "price"
  end

  add_index "mark_up_prices", ["artwork_id"], name: "index_mark_up_prices_on_artwork_id", using: :btree

  create_table "materials", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "no_of_approvals", force: :cascade do |t|
    t.integer  "count"
    t.datetime "effectivity_date"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "order_assignments", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "admin_user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "order_assignments", ["admin_user_id"], name: "index_order_assignments_on_admin_user_id", using: :btree
  add_index "order_assignments", ["order_id"], name: "index_order_assignments_on_order_id", using: :btree

  create_table "order_couriers", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "name"
    t.string   "tracking_no"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "order_couriers", ["order_id"], name: "index_order_couriers_on_order_id", using: :btree

  create_table "order_histories", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "status"
    t.datetime "processed_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "order_histories", ["order_id"], name: "index_order_histories_on_order_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "cart_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "user_shipping_detail_id"
    t.string   "card_stripe_id"
    t.string   "current_status"
  end

  add_index "orders", ["cart_id"], name: "index_orders_on_cart_id", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "price_per_units", force: :cascade do |t|
    t.decimal  "number_per_unit"
    t.decimal  "price"
    t.integer  "artwork_id"
    t.datetime "effectivity_date"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "price_per_units", ["artwork_id"], name: "index_price_per_units_on_artwork_id", using: :btree

  create_table "promo_code_dtls", force: :cascade do |t|
    t.integer  "promo_code_id"
    t.integer  "promo_codable_id"
    t.string   "promo_codable_type"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "promo_code_dtls", ["promo_code_id"], name: "index_promo_code_dtls_on_promo_code_id", using: :btree

  create_table "promo_codes", force: :cascade do |t|
    t.string   "code"
    t.integer  "limit"
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer  "percentage_off"
    t.decimal  "price_off"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "reviews", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "artwork_id"
  end

  add_index "reviews", ["artwork_id"], name: "index_reviews_on_artwork_id", using: :btree
  add_index "reviews", ["user_id"], name: "index_reviews_on_user_id", using: :btree

  create_table "size_options", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "supports", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "contact_no"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "supports", ["user_id"], name: "index_supports_on_user_id", using: :btree

  create_table "taggables", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "artwork_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "taggables", ["artwork_id"], name: "index_taggables_on_artwork_id", using: :btree
  add_index "taggables", ["tag_id"], name: "index_taggables_on_tag_id", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "test_users", force: :cascade do |t|
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "upload_artwork_limits", force: :cascade do |t|
    t.integer  "from_counter"
    t.integer  "to_counter"
    t.integer  "limit"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "user_activation_codes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "activation_code_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "reserved_token"
    t.datetime "expiration"
  end

  add_index "user_activation_codes", ["activation_code_id"], name: "index_user_activation_codes_on_activation_code_id", using: :btree
  add_index "user_activation_codes", ["user_id"], name: "index_user_activation_codes_on_user_id", using: :btree

  create_table "user_app_tracking_logs", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "artwork_id"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_app_tracking_logs", ["artwork_id"], name: "index_user_app_tracking_logs_on_artwork_id", using: :btree
  add_index "user_app_tracking_logs", ["user_id"], name: "index_user_app_tracking_logs_on_user_id", using: :btree

  create_table "user_authentications", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_bank_details", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "stripe_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_bank_details", ["user_id"], name: "index_user_bank_details_on_user_id", using: :btree

  create_table "user_checked_ins", force: :cascade do |t|
    t.integer  "event_batch_id"
    t.integer  "user_id"
    t.datetime "checked_in_at"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "user_checked_ins", ["event_batch_id"], name: "index_user_checked_ins_on_event_batch_id", using: :btree
  add_index "user_checked_ins", ["user_id"], name: "index_user_checked_ins_on_user_id", using: :btree

  create_table "user_favorites", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "artwork_id"
    t.boolean  "deleted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_favorites", ["artwork_id"], name: "index_user_favorites_on_artwork_id", using: :btree
  add_index "user_favorites", ["user_id"], name: "index_user_favorites_on_user_id", using: :btree

  create_table "user_launch_app_counters", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "counter"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_launch_app_counters", ["user_id"], name: "index_user_launch_app_counters_on_user_id", using: :btree

  create_table "user_shipping_details", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone_number"
    t.text     "detailed_addr"
    t.string   "city"
    t.string   "country"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "is_default"
    t.boolean  "active",        default: true
    t.string   "email"
    t.string   "postal_code"
  end

  add_index "user_shipping_details", ["user_id"], name: "index_user_shipping_details_on_user_id", using: :btree

  create_table "user_verifieds", force: :cascade do |t|
    t.integer  "user_id"
    t.boolean  "deleted"
    t.datetime "effectivity_date"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "user_verifieds", ["user_id"], name: "index_user_verifieds_on_user_id", using: :btree

  create_table "user_views", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "artwork_id"
    t.integer  "counter"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_views", ["artwork_id"], name: "index_user_views_on_artwork_id", using: :btree
  add_index "user_views", ["user_id"], name: "index_user_views_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                              default: "",   null: false
    t.string   "encrypted_password",                 default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "authentication_token",    limit: 30
    t.string   "name"
    t.string   "country"
    t.string   "username"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.boolean  "notify"
    t.string   "customer_stripe_id"
    t.string   "artist_type"
    t.integer  "artwork_sequence_number"
    t.boolean  "by_pass_activation_code"
    t.string   "website_portfolio"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "contact_no"
    t.boolean  "active",                             default: true
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "web_repositories", force: :cascade do |t|
    t.string   "name"
    t.text     "content_html"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "category"
    t.string   "page"
  end

  add_foreign_key "artwork_approval_dtls", "admin_users"
  add_foreign_key "artwork_approval_dtls", "artwork_approval_hdrs"
  add_foreign_key "artwork_approval_hdrs", "artworks"
  add_foreign_key "artwork_histories", "admin_users"
  add_foreign_key "artwork_histories", "artworks"
  add_foreign_key "artwork_histories", "users"
  add_foreign_key "cart_items", "artwork_prices"
  add_foreign_key "cart_items", "artwork_sizes"
  add_foreign_key "cart_items", "artworks"
  add_foreign_key "cart_items", "base_prices"
  add_foreign_key "cart_items", "carts"
  add_foreign_key "cart_items", "fixed_size_frame_prices"
  add_foreign_key "cart_items", "fixed_size_prices"
  add_foreign_key "cart_items", "fixed_sizes"
  add_foreign_key "cart_items", "frame_types"
  add_foreign_key "cart_items", "mark_up_prices"
  add_foreign_key "cart_items", "materials"
  add_foreign_key "cart_items", "price_per_units"
  add_foreign_key "carts", "delivery_prices"
  add_foreign_key "carts", "promo_codes"
  add_foreign_key "carts", "user_bank_details"
  add_foreign_key "carts", "user_shipping_details"
  add_foreign_key "carts", "users"
  add_foreign_key "content_banner_dtls", "content_banners"
  add_foreign_key "delivery_prices", "delivery_ranges"
  add_foreign_key "event_artworks", "artworks"
  add_foreign_key "event_artworks", "events"
  add_foreign_key "event_batches", "events"
  add_foreign_key "fixed_size_frame_prices", "fixed_sizes"
  add_foreign_key "fixed_size_frame_prices", "frame_types"
  add_foreign_key "fixed_size_prices", "fixed_sizes"
  add_foreign_key "fixed_size_prices", "materials"
  add_foreign_key "installation_prices", "installation_ranges"
  add_foreign_key "mark_up_prices", "artworks"
  add_foreign_key "order_assignments", "admin_users"
  add_foreign_key "order_assignments", "orders"
  add_foreign_key "order_couriers", "orders"
  add_foreign_key "order_histories", "orders"
  add_foreign_key "orders", "carts"
  add_foreign_key "orders", "users"
  add_foreign_key "price_per_units", "artworks"
  add_foreign_key "promo_code_dtls", "promo_codes"
  add_foreign_key "reviews", "artworks"
  add_foreign_key "reviews", "users"
  add_foreign_key "supports", "users"
  add_foreign_key "taggables", "artworks"
  add_foreign_key "taggables", "tags"
  add_foreign_key "user_activation_codes", "activation_codes"
  add_foreign_key "user_activation_codes", "users"
  add_foreign_key "user_app_tracking_logs", "artworks"
  add_foreign_key "user_app_tracking_logs", "users"
  add_foreign_key "user_bank_details", "users"
  add_foreign_key "user_checked_ins", "event_batches"
  add_foreign_key "user_checked_ins", "users"
  add_foreign_key "user_favorites", "artworks"
  add_foreign_key "user_favorites", "users"
  add_foreign_key "user_launch_app_counters", "users"
  add_foreign_key "user_shipping_details", "users"
  add_foreign_key "user_verifieds", "users"
  add_foreign_key "user_views", "artworks"
  add_foreign_key "user_views", "users"
end
