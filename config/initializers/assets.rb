Rails.application.config.assets.precompile += %w( jquery.Jcrop.css )
Rails.application.config.assets.precompile += %w( jquery.Jcrop.js )
Rails.application.config.assets.precompile += %w( devise.css)
Rails.application.config.assets.precompile += %w( svg eot woff tff )

