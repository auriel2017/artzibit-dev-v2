ArtzibitDev::Application.routes.draw do

  devise_for :users, controllers: {
    registrations: "users/registrations",
    sessions: "users/sessions",
    passwords: "users/passwords",
    mailers: "users/mailer"
  }

  get "home", to: 'page#home', as: 'home'
  get "about", to: 'page#about', as: 'about'
  get "terms_of_service", to: 'page#terms_of_service', as: 'terms_of_service'
  get "faq", to: 'page#faq', as: 'faq'
  get "contact", to: 'page#contact', as: 'contact'
  get "favorites", to: 'page#favorites', as: 'favorites'
  get "cart", to: 'page#cart', as: 'cart'
  get "profile", to: 'page#profile', as: 'profile'
  get "manual_payment", to: 'page#manual_payment', as: 'manual_payment'

  resources :charges
  resources :test_users, only: [:create]
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # match "/auth/:provider/callback" => "sessions#create"
  # match "/signout" => "sessions#destroy", :as => :signout
  # match "/signin" => "sessions#new", :as => :signin
  resources :users, only: [:show]
  resources :artwork_styles, only: [:index]
  resources :orders, only: [:show]
  resources :artworks, only: [:index, :show, :new, :create, :edit, :update,
    :destroy] do
    collection do
      get "crop/:id", action: "crop", as: "crop"
      get "reprocess/:id", action: "reprocess", as: "reprocess"
    end
  end
  resources :artwork_approval_dtls, only: [:show]

  resources :products, only: [:index, :show]

  # ==================================================== #
  # =============  API Routes - Version 1  ============= #
  # ==================================================== #
  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      devise_scope :user do
        scope "/users" do
          resources :registrations, only: [:create, :update, :destroy]
          resources :sessions, only: [:create, :destroy]
          resources :passwords, only: [:create]
        end
        get "current_user_details", to: "users#current_user_details", as: "current_user_details"
      end
      resources :users, only: [:show]
      resources :size_options, only: [:index]
      resources :categories, only: [:index]
      resources :artworks, only: [:index, :show, :new, :create, :edit, :update,
        :destroy]
      resources :artists, only: [:index]
      resources :user_favorites, only: [:index, :create, :update]
      resources :artwork_styles, only: [:index]
      resources :sizes, only: [:index]
      resources :event_batches, only: [:index, :show]
      resources :user_checked_ins, only: [:create]
      resources :carts, only: [:index, :show, :create, :update]
      resources :users, only: [:update]
      resources :orders, only: [:index, :show]
      resources :user_shipping_details, only: [:index, :create, :destroy, :update]
      resources :filters, only: [:index]
      resources :user_bank_details, only: [:index, :create]
      resources :artwork_markers, only: [:create]
      resources :user_views, only: [:create]
      resources :user_launch_app_counters, only: [:create]
      resources :user_app_tracking_logs, only: [:create]
      resources :user_activation_codes, only: [:create]
    end
  end

  # ==================================================== #
  # =============  API Routes - Version 2  ============= #
  # ==================================================== #

  namespace :api, defaults: {format: :json} do
    namespace :v2 do
      devise_scope :user do
        scope "/users" do
          resources :registrations, only: [:create, :update, :destroy]
          resources :sessions, only: [:create, :destroy]
          resources :passwords, only: [:create]
        end
      end
      resources :categories, only: [:index]
      resources :artworks
      resources :filters, only: [:index]
      resources :user_favorites, only: [:index, :create]
      resources :cart_items, only: [:index, :create, :update, :destroy]
      resources :user_bank_details, only: [:index, :create, :update]
      resources :user_shipping_details, only: [:index, :create, :update,
        :destroy]
      resources :payments, only: [:create]
      resources :orders, only: [:index, :show]
      resources :supports, only: [:create]
      resources :carts, only: [:create]
      resources :web_repositories, only: [:index, :show]
      resources :reviews, only: [:index, :show, :create]
    end
  end
  root :to => "page#home"
end
