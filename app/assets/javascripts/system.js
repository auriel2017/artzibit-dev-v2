
//global scripts
String.prototype.capitalize = function(){
  return this.toLowerCase().replace( /\b\w/g, function (m) {
      return m.toUpperCase();
  });
};

function getUrlParams( prop ) {
  var params = {};
  var search = decodeURIComponent( window.location.href.slice( window.location.href.indexOf( '?' ) + 1 ) );
  var definitions = search.split( '&' );

  definitions.forEach( function( val, key ) {
      var parts = val.split( '=', 2 );
      if(parts != window.location.href){
      	params[ parts[ 0 ] ] = parts[ 1 ];
      }
  } );

  return ( prop && prop in params ) ? params[ prop ] : params;
}


function setUrl(params){
	var clean_uri = location.protocol + "//" + location.host + location.pathname;
	var str = "";
	for (var key in params) {
	    if (str != "") {
	        str += "&";
	    }
	    str += key + "=" + params[key];
	}
	window.history.replaceState(params, document.title, clean_uri + "?"+str);
}
