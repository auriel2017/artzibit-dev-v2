//= require active_admin/base
//= require chosen-jquery
//= require jquery-ui
//= require scaffold
//= require subas/main

$(function(){
  // $(".chosen-input").chosen();
  // $(".can-create-chosen").chosen({ no_results_text: 'Press Enter to add new entry:' });

  // $("#fixed_size_select").each( function (index, element) {
  //   $(element).onclick(function(){
  //     alert("xx");
  //   });
  // }
  // );

});

$(document).on('mouseout, mouseover', '.chosen-input', function(){
  $(".chosen-input").chosen();
});


function trigger_chose(e) {
  $(".chosen-input").chosen();
}

$(document).ready(function() {
  artwork_sizes = $('#cart_artwork_sizes').html();
  artwork_prices = $('#cart_artwork_prices').html();
  price_per_units = $('#cart_price_per_units').html();
  $('#cart_artwork_sizes_input').hide();
  $('#cart_artwork_prices_input').hide();
  $('#cart_price_per_units_input').hide();
  $('#cart_fixed_size_prices_input').hide();
  $('#cart_fixed_size_frame_prices_input').hide();
  $('#cart_created_by_admin_input').hide();
  $('#cart_fixed_sizes_input').hide();
  $('#cart_materials_input').hide();
  $('#cart_frame_types_input').hide();
  $('#cart_mark_up_prices_input').hide();

  $('.has_many_add').click(function(){
   setTimeout(function() {
    var cusid_ele = document.getElementsByClassName('dynamic-select');
    for (var i = 0; i < cusid_ele.length; ++i) {
      // $(cusid_ele[i]).empty()
    }
   }, 100);
  });

});

function display_quantity_field(e) {
  quantity = document.getElementById("artwork_quantity_input");
  if (e.checked == true) {
    quantity.style.display = "block";
  }
  else {
    quantity.style.display = "none";
  }
}

function populate_artwork_size(e) {
  $(".chosen-input").chosen();
  l = e.indexOf("_artwork_id");
  idx = e.substring(27,l);
  artwork = '#cart_cart_items_attributes_' + idx + '_artwork_id';
  artwork_size = '#cart_cart_items_attributes_' + idx + '_artwork_size_id';
  user = $(artwork + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped_user + "']").html()
  $(artwork_size).html(options)
  $(artwork_size).show()
  populate_artwork_price("cart_cart_items_attributes_" + idx + "_artwork_size_id")
  // populate_price_per_unit("cart_cart_items_attributes_" + idx + "_price_per_unit_id")
}

function populate_artwork_price(e) {
  $(".chosen-input").chosen();
  l = e.indexOf("_artwork_size_id");
  idx = e.substring(27,l);
  artwork_size = '#cart_cart_items_attributes_' + idx + '_artwork_size_id';
  artwork_price = '#cart_cart_items_attributes_' + idx + '_artwork_price_id';
  user = $(artwork_size + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_prices).filter("optgroup[label='" + escaped_user + "']").html()
  $(artwork_price).html(options)
  $(artwork_price).show()
}

// function populate_price_per_unit(e) {
//   $(".chosen-input").chosen();
//   l = e.indexOf("_price_per_unit_id");
//   idx = e.substring(27,l);
//   artwork = '#cart_cart_items_attributes_' + idx + '_artwork_id';
//   price_per_unit = '#cart_cart_items_attributes_' + idx + '_price_per_unit_id';
//   user = $(artwork + ' :selected').text()
//   escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
//   options = $(price_per_units).filter("optgroup[label='" + escaped_user + "']").html()
//   $(price_per_unit).html(options)
//   $(price_per_unit).show()
// }

function populate_user_shipping_detail(e) {
  l = e.indexOf("_artwork_id");
  idx = e.substring(27,l);
  artwork = '#cart_cart_items_attributes_' + idx + '_artwork_id';
  artwork_size = '#cart_cart_items_attributes_' + idx + '_artwork_size_id';
  user = $(artwork + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped_user + "']").html()
  $(artwork_size).html(options)
  $(artwork_size).show()
  populate_artwork_price("cart_cart_items_attributes_" + idx + "_artwork_size_id")
  // populate_price_per_unit("cart_cart_items_attributes_" + idx + "_price_per_unit_id")
}

function populate_fixed_size(e) {
  artwork_sizes = $('#cart_fixed_size_prices').html();
  l = e.indexOf("_fixed_size_id");
  idx = e.substring(27,l);
  artwork = '#cart_cart_items_attributes_' + idx + '_fixed_size_id';
  artwork_size = '#cart_cart_items_attributes_' + idx + '_fixed_size_price_id';
  user = $(artwork + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped_user + "']").html()
  $(artwork_size).html(options)
  $(artwork_size).show()
}

function populate_fixed_frame_size(e) {
  artwork_sizes = $('#cart_fixed_size_frame_prices').html();
  l = e.indexOf("_fixed_size_id");
  idx = e.substring(27,l);
  artwork = '#cart_cart_items_attributes_' + idx + '_fixed_size_id';
  artwork_size = '#cart_cart_items_attributes_' + idx + '_fixed_size_frame_price_id';
  user = $(artwork + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped_user + "']").html()
  $(artwork_size).html(options)
  $(artwork_size).show()
}

function upd_populate_user_shipping(e, parent_name) {
  artwork_sizes = $('#' + parent_name + '_shippings').html();
  artwork = '#' + parent_name + '_user_id';
  artwork_size = '#' + parent_name + '_user_shipping_detail_id';
  chosen_artwork_size = '#' + parent_name + '_user_shipping_detail_id_chosen';
  user = $(artwork + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped_user + "']").html()
  $(chosen_artwork_size).hide();
  $(artwork_size).show();
  $(artwork_size).html(options);
  $(artwork_size).chosen();
  $(artwork_size).trigger("chosen:updated");
  $(chosen_artwork_size).show();
  $(artwork_size).hide();
}

function call_from_fixed_size(e, parent_name) {
  l = e.indexOf("_fixed_size_id");
  idx = e.substring(27,l);
  fixed_size_price_id = parent_name +'_' + idx + '_artwork_id'
  upd_populate_artwork_fixed_sizes(fixed_size_price_id, parent_name);
}

function upd_populate_artwork_fixed_sizes(e, parent_name) {
  artwork_sizes = $('#cart_fixed_sizes').html();
  l = e.indexOf("_artwork_id");
  idx = e.substring(27,l);
  artwork = '#' + parent_name +'_' + idx + '_artwork_id';
  artwork_size = '#' + parent_name + '_' + idx + '_fixed_size_id';
  chosen_artwork_size = '#' + parent_name + '_' + idx + '_fixed_size_id_chosen';
  user = $(artwork + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped_user + "']").html()
  $(chosen_artwork_size).hide();
  $(artwork_size).show();
  $(artwork_size).html(options);
  $(artwork_size).chosen();
  $(artwork_size).trigger("chosen:updated");
  $(chosen_artwork_size).show();
  $(artwork_size).hide();

  fixed_size_price_id = parent_name +'_' + idx + '_fixed_size_id'
  upd_populate_artwork_fixed_prices(fixed_size_price_id, parent_name);
  upd_populate_artwork_frame_prices(fixed_size_price_id, parent_name);
}

function upd_populate_artwork_markup_price(e, parent_name) {
  artwork_sizes = $('#cart_mark_up_prices').html();
  l = e.indexOf("_artwork_id");
  idx = e.substring(27,l);
  artwork = '#' + parent_name +'_' + idx + '_artwork_id';
  artwork_size = '#' + parent_name + '_' + idx + '_mark_up_price_id';
  chosen_artwork_size = '#' + parent_name + '_' + idx + '_mark_up_price_id_chosen';
  user = $(artwork + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped_user + "']").html()
  $(chosen_artwork_size).hide();
  $(artwork_size).show();
  $(artwork_size).html(options);
  $(artwork_size).chosen();
  $(artwork_size).trigger("chosen:updated");
  $(chosen_artwork_size).show();
  $(artwork_size).hide();
}

function upd_populate_artwork_fixed_prices(e, parent_name) {
  // artwork_sizes = $('#cart_fixed_size_prices').html();
  artwork_sizes = $('#cart_materials').html();
  l = e.indexOf("_fixed_size_id");
  idx = e.substring(27,l);
  fixed_size = '#' + parent_name +'_' + idx + '_fixed_size_id';
  material =  '#' + parent_name +'_' + idx + '_material_id';
  artwork_size = '#' + parent_name + '_' + idx + '_fixed_size_price_id';
  chosen_artwork_size = '#' + parent_name + '_' + idx + '_fixed_size_price_id_chosen';
  selected_user = $(fixed_size + ' :selected').text()
  selected_mat = $(material + ' :selected').text()
  escaped = selected_mat + "|" + selected_user
  escaped2 = escaped.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped2 + "']").html()
  $(chosen_artwork_size).hide();
  $(artwork_size).show();
  $(artwork_size).html(options);
  $(artwork_size).chosen();
  $(artwork_size).trigger("chosen:updated");
  $(chosen_artwork_size).show();
  $(artwork_size).hide();
}

function upd_populate_artwork_fixed_prices_from_material(e, parent_name) {
  // artwork_sizes = $('#cart_fixed_size_prices').html();
  artwork_sizes = $('#cart_materials').html();
  l = e.indexOf("_material_id");
  idx = e.substring(27,l);
  fixed_size = '#' + parent_name +'_' + idx + '_fixed_size_id';
  material =  '#' + parent_name +'_' + idx + '_material_id';
  artwork_size = '#' + parent_name + '_' + idx + '_fixed_size_price_id';
  chosen_artwork_size = '#' + parent_name + '_' + idx + '_fixed_size_price_id_chosen';
  selected_user = $(fixed_size + ' :selected').text()
  selected_mat = $(material + ' :selected').text()
  escaped = selected_mat + "|" + selected_user
  escaped2 = escaped.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped2 + "']").html()
  $(chosen_artwork_size).hide();
  $(artwork_size).show();
  $(artwork_size).html(options);
  $(artwork_size).chosen();
  $(artwork_size).trigger("chosen:updated");
  $(chosen_artwork_size).show();
  $(artwork_size).hide();
}

function upd_populate_artwork_frame_prices(e, parent_name) {
  // artwork_sizes = $('#cart_fixed_size_prices').html();
  artwork_sizes = $('#cart_frame_types').html();
  l = e.indexOf("_fixed_size_id");
  idx = e.substring(27,l);
  fixed_size = '#' + parent_name +'_' + idx + '_fixed_size_id';
  material =  '#' + parent_name +'_' + idx + '_frame_type_id';
  artwork_size = '#' + parent_name + '_' + idx + '_fixed_size_frame_price_id';
  chosen_artwork_size = '#' + parent_name + '_' + idx + '_fixed_size_frame_price_id_chosen';
  selected_user = $(fixed_size + ' :selected').text()
  selected_mat = $(material + ' :selected').text()
  escaped = selected_mat + "|" + selected_user
  escaped2 = escaped.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped2 + "']").html()
  $(chosen_artwork_size).hide();
  $(artwork_size).show();
  $(artwork_size).html(options);
  $(artwork_size).chosen();
  $(artwork_size).trigger("chosen:updated");
  $(chosen_artwork_size).show();
  $(artwork_size).hide();
}

function upd_populate_artwork_frame_prices_from_frame_type(e, parent_name) {
  // artwork_sizes = $('#cart_fixed_size_prices').html();
  artwork_sizes = $('#cart_frame_types').html();
  l = e.indexOf("_frame_type_id");
  idx = e.substring(27,l);
  fixed_size = '#' + parent_name +'_' + idx + '_fixed_size_id';
  material =  '#' + parent_name +'_' + idx + '_frame_type_id';
  artwork_size = '#' + parent_name + '_' + idx + '_fixed_size_frame_price_id';
  chosen_artwork_size = '#' + parent_name + '_' + idx + '_fixed_size_frame_price_id_chosen';
  selected_user = $(fixed_size + ' :selected').text()
  selected_mat = $(material + ' :selected').text()
  escaped = selected_mat + "|" + selected_user
  escaped2 = escaped.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped2 + "']").html()
  $(chosen_artwork_size).hide();
  $(artwork_size).show();
  $(artwork_size).html(options);
  $(artwork_size).chosen();
  $(artwork_size).trigger("chosen:updated");
  $(chosen_artwork_size).show();
  $(artwork_size).hide();
}

function populate_dynamic_select(e) {
  artwork_sizes = $('#content_banner_models').html();
  l = e.indexOf("_contentable_type");
  idx = e.substring(46,l);
  artwork = '#content_banner_content_banner_dtls_attributes_' + idx + '_contentable_type';
  artwork_size = '#content_banner_content_banner_dtls_attributes_' + idx + '_contentable_name';
  chosen_artwork_size = '#content_banner_content_banner_dtls_attributes_' + idx + '_contentable_name_chosen';
  user = $(artwork + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped_user + "']").html()
  $(chosen_artwork_size).hide();
  $(artwork_size).show();
  $(artwork_size).html(options);
  $(artwork_size).chosen();
  $(artwork_size).trigger("chosen:updated");
  $(chosen_artwork_size).show();
  $(artwork_size).hide();
}