$(function() {
	var scope = $('#faq-page')
	var sections = scope.find('section')

   scope.find('.treeview a').on('click',function(event){
   		event.preventDefault()
   		var target = $(this).attr('href').replace('#','')
   		$('#faq-page .treeview li').removeClass('active')
   		$(this).parent('li').addClass('active')
   		toggleActiveFaq(target)
   })

   function toggleActiveFaq(id){
   		sections.hide()
   		scope.find('#'+id).slideDown()
   }
});