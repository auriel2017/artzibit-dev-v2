$ ->
  # enable chosen js
  #$('.chosen-select').chosen
  #  allow_single_deselect: true
  #  no_results_text: 'No results matched'

jQuery ->
  students = $('#artwork_price_artwork_size_id').html()
  user = $('#artwork_price_artwork_id :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(students).filter("optgroup[label='#{escaped_user}']").html()
  if options
    $('#artwork_price_artwork_size_id').html(options)
    $('#artwork_price_artwork_size_id').parent().show()
  $('#artwork_price_artwork_id').change ->
    user = $('#artwork_price_artwork_id :selected').text()
    escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(students).filter("optgroup[label='#{escaped_user}']").html()
    if options
      $('#artwork_price_artwork_size_id').html(options)
      $('#artwork_price_artwork_size_id').parent().show()
    else
      $('#artwork_price_artwork_size_id').empty()

jQuery ->
  shippings = $('#cart_user_shipping_detail_id').html()
  user = $('#cart_user_id :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(shippings).filter("optgroup[label='#{escaped_user}']").html()
  if options
    $('#cart_user_shipping_detail_id').html(options)
    $('#cart_user_shipping_detail_id').parent().show()
  $('#cart_user_id').change ->
    user = $('#cart_user_id :selected').text()
    escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(shippings).filter("optgroup[label='#{escaped_user}']").html()
    if options
      $('#cart_user_shipping_detail_id').html(options)
      $('#cart_user_shipping_detail_id').parent().show()
    else
      $('#cart_user_shipping_detail_id').empty()