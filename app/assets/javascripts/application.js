// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery.Jcrop
//= require chosen-jquery
//= require active_admin
//= require scaffold
//= require bootstrap


//= require subas/vendor/modernizr-2.8.3.min.js
//= require subas/jquery.nivo.slider.js
//= require subas/plugins.js
//= require subas/main.js


//= require lodash
//= require angular
//= require angular-animate
//= require angular-resource
//= require restangular
//= require angular-ui-bootstrap
//= require angular-ui-bootstrap-tpls


//= require plugins/angular-confirm.js
//= require plugins/ng-country-select.js
//= require plugins/angular-validator.js
//= require plugins/angular-input-masks-standalone.min.js
//= require plugins/ng-tags-input.min.js
//= require plugins/ng-mask.min.js

//= require plugins/stripe-checkout.min.js
//= require plugins/stripe-v3.min.js

//= require plugins/ng-file-upload/ng-file-upload-shim
//= require plugins/ng-file-upload/ng-file-upload
//= require plugins/angular-slick.min.js
//= require plugins/bootstrap_carousel_touch_slider.js

//= require angular/app.js

//= require angular/controllers/products.js
//= require angular/controllers/product_show.js
//= require angular/controllers/artwork_new.js
//= require angular/controllers/artwork_edit.js
//= require angular/controllers/profile.js
//= require angular/controllers/profile/my_profile.js
//= require angular/controllers/profile/my_orders.js
//= require angular/controllers/profile/payment_methods.js
//= require angular/controllers/profile/shipping_details.js
//= require angular/controllers/profile/my_artworks.js
//= require angular/controllers/manual_payment.js
//= require angular/controllers/home.js
//= require angular/controllers/header.js
//= require angular/controllers/favorites.js
//= require angular/controllers/terms_of_service.js
//= require angular/controllers/cart.js
//= require angular/controllers/contact.js

//= require angular/services/artworks.js
//= require angular/services/users.js
//= require angular/services/user_bank_details.js
//= require angular/services/user_checked_ins.js
//= require angular/services/user_favourites.js
//= require angular/services/user_launch_app_counters.js
//= require angular/services/user_shipping_details.js
//= require angular/services/orders.js
//= require angular/services/registrations.js
//= require angular/services/payments.js
//= require angular/services/categories.js
//= require angular/services/artwork_styles.js
//= require angular/services/artists.js
//= require angular/services/product_filters.js
//= require angular/services/cart_items.js
//= require angular/services/sessions.js
//= require angular/services/reviews.js
//= require angular/services/web_repositories.js
//= require angular/services/cart.js
//= require angular/services/categories_v2.js
//= require angular/services/supports.js

//= require angular/directives/focus-on-error.js
//= require angular/directives/favorite-toggle-button.js
//= require angular/directives/disable-right-click.js
//= require angular/directives/numbers-only.js
//= require angular/filters/reverse.js
//= require angular/filters/trust-as-html.js

//= require system.js

//= require faq.js
//= require cart.js


// $(document).ready(function () {
//     var menu = $('.navbar-transparent');
//     var origOffsetY = menu.offset().top;
//     function scroll() {
//         if ($(window).scrollTop() != 0) {
//           $('.navbar-transparent').addClass('navbar-gray');
//         }
//         else {
//           $('.navbar-transparent').removeClass('navbar-gray');
//         }
//     }
//     document.onscroll = scroll;
// });

// function at_upload(e) {
//   // var _URL = window.URL || window.webkitURL;
//   // var file, img;
//   // alert(e.id);
//   // if (file = e.files[0]) {
//   //     img = new Image();
//   //     img.onload = function() {
//   //         alert(e.width + " " + e.height);
//   //     };
//   //     img.onerror = function() {
//   //         alert( "not a valid file: " + file.type);
//   //     };
//   //     img.src = _URL.createObjectURL(file);
//   // }
// }

// $(document).ready(function () {
//   var _URL = window.URL || window.webkitURL;
//   $("#artwork_artwork_images_attributes_0_image").change(function(e) {
//       var file, img;
//       var square = document.getElementById("square_sizes");
//       var landscape = document.getElementById("landscape_sizes");
//       var portrait = document.getElementById("portrait_sizes");
//       var field_classname = document.getElementById("artwork_orientation");
//       if ((file = this.files[0])) {
//           img = new Image();
//           img.onload = function() {
//           var markup_price = document.getElementById("artwork_mark_up_prices_attributes_0_percentage").value;
//             if (this.width == this.height) {
//               square.style.display = "block";
//               landscape.style.display = "none";
//               portrait.style.display = "none";
//               field_classname.value = "square";
//               update_profit_fields("square");
//             }
//             else {
//               if (this.width < this.height) {
//                 square.style.display = "none";
//                 landscape.style.display = "none";
//                 portrait.style.display = "block";
//                 field_classname.value = "portrait";
//                 update_profit_fields("portrait");
//               }
//               else {
//                 square.style.display = "none";
//                 landscape.style.display = "block";
//                 portrait.style.display = "none";
//                 field_classname.value = "landscape";
//                 update_profit_fields("landscape");
//               }
//             }
//           };

//           img.onerror = function() {
//               alert( "not a valid file: " + file.type);
//           };
//           img.src = _URL.createObjectURL(file);
//       }
//   });
// });

// $(document).ready(function () {
//   if (document.getElementById("artwork_mark_up_prices_attributes_0_percentage")) {
//     update_profit_fields("square");
//   }
//   $("#artwork_mark_up_prices_attributes_0_percentage").change(function(e) {
//       var square = document.getElementById("square_sizes");
//       var landscape = document.getElementById("landscape_sizes");
//       var portrait = document.getElementById("portrait_sizes");
//       var field_classname = document.getElementById("artwork_orientation");

//       if (field_classname.value == "square") {
//         square.style.display = "block";
//         field_classname.value = "square";
//         update_profit_fields("square");
//       }
//       else {
//         if (field_classname.value == "portrait") {
//           portrait.style.display = "block";
//           field_classname.value = "portrait";
//           update_profit_fields("portrait");
//         }
//         else {
//           landscape.style.display = "block";
//           field_classname.value = "landscape";
//           update_profit_fields("landscape");
//         }
//       }
//   });
// });

// function update_profit_fields(group_name) {
//   var markup_price = document.getElementById("artwork_mark_up_prices_attributes_0_percentage").value;
//   var field_classname = document.getElementById("artwork_orientation").value;
//   if (field_classname) {
//     var profit_fields = document.getElementsByClassName(field_classname + "_fields");
//     var group_fields = field_classname;
//   }
//   else {
//     var profit_fields = document.getElementsByClassName(group_name + "_fields");
//     var group_fields = group_name;
//   }

//   if (markup_price == "") {
//     markup_price = 0;
//   }
//   else {
//     var markup_base = parseInt(markup_price) / parseFloat(100);
//     markup_price = markup_base;
//   }

//   for(var i = 0, length = profit_fields.length; i < length; i++) {
//     base_field = document.getElementById(group_fields + "_base_" + i);
//     retail_field = document.getElementById(group_fields + "_retail_" + i);
//     if(profit_fields[i]){
//       markup_amount = (parseFloat(markup_price) * parseFloat(base_field.value)).toFixed(2);
//       retail_price = (parseFloat(base_field.value) + parseFloat(markup_amount)).toFixed(2);;
//       profit_fields[i].value = commaSeparateNumber(markup_amount);
//       retail_field.value = commaSeparateNumber(retail_price);
//     }
//   }
// }

// function commaSeparateNumber(val){
//   while (/(\d+)(\d{3})/.test(val.toString())){
//     val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
//   }
//   return val;
// }


// function check_test_user_field() {
//     var email = document.getElementById("test_user_email")
//     if (email.value.length > 0) {
//       if (validateEmail(email.value)) {
//         return true;
//       }
//       else {
//         alert("Please enter valid email");
//         email.focus();
//         return false;
//       }
//     }
//     else {
//       alert("Please enter email");
//       email.focus();
//       return false;
//     }
// }
// function validateEmail(email) {
//   var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//   return re.test(email);
// }

// function closeButton() {
//   var notice = document.getElementById('notice');
//   var error = document.getElementById('error');
//   var alert = document.getElementById('alert');
//   document.getElementById('page-mask').style.display = "none";
//   if (notice) {
//     notice.style.display = "none";
//   }
//   if (error) {
//     error.style.display = "none";
//   }
//   if (alert) {
//     alert.style.display = "none";
//   }
// }


// function should_certify() {
//   if (document.getElementById("accept").checked == true) {
//     return true;
//   }
//   else {
//     alert("Please accept the terms and conditions before proceeding.");
//     return false;
//   }
// }
// function show_quantity(){
//   var yes = document.getElementById("artwork_limited_edition_true").checked;
//   if (yes) {
//     document.getElementById("artwork_quantity_div").style.display = "inline";
//   }
//   else {
//     document.getElementById("artwork_quantity").value = "";
//     document.getElementById("artwork_quantity_div").style.display = "none";
//   }
// }

// $(document).ready(function() {
//   create = document.getElementsByClassName("create_artwork_form");
//   if (create.length == 1) {
//     var x = $('.right-div').height();
//     $('.left-div').css('height', x + 100);
//   }
//   else {
//     $('.left-div').css('height', "100vh");
//   }
// });

// //
// $(document).ready(function(){
//   // $('#home_carousel').bsTouchSlider();
// })
