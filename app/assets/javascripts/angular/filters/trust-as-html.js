angular
	.module('microsite')
	.filter('trustAsHtml', function ($sce) {
    return function(htmlCode) {
	    return $sce.trustAsHtml(htmlCode);
	  };
});
