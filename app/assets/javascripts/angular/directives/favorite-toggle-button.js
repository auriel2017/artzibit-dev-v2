angular
	    .module('microsite')
	    .directive('favoriteToggleButton', favoriteToggleButton);

	    favoriteToggleButton.$inject = ['$ngConfirm', 'userFavoritesService']

	function favoriteToggleButton($ngConfirm, userFavoritesService) {
		var directive = {
        link: link,
        scope: '=',
        controllerAs: 'vm',
        bindToController: true // because the scope is isolated

    };
    return directive;

    function link(scope, elem, attrs) {
    	elem.on("click", function(){
            var target_field = attrs.target ? attrs.target : 'product'
            var target_scope = attrs.target ? scope["product"][attrs.target] : scope["product"]
    		confirmAction(target_scope)

    	})
      /* */
    }


    //functions
    function confirmAction(item){
    	var message = item.favorited ? 'Remove from favorites?' : 'Add to favorites?'
    	$ngConfirm({
	    		content: '<p>'+message+'</p>',
	    		buttons: {
	    			continue:{
		    			btnClass: 'btn-primary',
		    			action: function(scope, button){
		    				updateOrCreate(item)
		    			}
	    			},
	    			close: function(scope, button){}

	    		}
	    	})
    }

    function updateOrCreate(item){
        item.loading = true
    	userFavoritesServiceCreate({artwork_id: item.id})
    		.then(function(res){
    			if(res.status){
    				item.favorited = res.data.favorited
                    item.favorites_count = res.data.favorites_count
                    if(item["callback"]){
                        item["callback"](item)
                    }
                    item.loading = false
    			}else{
                    $ngConfirm({title: "Message", content: res.message})
                    item.loading = false
                }
    		},function(){
                item.loading = false
            })

    }

    //services
    function userFavoritesServiceCreate(id, params){
    	return userFavoritesService.create(id, params)
    					.then(function(res){
    						return res
    					})
    }
}
