
(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('ArtworkEditController', ArtworkEditController)

		ArtworkEditController.$inject = ['$ngConfirm', '$scope', 'artworksService', 'categoriesService', 'artworkStylesService']

		function ArtworkEditController($ngConfirm, $scope, artworksService, categoriesService, artworkStylesService){
			//variables
			var vm = this
			vm.categories = []
			vm.artwork_styles = []
			vm.artwork = false
			vm.selected_image = null
			vm.new_form_submitting = false

			vm.submitUpdateForm = submitUpdateForm
			vm.preUploadImage = preUploadImage
			vm.edit_params_valid = [ 'title', 'description', 'category_id', 'privacy', 'price', 'tags', 'image' ]
			vm.edit_params = {}
			categoriesServiceIndex()
			getArtworkEdit(ARTWORK_ID, {})


			function categoriesServiceIndex(params){
				return categoriesService.index(params)
								.then(function(res){
									console.log(res)
									vm.categories = res.data
								}, function(err){ console.log("err:", err)} )
			}



			function getArtworkEdit(id, params){
				artworksServiceEdit(id,params)
					.then(function(res){
						if(res.status){
							vm.artwork = angular.copy(res.data)
							setEditParams(res.data)
						}
					})
			}

			function submitUpdateForm(){
				angular.forEach(vm.edit_params, function(v, k){
					if(!vm.edit_params_valid.includes(k)){
						delete vm.edit_params[k]
					}
				})
				vm.edit_params['price'] = vm.edit_params['price'].toString()
				var params = {artwork: vm.edit_params}
				artworksServiceUpdate(ARTWORK_ID, params)
					.then(function(res){
						if(res.status){
							scrollToTop()
							vm.artwork.image = res.data.image
							$ngConfirm({
								title: 'Success!',
								content: "Successfully Updated!"
							})		
						}
					})
			}
			

			function preUploadImage(file){
				if(!file){ return false}
    		var fileReader = new FileReader();
				fileReader.readAsDataURL(file);
				vm.selected_image = file
				fileReader.onload = function (e) {
				  var dataUrl = e.target.result;
				  var base64Data = dataUrl.substr(dataUrl.indexOf('base64,') + 'base64,'.length);
				  vm.edit_params.image = base64Data
				  $scope.$apply()
				};
				$scope.$apply()
			}


			function setEditParams(data){
				vm.edit_params = angular.copy(data)
				vm.edit_params['title'] = angular.copy(data['name'])
				vm.edit_params['price'] = angular.copy(data['mark_up_price'])
				vm.edit_params['tags'] = angular.copy(data['tags'].replace(/\s/g,''))
				delete vm.edit_params['image']
			}

			function resetForm(name){
				vm.selected_image = null
				$scope[name].reset()
				scrollToTop()
			}

			function scrollToTop(){
				$("html, body").animate({ scrollTop: 0 }, 400);
			}

			//services
			function artworksServiceEdit(id, params){
				return artworksService.edit(id, params)
								.then(function(res){
									return res
								})
			}

			function artworksServiceUpdate(id, params){
				return artworksService.update(id, params)
								.then(function(res){
									return res
									
								})
			}

		}
}());









