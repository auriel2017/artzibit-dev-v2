
(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('ContactController', ContactController)

		ContactController.$inject = ['$ngConfirm', '$scope', 'supportsService']

		function ContactController($ngConfirm, $scope, supportsService){
			//variables
			var vm = this
			vm.form_params = {name: null, email: null, subject: null, content: null}

			vm.sendForm = sendForm
			vm.formLoading = null

			//functions
			function sendForm(){
				vm.formLoading = $ngConfirm({
														content: "<div class='text-center'> Submitting <i class='fa fa-spin fa-spinner'></i></div>",
														closeIcon: false,
														title: "",
													})

				supportsServiceCreate({support: vm.form_params})
					.then(function(res){
						if(res.status){
							vm.formLoading.close()
							$ngConfirm({	
														title: "",
														closeIcon: false,
														columnClass: "col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 ng-confirm-center-buttons",
														content: "<div class='text-center'><strong>Thank you for reaching out.</strong><br/>A representative will contact you regarding your query shortly.</div>",
														buttons: {
															ok: {
																text: 'OK',
									            	btnClass: 'button btn btn-primary btn-lg btn-blue center-block',
									            }
														}
													})
							$scope.contact_form.reset()
						}else{
							vm.formLoading.close()
							$ngConfirm({	title: "", 
														content: "<div class='text-center'>"+res.message+"</div>", 
														buttons: {
															ok: {
																text: 'OK',
									            	btnClass: 'button btn btn-primary btn-lg btn-blue center-block',
									            }
														},	
														columnClass: "col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 ng-confirm-center-buttons",
													})
						}
						scrollToTop()
					},function(err){
						vm.formLoading.close()
						$ngConfirm({	title: "", 
														content: "<div class='text-center'>"+res.message+"</div>", 
														buttons: {
															ok: {
																text: 'OK',
									            	btnClass: 'button btn btn-primary btn-lg btn-blue center-block',
									            }
														},
														columnClass: "col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 ng-confirm-center-buttons",
													})
						scrollToTop()
					})
			}

			//services
			function supportsServiceCreate(params){
				return	supportsService.create(params)
									.then(function(res){
										return res
									},function(res){
										return res
									})
			}

			function scrollToTop(){
				$("html, body").animate({ scrollTop: 0 }, 400);
			}

		}
}());









