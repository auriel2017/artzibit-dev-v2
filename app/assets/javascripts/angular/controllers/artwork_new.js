
(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('ArtworkNewController', ArtworkNewController)

		ArtworkNewController.$inject = ['$ngConfirm', '$scope', 'artworksService', 'categoriesService', 'artworkStylesService']

		function ArtworkNewController($ngConfirm, $scope, artworksService, categoriesService, artworkStylesService){
			//variables
			var vm = this
			vm.categories = []
			vm.artwork_styles = []
			vm.artwork_default = {	
				title: null,
				description: null,
				category_id: null,
				privacy: "public",
				price: null,
				tags: null,
				image: null,
				accept: false,
			}	
			vm.artwork = angular.copy(vm.artwork_default)
			vm.selected_image = null
			vm.new_form_submitting = false
				

			vm.submitNewForm = submitNewForm
			vm.preUploadImage = preUploadImage

			categoriesServiceIndex()
			// artworkStylesServiceIndex()


			function categoriesServiceIndex(params){
				return categoriesService.index(params)
								.then(function(res){
									console.log(res)
									vm.categories = res.data
								}, function(err){ console.log("err:", err)} )
			}
			function artworkStylesServiceIndex(params){
				return artworkStylesService.index(params)
								.then(function(res){
									console.log(res)
									vm.artwork_styles = res.data
								}, function(err){ console.log("err:", err)} )
			}

			function preUploadImage(file){
				if(!file){ return false}
    		var fileReader = new FileReader();
				fileReader.readAsDataURL(file);
				vm.selected_image = file
				fileReader.onload = function (e) {
				  var dataUrl = e.target.result;
				  var base64Data = dataUrl.substr(dataUrl.indexOf('base64,') + 'base64,'.length);
				  vm.artwork.image = base64Data
				  $scope.$apply()
				};
				$scope.$apply()
			}

			function submitNewForm(){
				var params = {artwork: vm.artwork}
				if(vm.artwork.image == null){
					return $('#artwork-image').focus()
				}
				if(vm.artwork.image && $scope.new_form.$valid){
					// params['artwork']['tags'] = vm.artwork.tags.map(function(elem){return elem.text}).join(",")
					
					artworksServiceCreate(params)
						.then(function(res){
							if(res.status){
								resetForm('new_form')
								$("html, body").animate({ scrollTop: 0 }, 100);
								return $ngConfirm({
												title: '',
												closeIcon: false,
												columnClass: "col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 ng-confirm-center-buttons",
												content: "<div class='text-center'><strong>You have succesfully uploaded your artwork!</strong> <br/> Public artworks are subject to an approval process that can take up to 48 hours. Private artworks will be posted immediately.</div>",
												buttons: {
													heyThere: {
								            text: 'OK',
								            btnClass: 'button btn btn-primary btn-lg btn-blue center-block',
								            action: function(scope, button){
								                // button action
								                window.location.href = "/profile?active_menu=my_artworks"
								            }
								        	}
								      	}
											})		
							}else{
								$ngConfirm(res.message)
							}
						},function(err){console.log(err)})
				}
				
			}

			function resetForm(name){
				vm.artwork = angular.copy(vm.artwork_default)
				vm.selected_image = null
				$scope[name].reset()
				$("html, body").animate({ scrollTop: 0 }, 400);
			}
			//services
			function artworksServiceCreate(params){
				vm.new_form_submitting = true
				vm.submitting = $ngConfirm({
			    closeIcon: false,
			    title: '',
			    content: '<div class="text-center"><i class="fa fa-spinner fa-spin"></i> Submitting..</div>',
				});
				
				return artworksService.create(params)
								.then(function(res){
									// vm.new_form_submitting = false
									vm.submitting.close()
									return res
								}, function(err){
									// vm.new_form_submitting = false
									vm.submitting.close()
									console.log(err)
								})
			}

		}
}());









