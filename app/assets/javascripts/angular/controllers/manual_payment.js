(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('ManualPaymentController', ManualPaymentController);
		ManualPaymentController.$inject = ['$ngConfirm', '$scope', '$filter', 'paymentsService'];

		function ManualPaymentController($ngConfirm, $scope, $filter, paymentsService){
			var vm = this
			vm.amount = null
			

			vm.initStripeForm = initStripeForm


			function initStripeForm(){
				var token_triggered = false
				var token = function(res){
			    var $id = $('<input type=hidden name=stripeToken />').val(res.id);
			    var $email = $('<input type=hidden name=stripeEmail />').val(res.email);
			    $('form').append($id).append($email).submit();
			    token_triggered = true
			    chargeStripe({amount: getValidAmount(), token: res})
			  }

				var handler = StripeCheckout.configure({
				  key:         PUBLISHABLE_KEY,
				  // key:         'pk_test_6pRNASCoBOKtIshFeQd4XMUh',
				  amount:      getValidAmount(),
				  name:        'Artzibit',
				  description: 'Manual Payment',
				  panelLabel:  'Pay',
				  image:        '/assets/artzibit-logo.png',
				  token:       token,
				  opened: function() {
				  	// console.log('Opened:', getValidAmount())
				  },
				  closed: function() {
				  	if (!token_triggered) {
	            console.log('closed')
	          } else {
	          	// completedStripeForm()
	          }
				  }
					});

				handler.open({});
				return false;
			}

			function chargeStripe(params){
				return  paymentsService.create(params)
									.then(function(res){
										console.log('res',res)
										if(res.status == 200 || res.status){
											completedStripeForm()
										}else{
											handleError(res)
											
										}
									})
			}

			function completedStripeForm(){
				$ngConfirm({
					title: 'Transaction completed!',
          content: 'You paid <strong>' + $filter('currency')(vm.amount.toFixed(2)) + '</strong>',
				})
				vm.amount = null
			}


			function getValidAmount(){
				return vm.amount ? parseInt(vm.amount.toFixed(2).split('.').join('')) : 0
			}

			function handleError(res){
				$ngConfirm({
					title: res.message,
          content: res.data.message
				})
				resetAmount()
			}

			function resetAmount(){
				vm.amount = 0
			}


		}//controller

})();