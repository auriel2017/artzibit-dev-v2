(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('HeaderController', HeaderController)
		HeaderController.$inject = ['$ngConfirm', '$scope', '$rootScope', 'productFiltersService', 'cartItemsService']

		function HeaderController($ngConfirm, $scope, $rootScope, productFiltersService, cartItemsService){
			var vm = this;
			vm.keyword = ''
			// vm.submitSearch = function(){ location.href = "products?keyword=" + vm.keyword }
			$rootScope.current_user_id = CURRENT_USER_ID
			$rootScope.categories = []
			$rootScope.cart_items = false
			$rootScope.getCartItems = getCartItems

			//init
			getFilters()
			getCartItems()


			//functions
			function getFilters(params){
	    	return productFiltersServiceIndex(params)
	    					.then(function(res){
	    						if(res.status){
	    							$rootScope.categories = res.data[1]
	    						}else{
	    							$rootScope.categories = []
	    						}
	    					})
	    }

	    function getCartItems(params){
	    	return cartItemsServiceIndex(params)
	    					.then(function(res){
	    						if(res.status){
	    							$rootScope.cart_items = res.data
	    							if(!res.data || _.isEmpty(res.data)){$rootScope.cart_items = []}
	   	 						}else{
	   	 							$rootScope.cart_items = []
	   	 						}
	    					})
	    }

			//services
			function productFiltersServiceIndex(params){
	    	return productFiltersService.index(params)
	    					.then(function(res){
	    						return res
	    					})
	    }
	    function cartItemsServiceIndex(params){
	    	return cartItemsService.index(params)
	    					.then(function(res){
	    						return res
	    					})
	    }
		}

})();















