(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('FavoritesController', FavoritesController);
		FavoritesController.$inject = ['$ngConfirm', '$scope','userFavoritesService'];

		function FavoritesController($ngConfirm, $scope, userFavoritesService){
			var vm = this;
			vm.favorites = false
			vm.faveToggelCallback = faveToggelCallback
			vm.confirmAction = confirmAction

			//init onload
			getAllFavorites()	


			//functions

			function getAllFavorites(){
				return userFavoritesServiceIndex()
								.then(function(res){
									if(res.status){
										vm.favorites = res.data
										if(!res.data){vm.favorites = []}
									}else{
										vm.favorites = []
									}
								})

			}

			function confirmAction(item){
	    	var message = item.favorited ? 'Remove from favorites?' : 'Add to favorites?'
	    	$ngConfirm({
		    		content: '<p>'+message+'</p>',
		    		buttons: {
		    			continue:{
			    			btnClass: 'btn-primary',
			    			action: function(scope, button){
			    				vm.favorites = false
			    				updateOrCreate(item).then(function(res){
			    					getAllFavorites()
			    				})
			    			}
		    			},
		    			close: function(scope, button){}

		    		}
		    	})
	    }

			function faveToggelCallback(item){
				if(!item.favorited){
					var index = _.findIndex(vm.favorites, function(val) { return val.id == item.id })
					vm.favorites.splice(index, 1)
				}
			}

			function updateOrCreate(item){
	    	return userFavoritesServiceCreate({artwork_id: item.id})
				    		.then(function(res){
				    			if(res.status){
				    				item.favorited = res.data.favorited
				                    item.favorites_count = res.data.favorites_count
				                    if(item["callback"]){
				                        item["callback"](item)
				                    }
				    			}else{
				                    $ngConfirm({title: "Message", content: res.message})
				                }
				    		})

	    }

			//services

			function userFavoritesServiceIndex(params){
				return userFavoritesService.index(params)
								.then(function(res){
									return res
								})
			}

	    function userFavoritesServiceCreate(id, params){
	    	return userFavoritesService.create(id, params)
	    					.then(function(res){
	    						return res
	    					})
	    }
		}


})();