(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('ProductShowController', ProductShowController)

		ProductShowController.$inject = ['$scope','$ngConfirm', '$window', '$rootScope', 'artworksService', 'cartItemsService', 'sessionsService',  'reviewsService']

		function ProductShowController($scope, $ngConfirm, $window, $rootScope, artworksService, cartItemsService, sessionsService, reviewsService){
			//variables
			var vm = this
			vm.computed_price = 0
			vm.current_user_id = CURRENT_USER_ID
			vm.product_id = PRODUCT_ID
			vm.details = false
			vm.print_sizes = []
			vm.materials = []
			vm.frames = []
			vm.added_item = {}
			vm.reviews = false
			vm.review_params_default = {
				artwork_id: PRODUCT_ID,
				content: '',
				image: null
			}
			vm.review_params = angular.copy(vm.review_params_default)
			//scope's functions
			vm.new_params_default =   {		
				artwork_id: PRODUCT_ID,	
				// material: false,
				material_id: null,	//a
				fixed_size_id: null,	//a
				fixed_size_price_id: null,	//a
				quantity: 1,	
				// frame: false,	//b
				frame_type_id: null, //b	
				fixed_size_frame_price_id: null, //b	
				installation: false,
				price: 0
  		}
			vm.new_params =  angular.copy(vm.new_params_default)
  		vm.related_products = false
  		vm.slick_basic_settings = {
			    enabled: true,
			    autoplay: true,
			    draggable: false,
			    autoplaySpeed: 3000,
			    slidesToShow: 4,
			    method: {},
			    prevArrow: '<button type="button" class="arrow-prev slick-arrow"><i class="zmdi zmdi-long-arrow-left"></i></button>',
	        nextArrow: '<button type="button" class="arrow-next slick-arrow"><i class="zmdi zmdi-long-arrow-right"></i></button>',
	        responsive: [
	            {  breakpoint: 991,   settings: { slidesToShow: 3,  }  },
	            {  breakpoint: 767,   settings: { slidesToShow: 1, }   },
	            {  breakpoint: 479,   settings: { slidesToShow: 1, }   },
	        ],
			};
			vm.related_products_sett = angular.copy(vm.slick_basic_settings)
			vm.related_products_sett['slidesToShow'] = 3

			vm.selected_image = null

  		vm.submitNewForm = submitNewForm
  		vm.previewWithAr = previewWithAr
  		vm.isLoggedIn = sessionsServiceIsLoggedIn()

  		vm.preUploadImage = preUploadImage
  		vm.createProductReview = createProductReview
  		vm.updateQuantity = updateQuantity


			//init on load
			getProduct(PRODUCT_ID, {})
			cartItemsServiceIndex()
			getProductReviews()

			//functions
			function getProduct(id, params){
				artworksServiceShow(id, params)
					.then(function(res){
						if(res.status){
							vm.details = res.data
							getRelatedProducts({artist_ids: vm.details.artist_id})
						}
					})
			}

			function getRelatedProducts(params){
				artworksServiceIndex(params).then(function(res){
					if(res.status){
						vm.related_products = res.data
					}
					console.log(vm.related_products)
				})
			}

			function submitNewForm(){
				computePrize()
				if(!vm.isLoggedIn){
					// return	$ngConfirm({
					// 					title: 'Message',
					// 					content: "You must be signin to your account to purchase prints",
					// 				})	
					return $("#must-signin-modal").modal("show")
				}
				var params = generateCartItemParams(angular.copy(vm.new_params))
				cartItemsServiceCreate({cart_item: params})
					.then(function(res){
						if(res.status){
							vm.added_item = res.data
							$("#item-added-modal").modal("show")
							//call cart items again
							$rootScope.getCartItems()
						}
					})
			}

			function updateNewParamsVal(key, value){
				console.log(vm.new_params[key])
				console.log(key)
				console.log(value)
				vm.new_params[key] = value
			}

			function updateQuantity(increase){
				if(vm.new_params.quantity <= 0 && !increase) return
				vm.new_params.quantity = increase ? vm.new_params.quantity + 1 : vm.new_params.quantity - 1
			}

			function previewWithAr(){
				$('#preview-with-ar-modal').modal("show")
			}

			function generateCartItemParams(raw){
				var params = angular.copy(raw)
				params['material_id'] = params.material.id
	  		params['fixed_size_price_id'] = findObjInArray(params['material']['amounts'], 'fixed_size_id', params['fixed_size_id']).fixed_size_price_id

	  		params['frame_type_id'] = params['frame'].id
	  		if(params['frame_type_id']){
	  			params['fixed_size_frame_price_id'] = findObjInArray(params['frame']['amounts'], 'fixed_size_id', params['fixed_size_id']).fixed_size_frame_price_id
	  		}

				// return delEmptyKeys(params)
				if(params["fixed_size_frame_price_id"] == null){
					params["frame"] = false
				}
				return delEmptyKeys(params)
			}

			function delEmptyKeys(obj){
				angular.forEach(obj,function(v,k){
					if(v == null || v == undefined){
						delete obj[k]
					}
				})
				return obj
			}
			///reviews
			function getProductReviews(){
				return reviewsServiceIndex({artwork_id: PRODUCT_ID})
								.then(function(res){
									if(res.status){
										vm.reviews = res.data
									}
								})
			}

			function createProductReview(){
				return reviewsServiceCreate({review: vm.review_params})
								.then(function(res){
									if(res.status){
										// vm.reviews.push(res.data)
										vm.reviews.unshift(res.data)
										vm.review_params = angular.copy(vm.review_params_default)
										vm.selected_image = null
									}else{
										$ngConfirm({title: 'Message', content: res.message})
									}
								})

			}

			function preUploadImage(file){
				if(!file){ return false}
    		var fileReader = new FileReader();
				fileReader.readAsDataURL(file);
				vm.selected_image = file
				fileReader.onload = function (e) {
				  var dataUrl = e.target.result;
				  var base64Data = dataUrl.substr(dataUrl.indexOf('base64,') + 'base64,'.length);
				  vm.review_params.image = base64Data
				  $scope.$apply()
				};
				$scope.$apply()
			}

			function computePrize(){
				var print_size_id = vm.new_params.fixed_size_id
				var print_size = _.find(vm.details.print_sizes, {id: print_size_id})
				var material_amount = vm.new_params.material ? _.find(vm.new_params.material.amounts, {fixed_size_id: print_size_id}) : false
				var material_prize = material_amount ? parseFloat(material_amount.price ) : 0
				var frame_type_amount = vm.new_params.frame ? _.find(vm.new_params.frame.amounts, {fixed_size_id: print_size_id}) : false
				var frame_type_prize = frame_type_amount ? parseFloat(frame_type_amount.price) : 0
				var is_install = vm.new_params.installation
				var installation_amount = vm.new_params.installation ? parseFloat(findInstallationAmount(print_size).price) : 0
				var quantity = vm.new_params.quantity
				var sum = _.sum([material_prize, frame_type_prize, installation_amount]) * quantity


				// console.log("print_size_id:",print_size_id)
				// console.log("print_size:",print_size)
				// // console.log("vm.new_params.material.amounts:",vm.new_params.material.amounts)
				// // console.log("vm.new_params.frame.amounts:",vm.new_params.frame.amounts)
				// console.log("material_prize:",material_prize)
				// console.log("frame_type_prize:",frame_type_prize)
				// console.log("installation_amount:",installation_amount)
				// console.log("quantity:",quantity)
				// console.log("sum:", sum)
				// console.log("sum:", sum * quantity)
				vm.computed_price = sum

			}

			function findInstallationAmount(print_size){
				var width = parseFloat(print_size.width)
				var height = parseFloat(print_size.height)
				var highest_side = width >= height ? width : height
				// console.log("highest_side:",highest_side)
				var installation_amount_obj = {}
				_.forEach(vm.details.installation_amounts, function(value) {
					// console.log("1:",parseFloat(value["min"]) <= highest_side)
					// console.log("2:",parseFloat(value["max"]) >= highest_side)
					if(parseFloat(value["min"]) <= highest_side && parseFloat(value["max"]) >= highest_side){
						installation_amount_obj = value
					}
				})
				return installation_amount_obj
			}






			//services
			function cartItemsServiceIndex(params){
				return cartItemsService.index(params)
								.then(function(res){
									return res
								})
			}
			function cartItemsServiceCreate(params){
				return cartItemsService.create(params)
								.then(function(res){
									return res
								})
			}

			function artworksServiceIndex(params){
				return artworksService.index(params)
								.then(function(res){
									return res
								})
			}
			function artworksServiceShow(id, params){
				return artworksService.show(id, params)
								.then(function(res){
									return res
								})
			}

			function sessionsServiceIsLoggedIn(){
				return sessionsService.isLoggedIn()
			}

			function reviewsServiceIndex(params){
				return reviewsService.index(params)
								.then(function(res){
									return res
								})
			}
			function reviewsServiceShow(id,params){
				return reviewsService.show(id,params)
								.then(function(res){
									return res
								})
			}
			function reviewsServiceCreate(params){
				return reviewsService.create(params)
								.then(function(res){
									return res
								})
			}

			//general
			//find object in array by key and value


			$scope.$watch('product.new_params', function() {
	       computePrize()
	    },true);



			$scope.$watch('product.new_params.fixed_size_id', function(n) {
				console.log(n)
				if(!vm.new_params.material && vm.details.materials){
					vm.new_params.material = vm.details.materials[0]
				}
	    },true);


			function findObjInArray(arr,key,val){
				var obj = {}
				angular.forEach(arr, function(v,k){
					if(v[key] == val){
						obj = v
					}
				})
				return obj
			}
			

		}
}());