(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('CartController', CartController)
		CartController.$inject = ['$ngConfirm', '$scope', '$rootScope', 'cartItemsService', 'cartsService']

		function CartController($ngConfirm, $scope, $rootScope, cartItemsService, cartsService){
			var vm = this;
			vm.last_order = {}
			//@TODO
			//Order Summary (list of all cart items)
			//sumbmit CHECKOUT
			//SUCCESS MESSAGE
			vm.actual_promo_code = null
			vm.cartItemDestroy = cartItemDestroy
			vm.applyPromoCode = applyPromoCode
			vm.cartItemUpdateByKey = cartItemUpdateByKey
			vm.checkout = checkout
			
			vm.wizard = {
				tabs: [
					{active: true, title: "Cart", cleared: false},
					{active: false, title: "Checkout", cleared: false},
					{active: false, title: "Order Confirmation", cleared: false}
				],
				setActive: function(index){
					if(index > 0){
						if(!vm.wizard.tabs[index - 1].cleared){return}
					}
					angular.forEach(vm.wizard.tabs, function(v, idx){
						v.active = false
						if(idx == index){
							v.active = true
						}
					})
				},
				goToStep2: function(){
					if(!$rootScope.cart_items.items){return}
					vm.wizard.tabs[0].cleared = true
					vm.wizard.setActive(1)
					$("html, body").animate({ scrollTop: 0 }, 100);
				},

				goToStep3: function(){
					var checkout_loading = $ngConfirm({
						content: "Please Wait...",
						closeIcon: false,
						title: "<span class='text-center'>Submitting</span>",
					})
					checkout()
						.then(function(res){
							checkout_loading.close()
							if(res.status){
								vm.wizard.tabs[1].cleared = true
								vm.wizard.setActive(2)
								vm.last_order = res.data
							}else{
								$ngConfirm({
									title: "Message",
									content: res.message
								})
							}
							
						},function(err){
							checkout_loading.close()
						})
						$("html, body").animate({ scrollTop: 0 }, 100);
				}

			}


			//init
			// getCartItems()
			// 



			//functions

			//get all cart items
	    // function getCartItems(params){
	    // 	return cartItemsServiceIndex(params)
	    // 					.then(function(res){
	    // 						if(res.status){
	    // 							$rootScope.cart_items = res.data
	    // 							if(!res.data){$rootScope.cart_items = []}
	   	//  						}else{
	   	//  							$rootScope.cart_items = []
	   	//  						}

	    // 					})
	    // }

	    function cartItemDestroy(item){
	    	$ngConfirm({
	    		content: '<p>This will permanently delete the record. Continue?</p>',
	    		buttons: {
	    			continue:{
		    			btnClass: 'btn-primary',
		    			action: function(scope, button){
								cartItemsServiceDestroy(item.id)
								.then(function(res){
									if(res.status){
										$rootScope.getCartItems()
										return	$ngConfirm('Deleted')
									}
								})
		    			}
	    			},
	    			close: function(scope, button){}

	    		}
	    	})
	    }

	    function applyPromoCode(){
	    	return cartsServiceCreate({cart:{actual_promo_code: vm.actual_promo_code}})
	    					.then(function(res){
	    						if(res.status){
	    							$ngConfirm({
	    								title: "Message",
	    								content: "Promo code submitted!"
	    							})
	    							vm.actual_promo_code = null
	    							return $rootScope.getCartItems()
	    						}else{
	    							$ngConfirm({
	    								title: "Message",
	    								content: res.message
	    							})
	    						}
	    					})
	    }

	    vm.updating = false

	    function cartItemUpdateByKey(item, key, val){
	    	item.updating = true
	    	var params = angular.copy(item)
	    	params[key] = val
	    	if(params['quantity'] <= 0){return}
	    	cartItemUpdate(item, {cart_item: params} )
	    		.then(function(res){
	    			if(res.status){
	    				cartItemsServiceIndex({})
	    					.then(function(res){
	    						if(res.status){
	    							$rootScope.cart_items = angular.merge($rootScope.cart_items, res.data)
	    						}
	    						item.updating = false
	    					})
	    			}else{
	    				item.updating = false
	    			}
	    		},function(){item.updating = false})

	    }

	    function cartItemUpdate(item, params){
	    	return cartItemsServiceUpdate(item.id, params)
	    					.then(function(res){
	    						return res
	    					})
	    }

			//checkout -> next step (2)
			//Payment Method Add

			//Shippong Method Add

			function checkout(){
				// console.log("$rootScope.bank_details_default:",$rootScope.bank_details_default)
				// console.log("$rootScope.getDefaultPaymentMethod:",$rootScope.getDefaultPaymentMethod())
				// console.log("$rootScope.shipping_details_default:",$rootScope.shipping_details_default)
				// console.log("$rootScope.getDefaultShippingDetails:",$rootScope.getDefaultShippingDetails())
				// console.log("$rootScope.cart_items.id:", $rootScope.cart_items.id)
				var params = {
											user_shipping_detail_id: $rootScope.shipping_details_default.id,
											user_bank_detail_id: $rootScope.bank_details_default.id,
											card_id: $rootScope.bank_details_default.card_id,
											status: "checkout"
										}
				var valid = true
				angular.forEach(params, function(v,k){
					if(v == null || v == undefined){valid = false}
				})
				if(valid){
					return cartsServiceCreate({cart: params})
						.then(function(res){
							return (res)
						})
				}else{
					$ngConfirm({title: "Message", content: "Invalid Payment Method or Shipping Details"})
				}
				
			}

			//services
	    function cartItemsServiceIndex(params){
	    	return cartItemsService.index(params)
	    					.then(function(res){
	    						return res
	    					})
	    }

	    function cartItemsServiceDestroy(id){
	    	return cartItemsService.destroy(id)
	    					.then(function(res){
	    						return res
	    					})
	    }
	    function cartItemsServiceUpdate(id,params){
	    	return cartItemsService.update(id, params)
	    					.then(function(res){
	    						return res
	    					})
	    }

	    function cartsServiceCreate(params){
	    	return cartsService.create(params)
	    					.then(function(res){
	    						return res
	    					})
	    }


	    //
	    var cart_page = $('#cart-page')
			var wizard = cart_page.find('.cart-wizard')
			var steps = wizard.find('.step a')
			var step_content = wizard.find('.step-content')


			

			// function showActiveStep(){
			//   step_content.hide()
			//   wizard.find(".step-content.active").show()
			// }

			// showActiveStep();

			// cart_page.find('.step a').on('click',function(event){
			//   event.preventDefault()
			//   var target = $(this).attr('href').replace('#','')
			//   steps.removeClass('active')
			//   step_content.removeClass('active')
			//   $(this).addClass('active')
			//   $('#'+target).addClass('active')
			//   showActiveStep()
			// })

		}

})();















