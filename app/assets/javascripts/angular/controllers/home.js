(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('HomeController', HomeController)
		HomeController.$inject = ['$ngConfirm', '$scope', '$http', 'categoriesServiceV2', 'userFavoritesService']

		function HomeController($ngConfirm, $scope, $http, categoriesServiceV2, userFavoritesService){
			var vm = this;
			vm.now = Date.now()
			vm.top_favs = false
			vm.categories = false

			vm.bannered_contents = false
			vm.featured_artworks = false
			vm.featured_categories = false

			vm.slick_basic_settings = {
			    enabled: true,
			    autoplay: true,
			    draggable: false,
			    autoplaySpeed: 3000,
			    slidesToShow: 4,
			    method: {},
			    prevArrow: '<button type="button" class="arrow-prev slick-arrow"><i class="zmdi zmdi-long-arrow-left"></i></button>',
	        nextArrow: '<button type="button" class="arrow-next slick-arrow"><i class="zmdi zmdi-long-arrow-right"></i></button>',
	        responsive: [
	            {  breakpoint: 991,   settings: { slidesToShow: 3,  }  },
	            {  breakpoint: 767,   settings: { slidesToShow: 1, }   },
	            {  breakpoint: 479,   settings: { slidesToShow: 1, }   },
	        ],
			};


			vm.top_favs_settings = angular.copy(vm.slick_basic_settings)
			vm.cat_settings = angular.copy(vm.slick_basic_settings)
			vm.cat_settings['slidesToShow'] = 3
			vm.home_slider_sett =  angular.copy(vm.slick_basic_settings)
			vm.home_slider_sett['slidesToShow'] = 1
			vm.home_slider_sett['fade'] = true
			vm.home_slider_sett['rtl'] = true
			vm.home_slider_sett['dots'] = true

			//init
			getCategories({source: 'microsite'})


			//functions

			function getCategories(params){
				categoriesServiceV2Index(params)
					.then(function(res){
						if(res.status){
							vm.bannered_contents = res.data.bannered_contents
							vm.featured_artworks = res.data.featured_artworks
							vm.featured_categories = res.data.featured_categories
							setTimeout(function(){
								$('#home_carousel').bsTouchSlider();
							},1000)
						}
					})
			}



			//services
			function categoriesServiceV2Index(params){
				return categoriesServiceV2.index(params)
								.then(function(res){
									return res
								})
			}


			//top favorites
			//categories

		}

})();















