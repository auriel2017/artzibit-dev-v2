(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('MyProfileController', MyProfileController)
		MyProfileController.$inject = ['$ngConfirm', '$scope', '$http', 'usersService', 'registrationsService']

		function MyProfileController($ngConfirm, $scope, $http, usersService, registrationsService){
			var vm = this;
			vm.current_user = false
			vm.user_form = {}
			vm.change_pass_form_default = {old_password: '', new_password: '', cnew_password: ''}
			vm.change_pass_form = angular.copy(vm.change_pass_form_default)

			//scope's functions
			vm.preUpdateCurrentUser = preUpdateCurrentUser
			vm.submitCurrentUserForm = submitCurrentUserForm

			vm.preChangePass = preChangePass
			vm.submitChangePass = submitChangePass



			//init on load
			getCurrentUserDetails()

			//=================================
			//Controller's functions

			//My Profile
			function getCurrentUserDetails(){
				return usersService.currentUserDetails()
								.then(function(res){
									vm.current_user = res.data
									return res.data
								})
			}

			function preUpdateCurrentUser(item){
				$scope.my_profile_form.reset()
				vm.user_form = angular.copy(item)
				showModal('my-profile-modal')
			}

			function submitCurrentUserForm(){
				return registrationsServiceUpdate(vm.current_user.id, vm.user_form)
								.then(function(res){
									if(res.status){
										// vm.current_user = $.extend(vm.current_user, res.data)
										_.merge(vm.current_user, res.data)
										// $ngConfirm({content: 'Success', title: '', backgroundDismiss: true, closeIcon: false})
										$ngConfirm('Success')
									}else{
										$ngConfirm('Something went wrong')
									}
									$('.modal').modal('hide')
								})
			}

			function registrationsServiceUpdate(id, params){
				return registrationsService.update(id, {user: params })
								.then(function(res){
									return res
								})
			}

			function preChangePass(){
				vm.change_pass_form = angular.copy(vm.change_pass_form_default)
				vm.change_pass_form.id = vm.current_user.id
				showModal('change-password-modal')
			}

			function submitChangePass(){
				return registrationsServiceUpdate(vm.change_pass_form.id, vm.change_pass_form)
								.then(function(res){
									if(res.status){
										$scope.change_pass_form.reset()//reset for angular-valiator
										$ngConfirm('Updated')
										$('.modal').modal('hide')
									}else{
										$ngConfirm(res.message)
									}
									
								})
			}


			//GENERRAL
			function showModal(id){ 
				$('#'+id).modal('show') 
			}


		}

})();















