(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('MyOrdersController', MyOrdersController)
		MyOrdersController.$inject = ['$ngConfirm', '$scope', 'ordersService']

		function MyOrdersController($ngConfirm, $scope, ordersService){
			var vm = this;
			vm.orders = false
			vm.order_selected = false	
			vm.showOrderDetails = showOrderDetails	

			//init on load
			getOrders()

			//=================================
			//Controller's functions

			//My Orders
			function getOrders(params){
				return ordersService.index(params)
								.then(function(res){
									vm.orders = res.data
									return res.data
								})
			}

			function showOrderDetails(item, params){
				console.log("item:", item)
				vm.order_selected = false	
				$("#order-selected-modal").modal("show")
				ordersServiceShow(item.id, params)
					.then(function(res){
							if(res.status){
								vm.order_selected = res.data
							}else{
								vm.order_selected = {}
							}
						
					})
			}

			//services
			function ordersServiceShow(id, params){
				return ordersService.show(id, params)
								.then(function(res){
									return res
								})
			}
			
			//GENERRAL
			function showModal(id){ 
				$('#'+id).modal('show') 
			}


		}

})();















