(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('PaymentMethodsController', PaymentMethodsController)
		PaymentMethodsController.$inject = ['$ngConfirm', '$scope', '$rootScope', 'userBankDetailsService',]

		function PaymentMethodsController($ngConfirm, $scope, $rootScope, userBankDetailsService){
			var vm = this;

			vm.banks = false
			vm.valid_card_keys = ['number', 'exp_month', 'exp_year', 'cvc', 'name']
			vm.bank_form_default = {
				form_type: 0, //{0: create, 1: update}
				card_id: 1,
				delete: false,
				is_default: false,
				number: '',
				exp_month: '',
				exp_year: '',
				cvc: '',
				email: '',
			}
			vm.bank_form = angular.copy(vm.bank_form_default)
			vm.submitting_form = false

			vm.stripe = null
			vm.elements = null
			vm.card = null

			//rootScope
			$rootScope.bank_details_default = {}
			$rootScope.getDefaultPaymentMethod = getDefaultPaymentMethod
			
			//scope's functions
			vm.preCreateBankForm = preCreateBankForm
			vm.submitBankForm = submitBankForm
			vm.preUpdateBankForm = preUpdateBankForm
			vm.destroyBankItem = destroyBankItem
			vm.setDefaultBank = setDefaultBank


			//init on load
			getBankDetails()
			initStripeForm()



			//=================================
			//Controller's functions


			//Payment Methods

			//update
			function preUpdateBankForm(item){
				vm.bank_form = angular.copy(item)
				vm.bank_form.form_type = 1
				// $scope.bank_form.reset()				
				showModal('payment-method-modal')
			}


			//create
			function preCreateBankForm(){
				// vm.bank_form = angular.copy(vm.bank_form_default)
				// vm.bank_form.form_type = 0
				// $scope.bank_form.reset()
				initStripeForm()
				showModal('payment-method-modal')
			}
			
			function submitBankForm(){
				if(vm.bank_form.form_type == 0){
					var params = cardParamsValidate(vm.bank_form)
					vm.stripe.createToken(vm.card).then(function(res){
						// Handle result.error or result.token	
						if(res.token){
							var bank_params = {user_bank_detail: {number:  res.token.id} }
							userBankDetailsServiceCreate(bank_params).then(function(res){
								if(res){
									$ngConfirm("Record createad!")
									getBankDetails()
								}
								vm.submitting_form = false
								return $('.modal').modal('hide')
							})	
						}
						vm.submitting_form = false
					})
					.catch(function (err) {
		        if (err.type && /^Stripe/.test(err.type)) {
		          console.log('Stripe error: ', err.message)
		        }
		        else {
		          console.log('Other error occurred, possibly with your API', err.message)
		        }
		        vm.submitting_form = false
		      })
					
				}else if(vm.bank_form.form_type == 1){
					 // return userBankDetailsServiceUpdate(vm.bank_form).then(function(res){
						// 			$ngConfirm("Record updated!")
						// 			getBankDetails()
						// 			$('.modal').modal('hide')
						// 		})
				}
			}

			function userBankDetailsServiceCreate(params){
				return userBankDetailsService.create(params)
								.then(function(res){
									if(res.status){
										return res.data
									}else{
										$ngConfirm(res.message)										
									}
								})
			}

			function cardParamsValidate(card_params){
				var card_params_keys = Object.keys(card_params)
				angular.forEach(card_params, function(v,k){
					if(vm.valid_card_keys.includes(k) == false){
						delete card_params[k]
					}
				})
				return card_params
			}

			function userBankDetailsServiceUpdate(id, params){
				params['card_id'] = params.id
				return userBankDetailsService.update(id, params)
								.then(function(res){
									return res
								})
			}

			function getBankDetails(params){
				return userBankDetailsService.index(params)
								.then(function(res){
									if(res.status){
										vm.banks = res.data
										$rootScope.bank_details_default = getDefaultPaymentMethod()
										return res.data
									}

								})
			}

			function userBankDetailsServiceDestroy(params){
				// params['card_id'] = params.cart_id
				params['delete'] = true
				return userBankDetailsService.destroy({user_bank_detail: params})
								.then(function(res){
									return res.data
								})
			}

			function destroyBankItem(item, index){
				$ngConfirm({
					content: '<p> This will permanently delete record. Continue?<p>',
					buttons:{
						continue: {
							btnClass: 'btn-primary',
							action: function(scope, button){
								// userBankDetailsServiceDestroy(item)
								// 	.then(function(res){
								// 		vm.banks.splice(index, 1)
								// 		getBankDetails()
								// 		$ngConfirm('Record deleted')
								// 	})
								var params = {user_bank_detail: {card_id: item.card_id, to_delete: true}}
								userBankDetailsServiceUpdate(item.id, params)
									.then(function(res){
										if(res.status){
											vm.banks.splice(index, 1)
											getBankDetails()
											$ngConfirm('Record deleted')
										}
									})
							}
						},
						close: function(scope,button){}
					}
				})
				
			}

	    function setDefaultBank(item){
	    	$ngConfirm({
	    		content: '<p>Make this as default Payment Method?</p>',
	    		buttons: {
	    			continue:{
		    			btnClass: 'btn-primary',
		    			action: function(scope, button){
		    				var params = {user_bank_detail: {card_id: item.card_id, is_default: true} }
								userBankDetailsServiceUpdate(item.id, params)
									.then(function(res){
										if(res.status){
											getBankDetails()
											return	$ngConfirm('Updated')
										}
									},function(){
									})
		    			}
	    			},
	    			close: function(scope, button){}

	    		}
	    	})
	    }

	    function getDefaultPaymentMethod(){
	    	var data = {}
	    	angular.forEach(vm.banks, function(v,k){
	    		if(v['is_default'] == true){
	    			data = v
	    		}
	    	})
	    	return data
	    }

	    //stripe js
			function createStipeClient(key){
				// Create a Stripe client
				return Stripe(key);
			}

			function createStripeElem(item){
				// Create an instance of Elements
				return item.elements();
			}

			
			function initStripeForm(){
				vm.stripe = createStipeClient(PUBLISHABLE_KEY)
				vm.elements = createStripeElem(vm.stripe)
				// Create an instance of the card Element
				vm.card = vm.elements.create('card', {hidePostalCode: true});
				// Add an instance of the card Element into the `card-element` <div>
				vm.card.mount('#card-element');

				// Handle real-time validation errors from the card Element.
				vm.card.addEventListener('change', function(event) {
				  var displayError = document.getElementById('card-errors');
				  if (event.error) {
				    displayError.textContent = event.error.message;
				  } else {
				    displayError.textContent = '';
				  }
				});
			}

	    
			//GENERRAL
			function showModal(id){ 
				$('#'+id).modal('show') 
			}

			
		}

})();















