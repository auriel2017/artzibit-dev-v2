(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('MyArtworksController', MyArtworksController)
		MyArtworksController.$inject = ['$ngConfirm', '$scope', '$rootScope','artworksService']

		function MyArtworksController($ngConfirm, $scope, $rootScope, artworksService){
			var vm = this;
			$rootScope.my_artwork_page = true
			vm.artworks = []
			vm.artworks_public = false
			vm.artworks_private = false

			//scope's functions
			vm.destroyArtworkDetails = destroyArtworkDetails



			//init on load
			getArtworkPublic()
			getArtworkPrivate()


			//=================================
			//Controller's functions

			//My Artworks
			function getArtworkPublic(){
				artworkServiceIndex({marketplace: false, privacy: 'public'})
					.then(function(res){ 
						if(res.status){
							vm.artworks_public = res.data
						}
					})
			}
			function getArtworkPrivate(){
				artworkServiceIndex({marketplace: false, privacy: 'private'})
					.then(function(res){ 
						if(res.status){
							vm.artworks_private = res.data
						}
					})
			}
			function artworkServiceIndex(params){
				return artworksService.index(params)
								.then(function(res){
									return res
								})
			}
			function destroyArtworkDetails(item, idx){
				$ngConfirm({
	    		content: '<p>This will permanently delete the record. Continue?</p>',
	    		buttons: {
	    			continue:{
		    			btnClass: 'btn-primary',
		    			action: function(scope, button){
								artworksServiceDestroy(item.id)
									.then(function(res){
										if(res.status){
				    					var types = {'public': vm.artworks_public, 'private': vm.artworks_private}
				    					types[item.privacy].splice(idx, 1)
											return	$ngConfirm('Deleted')
										}
									})
		    			}
	    			},
	    			close: function(scope, button){}

	    		}
	    	})
			}

			function artworksServiceDestroy(id, params){
				return artworksService.destroy(id, params)
								.then(function(res){
									return res
								})
			}


			//GENERRAL
			function showModal(id){ 
				$('#'+id).modal('show') 
			}


		}

})();















