(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('ShippingDetailsController', ShippingDetailsController)
		ShippingDetailsController.$inject = ['$ngConfirm', '$scope', '$rootScope', 'userShippingDetailsService']

		function ShippingDetailsController($ngConfirm, $scope, $rootScope, userShippingDetailsService){
			var vm = this;
			///shipping details
			vm.shipping_details = false
			vm.sd_form_default = {
				form_type: 0, //{0: create, 1: update}
				first_name: '',
				last_name: '',
				phone_number: '',
				email: '',
				address: '',
				city: '',
				postal_code: '',
				country: ''
			}
			vm.sd_form = angular.copy(vm.sd_form_default)
			//rootScope
			$rootScope.shipping_details_default = {}
			$rootScope.getDefaultShippingDetails = getDefaultShippingDetails



			//scope's functions
			vm.preCreateShippingDetails = preCreateShippingDetails
			vm.setDefaultShipping = setDefaultShipping
			vm.preUpdateShippingDetails = preUpdateShippingDetails
			vm.submitShippingForm = submitShippingForm
			vm.destroyShippingDetails = destroyShippingDetails


			//init on load
			getShippingDetails()


			//=================================
			//Controller's functions

			//Shipping Details
			function getShippingDetails(params){
				return userShippingDetailsService.index(params)
								.then(function(res){
									if(res.status){
										vm.shipping_details = res.data
										$rootScope.shipping_details_default = getDefaultShippingDetails()
										return res.data
									}
								})
			}

			function preCreateShippingDetails(){
				vm.sd_form = angular.copy(vm.sd_form)
				vm.sd_form.form_type = 0
				$scope.sd_form.reset()
				showModal('shipping-details-modal')
			}

			function preUpdateShippingDetails(item){
				vm.sd_form = angular.copy(item)
				vm.sd_form.form_type = 1
				showModal('shipping-details-modal')
			}

			function submitShippingForm(){
				if(vm.sd_form.form_type == 0){
					//create
					var params = angular.copy(vm.sd_form)
					params['detailed_addr'] = params['address']
					return userSDServiceCreate(params)
									.then(function(res){
										if(res.status){
											getShippingDetails()
											$('.modal').modal('hide')
											return $ngConfirm('Record created!')
										}else{
											$ngConfirm({title: "Message", content: res.message})
										}
									})
				}else if(vm.sd_form.form_type == 1){
					//update
					return userSDServiceUpdate(vm.sd_form.id, vm.sd_form)
								.then(function(res){
									if(res.status){
										getShippingDetails()
										$('#shipping-details-modal').modal('hide')
										$ngConfirm('Updated')
									}else{
											$ngConfirm({title: "Message", content: res.message})
										}
								})
				}
				
			}

			function setDefaultShipping(item){
				$ngConfirm({
	    		content: '<p>Make this as default Shipping Method?</p>',
	    		buttons: {
	    			continue:{
		    			btnClass: 'btn-primary',
		    			action: function(scope, button){
								userSDServiceUpdate(item.id, {is_default: true})
								.then(function(res){
									getShippingDetails()
									return	$ngConfirm('Updated')
								})
		    			}
	    			},
	    			close: function(scope, button){}

	    		}
	    	})
			}

			function userSDServiceCreate(params){
				return userShippingDetailsService.create(params)
								.then(function(res){
									return res
								})
			}

			function userSDServiceUpdate(id, params){
				return userShippingDetailsService.update(id, params)
								.then(function(res){
									return res
								})
			}

			function destroyShippingDetails(item){
				$ngConfirm({
	    		content: '<p>This will permanently delete the record. Continue?</p>',
	    		buttons: {
	    			continue:{
		    			btnClass: 'btn-primary',
		    			action: function(scope, button){
								userShippingDetailsService.destroy(item.id)
								.then(function(res){
									getShippingDetails()
									return	$ngConfirm('Deleted')
								})
		    			}
	    			},
	    			close: function(scope, button){}

	    		}
	    	})
			}


			function getDefaultShippingDetails(){
	    	var data = {}
	    	angular.forEach(vm.shipping_details, function(v,k){
	    		if(v['is_default'] == true){
	    			data = v
	    		}
	    	})
	    	return data
	    }
			
			//GENERRAL
			function showModal(id){ 
				$('#'+id).modal('show') 
			}


		}

})();















