(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('TermsOfServiceController', TermsOfUseController)
		TermsOfUseController.$inject = ['$ngConfirm', '$scope', 'webRepositoriesService']

		function TermsOfUseController($ngConfirm, $scope, webRepositoriesService){
			var vm = this;
			vm.content = ''

			//init
			webRepositoriesServiceIndex({name: 'tos'})

			//functions

			//services
			function webRepositoriesServiceIndex(params){
				return webRepositoriesService.index(params)
								.then(function(res){
									console.log(res)
									if(res.status){
										vm.content = res.data.content
									}
								})
			}
		}

})();















