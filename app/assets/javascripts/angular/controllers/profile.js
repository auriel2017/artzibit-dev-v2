(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('ProfileController', ProfileController)
		ProfileController.$inject = ['$ngConfirm', '$scope', '$http']

		function ProfileController($ngConfirm, $scope, $http){
			var vm = this;
			vm.now = Date.now()
			vm.side_menu = [
				{text: 'My Profile'},
				{text: 'My Orders'},
				{text: 'Payment Methods'},
				{text: 'Shipping Details'},
				{text: 'Upload Artwork'},
				{text: 'My Artworks'}
			]
			vm.active_menu = vm.side_menu[0]
			vm.toggleActiveMenu = toggleActiveMenu
			vm.showModal = showModal

			
			getActiveMenuByUrl("active_menu")

			//=================================
			//Controller's functions

			function toggleActiveMenu(item){
				if(item.text == 'Upload Artwork'){
					return window.location.href = "artworks/new"
				}
				var active_menu = item.text.replace(" ","_").toLowerCase()
				window.history.pushState({}, '', '?active_menu='+active_menu)
				vm.active_menu = item
			}

			//GENERRAL

			function showModal(id){ 
				$('#'+id).modal('show') 
			}


			function getActiveMenuByUrl(name){
				var text = getUrlParams(name)
				if(!Object.values(getUrlParams(name)).length){ return }
				text = text.replace("#","")

				var selected_idx = 0
				if(text.length){
					text = text.replace("_"," ").capitalize();
					var side_menu_items = vm.side_menu.map(function(i){ return i.text })
					selected_idx = side_menu_items.indexOf(text)
					if(selected_idx < 0){ return }
				}
				toggleActiveMenu(vm.side_menu[selected_idx])

			}


		}

})();















