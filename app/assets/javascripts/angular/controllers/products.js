(function(){
	'use strict';
	angular
		.module('microsite')
		.controller('ProductsController', ProductsController);
		ProductsController.$inject = ['$scope','artworksService', '$rootScope', 'productFiltersService'];

		function ProductsController($scope, artworksService, $rootScope, productFiltersService){
			var vm = this;
			vm.artworks_loading = false
			vm.artworks = []
			vm.filters = {}
			vm.artists = false
			vm.categories = false
			vm.keyword = ''
			vm.artists_all = false
			vm.categories_all = false

			vm.triggerFilters = triggerFilters
			vm.selectAllFilters = selectAllFilters
			vm.checkIfCheckAll = checkIfCheckAll
			vm.clearFilters = clearFilters

			console.log("rootScope:", $rootScope)

			//on init
			// getArtworks({all: true});
			getFilterByUrl()
			getFilters();


	    function getFilters(params){
	    	return productFiltersServiceIndex(params)
	    					.then(function(res){
	    						if(res.status){
	    							vm.filters = res.data
	    							vm.artists = res.data[0]
	    							vm.categories = res.data[1]
	    						}
	    					})
	    }

	    function getArtworks(params) {
	    	if(vm.artworks_loading){return}
	    	vm.artworks_loading = true
        return artworkServiceIndex(params)
        				.then(function(res){
        					if(res.status){
        						vm.artworks = res.data
        					}
        					vm.artworks_loading = false
        				})
	    }

	    function triggerFilters(){
	    	var params = {
	    		category_ids: getSelectedItems(vm.categories.values),
	    		artist_ids: getSelectedItems(vm.artists.values),
	    		keyword: vm.keyword
	    	}
	    	//remove empty keys
	    	console.log(params)
	    	Object.keys(params).forEach((key) => (params[key] == null || !params[key]) && delete params[key]);
	    	setUrl(params)
	    	return getArtworks(params)
	    }

	    function getSelectedItems(items){
	    	var selected = []
	    	angular.forEach(items, function(v,k){
	    		if(v.selected){
	    			selected.push(v.id)
	    		}
	    	})
	    	return selected.join(",")
	    }

	    function selectAllFilters(value, items){
	    	angular.forEach(items, function(v,k){
	    		v.selected = value
	    	})
	    }
	    function checkIfCheckAll(type){
	    	var items = {
	    		'artists': vm.artists.values,
	    		'categories': vm.categories.values,
	    	}
	    	var not_selected = []
	    	angular.forEach(items[type], function(v,k){
	    		if(!v.selected){
	    			not_selected.push(v)
	    		}
	    	})
	    	vm[type+'_all'] = (not_selected.length > 0) ? false : true
	    }

	    function clearFilters(){
	    	var filters = [vm.artists, vm.categories]
	    	vm.artists_all = false
				vm.categories_all = false
				vm.keyword = ''
				angular.forEach(filters, function(v,k){
					angular.forEach(v.values, function(vv, kk){
						vv.selected = false
					})
				})
				return triggerFilters()
	    }

	    //services
	    function artworkServiceIndex(params){
	    	return artworksService.index(params)
            .then(function(res) {
            	return res
            })
	    }

	    function productFiltersServiceIndex(params){
	    	return productFiltersService.index(params)
	    					.then(function(res){
	    						return res
	    					})
	    }


	    //general
			function getFilterByUrl(){
				var url_params = getUrlParams()
				var params = {
					all: true,
	    		category_ids: url_params['category_ids'],
	    		artist_ids: url_params['artist_ids'],
	    		keyword: url_params['keyword']
	    	}
	    	var valid_params = []
	    	angular.forEach(params,function(v,k){ if(v){valid_params.push(v)} })
	    	if(valid_params.length > 1){
					return getArtworks(params)
	    	}else{
	    		getArtworks({all: true});
	    	}
			}


		}


})();