(function() {
    'use strict';
    angular
    	.module('microsite',['restangular','ngResource', 'cp.ngConfirm', 'countrySelect', 'angularValidator', 'ui.utils.masks', 'ngTagsInput', 'ngFileUpload', 'ngMask', 'ui.bootstrap', 'slickCarousel'])
    	.config(['RestangularProvider', function (RestangularProvider) {
			  // Add authenticity token for all requests
			  RestangularProvider.setRequestInterceptor(function(element, operation, route, url) {
			    if (['post', 'patch', 'put', 'delete', 'remove', 'get', 'DELETE'].indexOf(operation) !== -1) {
			      element[CSRF_TOKEN.name] = CSRF_TOKEN.value;
			    }
			    return element;
			  });
			  RestangularProvider.setDefaultHeaders({
			  	'X-User-Token': USER_TOKEN,
			  	'X-User-Email': USER_EMAIL,
			  });

			  // Configure base url
			  RestangularProvider.setBaseUrl(API_BASE_URL);
			}])
			// .config(function (stripeProvider) {
		 //    stripeProvider.setPublishableKey(PUBLISHABLE_KEY)
		 //  })
		 
})();
