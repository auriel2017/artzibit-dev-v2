(function(){
	'use strict';
	angular	
		.module('microsite')
		.factory('productFiltersService', productFiltersService)

	productFiltersService.$inject = ['Restangular']

	function productFiltersService(Restangular){
		var endpoint = Restangular.all('v2/filters')
		var service = {
			index: index
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}

	}

})();


