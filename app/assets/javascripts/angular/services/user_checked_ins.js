(function(){
	'use strict';
	angular
		.module('microsite')
		.factory('userCheckedInsService',userCheckedInsService)

	userCheckedInsService.$inject = ['Restangular']

	function userCheckedInsService(Restangular){
		var endpoint = Restangular.all('v1/user_checked_ins')
		var service = {
			index: index,
			show: show,
			update: update,
			destroy: destroy
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}
		function create(params){
			return endpoint.customPOST(params)
		}
		function show(id,params){
			return endpoint.customGET(id.toString(),params)
		}
		function update(id,params){
			return endpoint.customPATCH(id.toString(),params)
		}
		function destroy(id,params){
			return endpoint.customDELETE(id.toString(),params)
		}
	}

})();