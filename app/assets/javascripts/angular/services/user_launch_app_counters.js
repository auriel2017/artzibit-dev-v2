(function(){
	'use strict';
	angular
		.module('microsite')
		.factory('userLaunchAppCountersService',userLaunchAppCountersService)

	userLaunchAppCountersService.$inject = ['Restangular']

	function userLaunchAppCountersService(Restangular){
		var endpoint = Restangular.all('v1/user_launch_app_counters')
		var service = {
			index: index,
			show: show,
			update: update,
			destroy: destroy
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}
		function create(params){
			return endpoint.customPOST(params)
		}
		function show(id,params){
			return endpoint.customGET(id.toString(),params)
		}
		function update(id,params){
			return endpoint.customPATCH(id.toString(),params)
		}
		function destroy(id,params){
			return endpoint.customDELETE(id.toString(),params)
		}
	}

})();