(function(){
	'use strict';
	angular
		.module('microsite')
		.factory('userBankDetailsService',userBankDetailsService)

	userBankDetailsService.$inject = ['Restangular']

	function userBankDetailsService(Restangular){
		var endpoint = Restangular.all('v2/user_bank_details')
		var service = {
			index: index,
			create: create,
			show: show,
			update: update,
			destroy: destroy
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}
		function create(params){
			return endpoint.customPOST(params)
		}
		function show(id,params){
			return endpoint.customGET(id.toString(),params)
		}
		function update(id,params){
			return endpoint.one(id.toString()).patch(params)
		}
		// function update(params){
		// 	return endpoint.post(params)
		// }
		// function destroy(id,params){
		// 	return endpoint.customDELETE(id.toString(),params)
		// }
		function destroy(params){
			return endpoint.post(params)
		}
	}

})();