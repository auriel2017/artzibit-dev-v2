(function(){
	'use strict';
	angular	
		.module('microsite')
		.factory('cartItemsService', cartItemsService)

	cartItemsService.$inject = ['Restangular']

	function cartItemsService(Restangular){
		var endpoint = Restangular.all('v2/cart_items')
		var service = {
			index: index,
			show: show,
			edit: edit,
			update: update,
			create: create,
			destroy: destroy
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}
		
		function show(id, params){
			return endpoint.customGET(id.toString(),params)
		}

		function edit(id, params){
			return endpoint.customGET(id.toString() + '/edit',params)
		}

		function update(id,params){
			return endpoint.one(id.toString()).patch(params)
		}

		function create(params){
			return endpoint.customPOST(params)
		}

		function destroy(id, params){
			return endpoint.customDELETE(id.toString(),params)
		}

	}

})();


