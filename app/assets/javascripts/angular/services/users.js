(function(){
	'use strict';
	angular
		.module('microsite')
		.factory('usersService', usersService)

	usersService.$inject = ['Restangular']

	function usersService(Restangular){
		var endpoint = Restangular.all('v2/users')
		var service = {
			index: index,
			show: show,
			update: update,
			destroy: destroy,
			currentUserDetails: currentUserDetails
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}
		function show(id,params){
			return endpoint.customGET(id.toString(),params)
		}
		function update(id,params){
			return endpoint.one(id.toString()).patch(params)
		}
		function destroy(id,params){
			return endpoint.customDELETE(id.toString(),params)
		}

		function currentUserDetails(){
			// GET to http://www.google.com/1 You set the URL in this case
			return Restangular.oneUrl('current_user', 'api/v1/current_user_details').get();
		}
	}

})();