(function(){
	'use strict';
	angular	
		.module('microsite')
		.factory('categoriesService', categoriesService)

	categoriesService.$inject = ['Restangular']

	function categoriesService(Restangular){
		var endpoint = Restangular.all('v1/categories')
		var service = {
			index: index,
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}
	}

})();


