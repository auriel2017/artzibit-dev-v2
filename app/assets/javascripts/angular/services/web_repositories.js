(function(){
	'use strict';
	angular	
		.module('microsite')
		.factory('webRepositoriesService', webRepositoriesService)

	webRepositoriesService.$inject = ['Restangular']

	function webRepositoriesService(Restangular){
		var endpoint = Restangular.all('v2/web_repositories')
		var service = {
			index: index
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}

	}

})();


