(function(){
	'use strict';
	angular	
		.module('microsite')
		.factory('paymentsService', paymentsService)

	paymentsService.$inject = ['Restangular']

	function paymentsService(Restangular){
		var endpoint = Restangular.all('v2/payments')
		var service = {
			create: create,
		}

		return service

		function create(params){
			return endpoint.customPOST(params)
		}
		function update(id,params){
			return endpoint.one(id.toString()).put(params)
		}
	}

})();


