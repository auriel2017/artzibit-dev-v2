(function(){
	'use strict';
	angular	
		.module('microsite')
		.factory('ordersService', ordersService)

	ordersService.$inject = ['Restangular']

	function ordersService(Restangular){
		var endpoint = Restangular.all('v2/orders')
		var service = {
			index: index,
			show: show,
			create: create
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}
		function show(id, params){
			return endpoint.customGET(id.toString(),params)
		}

		function create(params){
			return endpoint.customPOST(params)
		}
	}

})();


