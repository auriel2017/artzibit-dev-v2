(function(){
	'use strict';
	angular	
		.module('microsite')
		.factory('artworkStylesService', artworkStylesService)

	artworkStylesService.$inject = ['Restangular']

	function artworkStylesService(Restangular){
		var endpoint = Restangular.all('v1/artwork_styles')
		var service = {
			index: index,
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}
	}

})();


