(function(){
	'use strict';
	angular
		.module('microsite')
		.factory('userShippingDetailsService',userShippingDetailsService)

	userShippingDetailsService.$inject = ['Restangular']

	function userShippingDetailsService(Restangular){
		var endpoint = Restangular.all('v2/user_shipping_details')
		var service = {
			create: create,
			index: index,
			show: show,
			update: update,
			destroy: destroy
		}

		return service

		function create(params){
			return endpoint.post(params)
		}
		function index(params){
			return endpoint.customGET('',params)
		}
		function show(id,params){
			return endpoint.customGET(id.toString(),params)
		}
		function update(id,params){
			return endpoint.one(id.toString()).patch(params)
		}
		function destroy(id,params){
			return endpoint.customDELETE(id.toString(),params)
		}
	}

})();