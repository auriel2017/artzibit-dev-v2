(function(){
	'use strict';
	angular	
		.module('microsite')
		.factory('reviewsService', reviewsService)

	reviewsService.$inject = ['Restangular']

	function reviewsService(Restangular){
		var endpoint = Restangular.all('v2/reviews')
		var service = {
			index: index,
			show: show,
			create: create,
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}
		function show(id, params){
			return endpoint.customGET(id.toString(),params)
		}
		function create(params){
			return endpoint.customPOST(params)
		}
	}

})();


