(function(){
	'use strict';
	angular	
		.module('microsite')
		.factory('categoriesServiceV2', categoriesServiceV2)

	categoriesServiceV2.$inject = ['Restangular']

	function categoriesServiceV2(Restangular){
		var endpoint = Restangular.all('v2/categories')
		var service = {
			index: index,
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}
	}

})();


