(function(){
	'use strict';
	angular
		.module('microsite')
		.factory('userFavoritesService',userFavoritesService)

	userFavoritesService.$inject = ['Restangular']

	function userFavoritesService(Restangular){
		var endpoint = Restangular.all('v2/user_favorites')
		var service = {
			index: index,
			create: create,
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}
		function create(params){
			return endpoint.customPOST(params)
		}
	}

})();