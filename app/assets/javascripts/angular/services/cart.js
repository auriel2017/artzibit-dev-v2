(function(){
	'use strict';
	angular	
		.module('microsite')
		.factory('cartsService', cartsService)

	cartsService.$inject = ['Restangular']

	function cartsService(Restangular){
		var endpoint = Restangular.all('v2/carts')
		var service = {
			create: create,
		}

		return service

		function create(params){
			return endpoint.customPOST(params)
		}
	}

})();


