(function(){
	'use strict';
	angular	
		.module('microsite')
		.factory('supportsService', supportsService)

	supportsService.$inject = ['Restangular']

	function supportsService(Restangular){
		var endpoint = Restangular.all('v2/supports')
		var service = {
			create: create
		}

		return service

		function create(params){
			return endpoint.post(params)
		}

	}

})();


