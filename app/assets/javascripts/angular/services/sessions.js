(function(){
	'use strict';
	angular
		.module('microsite')
		.factory('sessionsService', sessionsService)

	sessionsService.$inject = ['Restangular']

	function sessionsService(Restangular){
		var endpoint = Restangular.all('v2/users')
		var service = {
			isLoggedIn: isLoggedIn,
		}

		return service

		function isLoggedIn(){
			return USER_TOKEN ? true : false
		}
	}

})();