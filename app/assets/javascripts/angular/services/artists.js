(function(){
	'use strict';
	angular	
		.module('microsite')
		.factory('artistsService', artistsService)

	artistsService.$inject = ['Restangular']

	function artistsService(Restangular){
		var endpoint = Restangular.all('v1/artists')
		var service = {
			index: index
		}

		return service

		function index(params){
			return endpoint.customGET('',params)
		}

	}

})();


