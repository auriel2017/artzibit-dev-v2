class OrderPdf < Prawn::Document
  def initialize(order, view)
    super(top_margin: 70)
    @order = order
    @view = view
    order_number
    line_items
    summary
  end

  def order_number
    shipping_detail = @order.user_shipping_detail
    text "<b>Customer's Name:</b> #{@order.user.try(:name)}", size: 12, :inline_format => true
    text "<b>Receiver's Name:</b> #{shipping_detail.try(:name)}", size: 12, :inline_format => true
    text "<b>Mobile No:</b> #{shipping_detail.try(:phone_number)}", size: 12, :inline_format => true
    text "<b>Delivery Address:</b> #{shipping_detail.try(:detailed_addr) }", size: 12, :inline_format => true
    text"<b>Order</b> # #{@order.id}", size: 12, :inline_format => true
    text "<b>Order Date:</b> #{@order.created_at.strftime("%Y-%m-%d %H:%M")}", size: 12, :inline_format => true
  end

  def line_items
    move_down 20
    table line_item_rows, position: :center, cell_style: {size: 9, borders: [:left, :bottom]} do
      row(0).font_style = :bold
      row(0).border_width = 1
      row(0).borders = [:top, :bottom, :left, :right]
      columns(2..5).align = :right
      column(5).border_width = 1
      column(5).borders = [:top, :bottom, :left, :right]
      self.header = true
      self.column_widths = [40,300,50,50,50,50]
    end
  end

  def summary
    table summary_rows, position: :center, cell_style: {size: 9, borders: [:left, :bottom]} do
      column(0).font_style = :bold
      columns(0..1).align = :right
      column(1).borders = [:top, :bottom, :left, :right]
      self.header = false
      self.column_widths = [490,50]
    end
  end

  def line_item_rows
    [["Code", "Description", "Sell", "Qty", "BO", "Total"]] +
    @order.cart.cart_items.map do |item|
      [item.artwork.id, item_desc(item), price(item.product_amount),
        item.quantity, nil, price(item.total_product_amount)]
    end
  end

  def price(num)
    @view.number_to_currency(num)
  end

  def item_desc item
    make_table([ ["Name: #{item.artwork.name}"], ["Size: #{item.size}"],
      ["Material: Smooth Fine Art Paper"], ["Installation: #{item.installation}"] ], width: 300,
      cell_style: {border_width: 0, size: 7})
  end

  def summary_rows
    [["Installation Charge",price(@order.cart.installation_fees)],
     ["Delivery Charge", price(@order.cart.delivery_charge_amount)],
     ["TOTAL AMOUNT", price(@order.cart.total_price)]]
  end
end