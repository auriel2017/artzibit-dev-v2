class WebRepository < ActiveRecord::Base
  validates :name, :content_html, presence: true
  validates :name, uniqueness: true

  def v2_format
    {
    	name: name,
      content: content_html,
      page: page,
      category: category
    }
  end
end
