class OrderAssignment < ActiveRecord::Base
  belongs_to :order
  belongs_to :admin_user

  after_create :notify_assignee

  def notify_assignee
    OrderMailer.notify_assignment(order, admin_user.email).deliver_now
  end
end
