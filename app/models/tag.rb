class Tag < ActiveRecord::Base
  scope :by_name, -> {order(:name)}
  has_many :taggables
  has_many :artworks, through: :taggables

  validates :name, presence: true, uniqueness: true

  def artworks_count
    artworks.count
  end
  # For Content Repository
  def content_name
    "Tag-#{id}"
  end
end
