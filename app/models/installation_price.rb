class InstallationPrice < ActiveRecord::Base
  validates :price, :effectivity_date, presence: true
  belongs_to :installation_range
  def self.current_price
    ins_pri = InstallationPrice.where("effectivity_date <= ?", Time.now).order(:effectivity_date).last
    ins_pri
  end
  def name
    ins_ran = InstallationRange.find_by(id: installation_range_id)
    "$#{price}: Artwork size between (#{ins_ran.try(:min_size)}cm - #{ins_ran.try(:max_size)}cm)"
  end
end
