class OrderHistory < ActiveRecord::Base
  STATUS = {"order"=> "Order submitted", "printing"=> "Printing in progress",
  "ready_to_deliver"=> "Ready to Deliver", "on_delivery"=> "On Delivery",
  "completed" => "Order delivered"}
  STATUS_V2 = {"order"=> "Submitted", "printing"=> "Printing",
  "ready_to_deliver"=> "Ready to Deliver", "on_delivery"=> "On Delivery",
  "completed" => "Delivered"}

  belongs_to :order
  validates :order_id, :status, presence: true
  # validates :status, uniqueness: {scope: :order_id}

  attr_accessor :processed_date_only, :processed_time
  after_save :change_order_current_status

  def v1_format
    {
      id: id,
      status: OrderHistory::STATUS[status],
      date: processed_at.strftime("%Y-%m-%d"),
      time: processed_at.strftime("%I:%M%p")
    }
  end

  def formatted_processed_at
    "#{processed_at.strftime("%Y-%m-%d")} #{processed_at.strftime("%I:%M%p")}"
  end

  private
  def change_order_current_status
    current_order = Order.find_by(id: order.id)
    current_order.current_status = status
    current_order.save(validate: false)
  end
end