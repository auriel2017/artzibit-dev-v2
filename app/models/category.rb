class Category < ActiveRecord::Base
  scope :by_name, -> {order(:name)}
  scope :by_sequence, -> {order(:sequence)}
  has_many :artworks
  has_attached_file :cover_image
  attr_accessor :remove_image
  validates_attachment_content_type :cover_image,
    content_type: /^image\/(jpg|jpeg|pjpeg|png|x-png|gif)$/, unless: "cover_image.blank?"
  validates :name, presence: true
  validates :name, uniqueness: true
  validates :sequence, presence: true, uniqueness: :true
  before_save :remove_photo, if: "remove_image == '1'"

  # Naming Functions
  def image_url
    "https:#{cover_image.url}"
  end

  # For Content Repository
  def content_name
    "Category-#{id}"
  end

  # API Functions
  def v1_format
    {
      id: id,
      name: name,
      description: description,
      image: cover_image
    }
  end
  def v2_format controller = ""
    if controller == "filter"
      {
        id: id,
        name: name
      }
    else
      {
        type: "category",
        details: {
          ids: id.to_s,
          banner_image: image_url,
          name: name,
          keyword: "category_ids",
          sequence: sequence
        }
      }
    end
  end
  private
  def remove_photo
    self.cover_image.destroy
    true
  end
end