class FixedSizePrice < ActiveRecord::Base
  belongs_to :fixed_size
  belongs_to :material
  has_many :cart_items
  validates :price, :effectivity_date, presence: true

  scope :by_date, -> {order(effectivity_date: :desc)}

  # FIeld Functions
  def name
    "$ #{price.round(2)},
      Effectivity Date: #{effectivity_date.strftime("%b %d, %Y %H:%M:%S")}"
  end
  # def v2_format
  #   {
  #     id: id,
  #     price: fixed_size.current_material_price(material_id),
  #     material: material_id,
  #     size: fixed_size_id
  #   }
  # end
end