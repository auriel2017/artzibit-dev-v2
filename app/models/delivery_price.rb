class DeliveryPrice < ActiveRecord::Base
  belongs_to :delivery_range
  validates :price, :effectivity_date, :delivery_range_id, presence: true
  def self.current_price
    price = DeliveryPrice.where("effectivity_date <= ?", Time.now).order(:effectivity_date).last
    price
  end
  def name
    del_range = DeliveryRange.find_by(id: delivery_range_id)
    "$#{price}: Order with Artwork size between (#{del_range.try(:min_size)}cm - #{del_range.try(:max_size)}cm) #{' - with Installation' if del_range.try(:with_installation)}"
  end
end