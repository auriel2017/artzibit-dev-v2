class OrderCourier < ActiveRecord::Base
  belongs_to :order
  validates :order_id, :name, :tracking_no, presence: true
  validates :name, uniqueness: {scope: :tracking_no}
end
