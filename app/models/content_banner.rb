class ContentBanner < ActiveRecord::Base
  has_many :content_banner_dtls, dependent: :destroy

  scope :active, -> {where(active: true).reorder('sequence asc')}
  scope :inactive, -> {where(active: false)}

  has_attached_file :banner_image, s3_credentials: {
    s3_protocol: :https }
  validates_attachment_content_type :banner_image,
    :content_type => /^image\/(jpg|jpeg|png)$/,
    :size => {in: 0..75.kilobytes}

  validates :name, presence: true, uniqueness: true
  validates :sequence, presence: true, uniqueness: :true
  validate :uniqueness_content_banner_dtls
  validate :artwork_should_have_one_condition

  accepts_nested_attributes_for :content_banner_dtls, allow_destroy: true
  attr_accessor :models

  # Validation Functions
  def uniqueness_content_banner_dtls
    errors.add(:base, "Conditions have duplicate records") unless (self.
      content_banner_dtls.map{|x| x.contentable_name}.uniq.count == self.
      content_banner_dtls.map{|x| x.contentable_name}.count)
  end
  def artwork_should_have_one_condition
    errors.add(:base, "Artwork should have 1 condition") if self.content_banner_dtls.reject{|x| x._destroy == true}.size > 1 \
      and self.content_banner_dtls.select{|x| x.contentable_type == "Artwork"}.count > 1
  end

  # Field Functions
  def banner_image_url
    if banner_image.blank?
      nil
    else
      "https:#{banner_image.url}"
    end
  end
  def conditions
    ContentBannerDtl.where(content_banner_id: id)
  end
  def condition
    ContentBannerDtl.where(content_banner_id: id).first
  end
  def keyword
    case conditions.first.contentable_type
      when "Artwork"
        "artwork_id"
      when "Category"
        "category_ids"
      when "Tag"
        "tag_ids"
      when "User"
        "artist_ids"
    end
  end
  def redirect_to_artwork
    Artwork.find_by id: condition.contentable_id
  end
  def redirect_tos
    details = Array.new
    ContentBannerDtl.where(content_banner_id: id).each do |condition|
      case condition.contentable_type
        when "Artwork"
          details << "Artwork:#{Artwork.where(id: condition.contentable_id).pluck(:name).first}"
        when "Category"
          details << "Category:#{Category.where(id: condition.contentable_id).pluck(:name).first}"
        when "Tag"
          details << "Tag:#{Tag.where(id: condition.contentable_id).pluck(:name).first}"
        when "User"
          details << "User:#{User.where(id: condition.contentable_id).pluck(:name).first}"
      end
    end
    details.join(", ")
  end
  def redirect_to_ids
    details = Array.new
    conditions.each do |condition|
      case condition.contentable_type
        when "Artwork"
          details << "#{Artwork.where(id: condition.contentable_id).pluck(:id).
            first}"
        when "Category"
          details << "#{Category.where(id: condition.contentable_id).pluck(:id).
            first}"
        when "User"
          details << "#{User.where(id: condition.contentable_id).pluck(:id).
            first}"
        when "Tag"
          details << "#{Tag.where(id: condition.contentable_id).pluck(:id).
            first}"
      end
    end
    details.join(",").to_s
  end

  # API Functions
  def v2_format current_user_id=""
    case keyword
      when "artwork_id"
        {
          type: "artwork",
          artwork_details: redirect_to_artwork.v2_format(current_user_id,"show"),
          details: {
            ids: redirect_to_ids,
            keyword: keyword,
            name: name,
            banner_image: banner_image_url
          }
        }
      when "category_ids"
        {
          type: "category",
          details: {
            ids: redirect_to_ids,
            keyword: keyword,
            name: name,
            banner_image: banner_image_url
          }
        }
      when "artist_ids"
        {
          type: "artist",
          details: {
            ids: redirect_to_ids,
            keyword: keyword,
            name: name,
            banner_image: banner_image_url
          }
        }
      when "tag_ids"
        {
          type: "tag",
          details: {
            ids: redirect_to_ids,
            keyword: keyword,
            name: name,
            banner_image: banner_image_url
          }
        }
    end
  end
end
