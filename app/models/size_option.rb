class SizeOption < ActiveRecord::Base
  has_many :artworks
  validates :name, presence: true
  validates :name, uniqueness: true

  def v1_format
  {
    id: id,
    name: name
  }
  end
end
