class Artwork < ActiveRecord::Base
  SIZE_TYPE = ['Fixed', 'Scalable', 'Presets']
  SIZE_FILTER = {"small"=> {min_h: 0, min_w: 0, max_h: 10, max_w: 10},
    "medium"=> {min_h: 10.01, min_w: 10.01, max_h:15, max_w:15},
    "large"=> {min_h: 15.01, min_w: 15.01, max_h: 20, max_w: 20},
    "xlarge"=> {min_h: 20.01, min_w: 20.01, max_h: 50, max_w: 50}}
  PRICE_FILTER = {"small"=> {min_p: 0, max_p: 30},
    "medium"=> {min_p: 30.01, max_p: 50},
    "large"=> {min_p: 50.01, max_p: 100},
    "xlarge"=> {min_p: 100.01, max_p: 9999}}

  attr_accessor :modified_admin_user_id, :modified_user_id

  scope :actives, -> {where(active: true)}
  scope :deleted, -> {where(active: false)}
  scope :public_artworks, -> {where(privacy: "public")}
  scope :private_artworks, -> {where(privacy: "private")}
  scope :by_name, -> {order(:name)}
  scope :for_marketplace, -> {actives.public_artworks}
  scope :top_picks, -> {where(top_pick: true).order(:sequence).for_marketplace}

  belongs_to :category
  belongs_to :artist
  belongs_to :user
  belongs_to :size_option
  # belongs_to :artwork_style
  # has_many :event_artworks
  # has_many :events, through: :event_artworks
  # has_many :artwork_sizes , dependent: :destroy
  has_many :artwork_images, dependent: :destroy
  has_many :artwork_prices, dependent: :destroy
  has_many :price_per_units, dependent: :destroy
  has_many :user_favorites, dependent: :destroy
  has_many :user_views, dependent: :destroy
  has_many :user_app_tracking_logs, dependent: :destroy
  has_many :cart_items, dependent: :destroy
  has_many :mark_up_prices, dependent: :destroy
  has_many :artwork_approval_hdrs, dependent: :destroy
  has_many :artwork_histories, dependent: :destroy
  has_many :taggables
  has_many :tags, through: :taggables

  has_many :promo_code_dtls, as: :promo_codable, dependent: :destroy
  has_many :promo_codes, through: :promo_code_dtls

  accepts_nested_attributes_for :artwork_images, allow_destroy: true
  accepts_nested_attributes_for :mark_up_prices, allow_destroy: true

  validates :name, :user_id, :privacy, presence: true
  validates :quantity, presence: true, if: "limited_edition == true"
  validates :name, uniqueness: {scope: [:artwork_style_id, :user_id]}
  # validates :artwork_images, presence: true
  validate :user_limit, if: "id.blank?"

  before_save :check_quantity_if_limited_edition
  after_create :update_user_upload_index
  after_create :check_if_user_index_not_unique
  after_create :create_artwork_approval, if: "privacy == 'public' and
    modified_admin_user_id.blank?"
  after_commit :send_submission_email, if: "privacy == 'public'"
  after_commit :send_submission_email_after_crop, if: "privacy == 'public'"
  before_update :create_history

  # Send Email Function
  def send_submission_email
    if right_aspect_ratio? == true
      ArtworkMailer.send_submission(self).deliver_now
    end
  end
  def send_submission_email_after_crop
    unless self.artwork_images.blank?
      if self.artwork_images.first.cropping?
        ArtworkMailer.send_submission(self).deliver_now
      end
    end
  end

  # Validation Functions
  def check_quantity_if_limited_edition
    unless self.limited_edition
      self.quantity = nil
    end
  end
  def check_if_user_index_not_unique
    @user = User.find_by id: user_id
    unless @user.blank?
      unless @user.artwork_sequence_number.blank?
        @same_index = User.where("id != ? and artwork_sequence_number = ?",
          @user.id, @user.artwork_sequence_number)
        unless @same_index.blank?
          @user_with_index = User.where("artwork_sequence_number IS NOT NULL")
          @user.artwork_sequence_number = @user_with_index.count + 1
        end
        @user.save(validate: false)
      end
    end
  end
  def validate_featured_size
    errors.add(:size_type, "unfeature the connected size before updating the Size Type") if artwork_sizes.where(featured: true).count > 0 and size_type != "Presets"
  end

  def validate_number_of_sizes
    if ["Fixed","Scalable"].include? size_type
      errors.add(:size_type, "There are #{artwork_sizes.size} connected sizes. You cannot update.") if
      artwork_sizes.size > 1
    end
  end

  # Callback Function
  def create_artwork_approval
    ArtworkApprovalHdr.create(artwork_id: self.id, expiration_date: Time.now + 48.hours)
  end
  def create_history
    fields = ["name", "category_id", "description", "privacy"]
    old_artwork = Artwork.find_by id: id
    fields.each do |att|
      if old_artwork.send(att) != self.send(att)
        case att
          when "category_id"
            old_value = Category.find_by(id: old_artwork.category_id).try(:name)
            new_value = Category.find_by(id: self.category_id).try(:name)
          else
            old_value  = old_artwork.send(att)
            new_value = self.send(att)
        end
        ArtworkHistory.create(
          artwork_id: self.id,
          field_name: att,
          admin_user_id: modified_admin_user_id,
          user_id: modified_user_id,
          old_value: old_value,
          new_value: new_value
        )
      end
    end
  end
  def update_user_upload_index
    @user = User.find_by id: user_id
    unless @user.blank?
      if @user.artwork_sequence_number.blank?
        @user.artwork_sequence_number = @user.index_in_first_artwork
        @user.save(validate: false)
      end
    end
  end


  # Attribute Functions
  def can_be_deleted?
    cart_items.count == 0
  end
  def right_aspect_ratio?
    unless featured_artwork_image.blank?
      case featured_artwork_image.orientation
        when "square"
          1 == featured_artwork_image.image.aspect_ratio.round(1)
        when "landscape"
          1.5 == featured_artwork_image.image.aspect_ratio.round(1)
        when "portrait"
          0.67 == featured_artwork_image.image.aspect_ratio.round(2)
        else
          false
      end
    end
  end
  def presets?
    size_type == "Presets"
  end
  def scalable?
    size_type == "Scalable"
  end


  # Naming Functions
  def user_limit
    unless user.blank?
      seq_no = user.artwork_sequence_number.to_i
      upload_limit = UploadArtworkLimit.where("from_counter <= ? and ? <= to_counter", seq_no, seq_no)
      unless upload_limit.blank?
        errors.add(:base, "Users form 1 - 100 can only upload #{upload_limit.first.limit} number of artworks.") unless user.artworks.actives.count < upload_limit.first.limit
      end
    end
  end
  def sales
    Order.all.includes(:cart_items).map{|o| o.cart_items.select{|ci| ci.artwork_id == self.id}.
      map{|ci| ci.total_product_amount('true')}.inject(0){|sum,x| sum + x }}.inject(0){|sum,x| sum + x }
  end
  def pieces_sold
    Order.all.includes(:cart_items).map{|o| o.cart_items.select{|ci| ci.artwork_id == self.id}.
      map{|ci| ci.quantity}.inject(0){|sum,x| sum + x}}.inject(0){|sum,x| sum + x}
  end
  def favorites
    UserFavorite.where(artwork_id: id, deleted: false).count
  end
  def status
    approval = ArtworkApprovalHdr.where(artwork_id: self.id).order(:id)
    unless approval.blank?
      unless approval.last.artwork_approval_dtls.blank?
        approval.last.status
      else
        "approve"
      end
    else
      "approve"
    end
  end
  def right_aspect_ratio
    unless featured_artwork_image.blank?
      featured_artwork_image.image.aspect_ratio.round(2)
    end
  end
  def featured_cart user_id
    user = User.where(id: user_id)
    unless user.blank?
      user.first.carts.where(status: nil).last.try(:id)
    end
  end


  # Favorite Functions
  def favorite user_id
    user_favorites.find_by(user_id: user_id)
  end
  def favorited? user_id
    fave = favorite(user_id)
    if fave.blank?
      false
    else
      !fave.deleted
    end
  end

  # Search Functions
  def self.filter_by_keyword keyword
    unless keyword.blank?
      includes([:user, :tags]).where("artworks.name ILIKE '%#{keyword}%'
        or users.name ILIKE '%#{keyword}%' or tags.name ILIKE '%#{keyword}%'").
        references([:user, :tags])
    else
      where("artworks.name IS NOT NULL")
    end
  end
  def self.filter_by filter
    case filter
      when "az"
        order("name asc")
      when "za"
        order("name desc")
      when "latest"
        order("created_at desc")
      else
        where("artworks.id IS NOT NULL")
    end
  end
  def self.filter_by_categories filter
    if filter.blank?
      where("category_id IS NOT NULL")
    else
      where("category_id IN (#{filter})")
    end
  end
  def self.filter_by_artists filter
    if filter.blank?
      where("user_id IS NOT NULL")
    else
      where("user_id IN (#{filter})")
    end
  end
  def self.filter_by_ids filter
    if filter.blank?
      where("artworks.id IS NOT NULL")
    else
      where("artworks.id IN (#{filter})")
    end
  end
  def self.filter_by_privacy filter
    if filter.blank?
      where("privacy = 'public'").select{|a| a.status == "approve"}
    else
      where("privacy = ?", filter)
    end
  end
  def self.filter_by_privacy_v2 filter
    if filter.blank?
      where("privacy = 'public'")
    else
      where("privacy = ?", filter)
    end
  end
  def self.filter_by_artist filter
    if filter.blank?
      where("user_id IS NOT NULL")
    else
      where("user_id = ?", filter)
    end
  end
  def self.filter_by_category filter
    if filter.blank?
      where("category_id IS NOT NULL")
    else
      where("category_id = ?", filter)
    end
  end
  def self.filter_by_artwork_style filter
    if filter.blank?
      where("artwork_style_id IS NOT NULL")
    else
      where("artwork_style_id = ?", filter)
    end
  end
  def self.filter_by_size filter
    if filter.blank?
      joins(:artwork_sizes)
    else
      query_string = ""
      filter.split(",").each do |x|
        min_h = SIZE_FILTER[x][:min_h]
        min_w = SIZE_FILTER[x][:min_w]
        max_h = SIZE_FILTER[x][:max_h]
        max_w = SIZE_FILTER[x][:max_w]
        query_string += " or " unless query_string.blank?
        query_string += ("((artwork_sizes.height <= #{max_h} and artwork_sizes.
        width <= #{max_w} and artwork_sizes.height >= #{min_h} and artwork_sizes.
        width >= #{min_w} and (artworks.size_type = 'Fixed' or artworks.size_type
        = 'Presets')) or artworks.size_type = 'Scalable')")
      end
      joins(:artwork_sizes).where(query_string)
    end
  end
  def self.filter_by_price filter
    if filter.blank?
      joins(:artwork_prices)
    else
      query_string = ""
      filter.split(",").each do |x|
        min_p = PRICE_FILTER[x][:min_p]
        max_p = PRICE_FILTER[x][:max_p]
        query_string += " or " unless query_string.blank?
        query_string += "(#{min_p} <= artwork_prices.price and artwork_prices.price <= #{max_p})"
      end
      joins(:artwork_prices).where(query_string)
    end
  end

  # Count Functions
  def favorites_count
    user_favorites.where(deleted: false).count
  end
  def views_count
    user_views.map{|u| u.counter}.inject(0){|sum,x| sum + x }
  end
  def user_views_duration_count
    total_duration = user_app_tracking_logs.map{|u| u.duration}.inject(0){|sum,x| sum + x}
    Time.at(total_duration).utc.strftime("%H:%M")
  end
  def popularity
    pieces_sold.to_s.to_i + favorites_count.to_s.to_i
  end


  def v1_format(mode = "", current_user_id = "")
    case mode
      when "show"
        {
          id: id,
          image: featured_image,
          featured_price: featured_price.to_s.to_d.to_f,
          name: name,
          artist_name: user.try(:full_name),
          # orientation: featured_artwork_image.try(:orientation),
          description: description,
          available_sizes: fixed_artwork_sizes.order(:width).map{|as| as.v1_format(self)},
          favorite_id: favorite(current_user_id).try(:id),
          favorited: favorited?(current_user_id),
          cart_id: featured_cart(current_user_id),
          privacy: privacy,
          limited_edition: (limited_edition.blank? ? false : limited_edition),
          quantity: current_quantity,
          original_quantity: quantity,
          artist_id: user.id,
          mark_up_price_id: current_markup.try(:id),
          mark_up_price_percentage: current_markup.try(:percentage),
          artwork_style_id: artwork_style_id,
          status: status
        }
      else
        {
          id: id,
          name: name,
          artist_name: user.try(:full_name),
          image: featured_image,
          # orientation: featured_artwork_image.try(:orientation),
          category_id: category_id,
          artwork_style_id: artwork_style_id,
          favorite_id: favorite(current_user_id).try(:id),
          favorited: favorited?(current_user_id),
          status: status,
          user_id: user_id,
          privacy: privacy,
          limited_edition: (limited_edition.blank? ? false : limited_edition),
          quantity: current_quantity,
          artist_id: user_id,
          mark_up_price_id: current_markup.try(:id),
          mark_up_price_percentage: current_markup.try(:percentage)
        }
    end
  end
  def v2_format current_user_id="", action_name="", cart_item_id=""
    case action_name
      when "show"
        unless cart_item_id.blank?
          cart_item = CartItem.find_by id: cart_item_id
        end
        markup = current_markup_v2
        {
          id: id,
          name: name,
          artist_name: user.try(:full_name),
          artist_id: user_id,
          image: featured_image,
          original_image: featured_original_image,
          orientation: orientation,
          description: description,
          privacy: privacy,
          created_at: created_at.strftime("%b %d, %Y %H:%M:%S"),
          favorited: favorited?(current_user_id),
          favorites_count: favorites_count,
          quantity: (cart_item.blank? ? 1 : cart_item.try(:quantity)),
          cart_item_id: cart_item_id.to_i,
          mark_up_price_id: markup[:id],
          mark_up_type: markup[:type],
          mark_up_price: markup[:value],
          installation: (cart_item.blank? ? false : cart_item.try(:installation).to_s.to_bool),
          installation_amounts: InstallationRange.by_min.map{|i| i.v2_format},
          print_sizes: fixed_artwork_sizes.map{|fa| fa.v2_format(nil, nil,
            cart_item.try(:fixed_size_id))},
          materials: Material.by_name.map{|fa| fa.v2_format(orientation,
            cart_item.try(:material_id), cart_item.try(:fixed_size_id),
            cart_item.try(:fixed_size_price_id))},
          frame_types: (FrameType.blank_frame_type(cart_item.try(:frame_type_id)) + FrameType.by_name.
            map{|fa| fa.v2_format(orientation, cart_item.try(:frame_type_id),
            cart_item.try(:fixed_size_id), cart_item.try(:fixed_size_frame_price_id))})
        }
      when "edit"
        markup = current_markup_v2
        {
          id: id,
          image: featured_original_image,
          name: user.try(:full_name),
          description: description,
          tags: tag_ids.join(", "),
          category_id: category_id,
          privacy: privacy,
          mark_up_price_id: markup[:id],
          mark_up_type: markup[:type],
          mark_up_price: markup[:value]
        }
      else
        image = featured_artwork_image.try(:image)
        original_image = "https:" + image.url(:original).to_s unless image.blank?
        medium_image = "https:" + image.url(:medium).to_s unless image.blank?
        markup = current_markup_v2
        {
          id: id,
          status: status,
          name: name,
          artist_name: user.try(:full_name),
          artist_id: user_id,
          image: medium_image,
          original_image: original_image,
          orientation: orientation,
          description: description,
          privacy: privacy,
          created_at: created_at.strftime("%b %d, %Y %H:%M:%S"),
          favorited: favorited?(current_user_id),
          favorites_count: favorites_count,
          mark_up_price_id: markup[:id],
          mark_up_type: markup[:type],
          mark_up_price: markup[:value]
        }
    end
  end

  # Image Functions
  def featured_image
    image = featured_artwork_image.try(:image)
    unless image.blank?
      "https:" + image.url(:medium).to_s unless image.blank?
    end
  end
  def featured_original_image
    image = featured_artwork_image.try(:image)
    unless image.blank?
      "https:" + image.url(:original).to_s unless image.blank?
    end
  end

  # Naming Functions
  def current_quantity
    if limited_edition
      (quantity - pieces_sold)
    else
      nil
    end
  end
  def artwork_fixed_sizes
    unless featured_artwork_image.blank?
      FixedSize.where(size_type: featured_artwork_image.orientation).by_name
    else
      FixedSize.where(size_type: "square").by_name
    end
  end
  def featured_artwork_image
    artwork_images.first
  end
  def orientation
    featured_artwork_image.try(:orientation)
  end
  def fixed_artwork_sizes
    FixedSize.where(size_type: featured_artwork_image.try(:orientation)).by_name
  end
  def featured_final_price
    featured_price.to_s.to_d.to_f + markup_price(featured_price).to_s.to_d.round(2)
  end
  def featured_price
    fixed_artwork_sizes.order(:width).first.try(:current_price).to_s.to_d.round(2)
  end
  def current_markup_v2
    value = Hash.new
    unless current_markup.blank?
      value[:id] = current_markup.try(:id)
      value[:type] = current_markup.try(:type_name)
      value[:value] = current_markup.try(:value_amount)
    else
      value[:id] = nil
      value[:type] = "price"
      value[:value] = 0
    end
    value
  end
  def current_markup
    mark_up_prices.where("effectivity_date <= ?", Time.now).
      order(:effectivity_date).last
  end
  def last_markup
    MarkUpPrice.where(artwork_id: id).order("id desc").first
  end
  def markup_price base_price = 0, saved=""
    unless current_markup.blank?
      unless current_markup.percentage.blank?
        mark_up_percentage = (current_markup.try(:percentage).to_s.to_d.to_f / 100).to_d.to_f
        base_price.to_s.to_d.to_f * mark_up_percentage
      else
        current_markup.try(:price).to_s.to_d.round(2)
      end
    else
      0
    end
  end
  def mark_up_prices_by_date
    MarkUpPrice.where(artwork_id: id).order(effectivity_date: :desc)
  end
  def tag_ids
    tags.order(:name).map{|x| x.name}
  end

  # For Content Repository
  def content_name
    "Artwork-#{id}"
  end
end