class UserShippingDetail < ActiveRecord::Base
  belongs_to :user
  has_many :orders
  has_many :carts

  scope :by_name, -> {order("is_default desc, first_name,
    detailed_addr")}
  scope "actives", -> {where("active = ?", true)}
  scope "deleted", -> {where("active = ?", false)}

  validates :user_id, :first_name, :last_name, :phone_number,
    :detailed_addr, :country, presence: true
  validates :detailed_addr, uniqueness: {scope: [:first_name, :last_name, :phone_number,
    :country, :user_id]}
  after_save :remove_all_default
  after_destroy :add_new_default

  attr_accessor :is_selected

  def add_new_default
    shippings = UserShippingDetail.where("user_id = ? and is_default = ? and
      active = ? and id != ?", user_id, true, true, id)
    if shippings.blank?
      last_shipping = user.user_shipping_details.actives.last
      last_shipping.is_default = true
      last_shipping.save(validate: false)
    end
  end
  def can_be_deleted?
    orders.count == 0
  end
  def remove_all_default
    if is_default == true
      users = UserShippingDetail.where("id != #{id} and user_id = #{user_id}
        and is_default is #{true}")
      users.update_all(is_default: false)
    else
     shippings = UserShippingDetail.where("user_id = ? and is_default is #{true}
        and id != ? and active is #{true}", user_id, id)
      if shippings.blank?
        last_shipping = user.user_shipping_details.actives.last
        user_last_shipping = UserShippingDetail.where(id: last_shipping.try(:id))
        user_last_shipping.update_all(is_default: true)
      end
    end
  end
  def name
    "#{first_name} #{last_name}"
  end
  def name_w_addr
    "#{name} - #{detailed_addr}, #{country}"
  end
  def v1_format
    {
      id: id,
      email: email,
      name: name,
      first_name: first_name,
      last_name: last_name,
      phone_number: phone_number,
      address: detailed_addr,
      country: country,
      city: city,
      is_default: is_default
    }
  end
  def v2_format
    {
      id: id,
      first_name: first_name,
      last_name: last_name,
      phone_number: phone_number,
      address: detailed_addr,
      postal_code: postal_code,
      country: country,
      is_default: is_default
    }
  end

  # Naming Functions
  def name_w_addr
    "#{name} - #{phone_number}, #{detailed_addr}, #{country}"
  end
  def country_details
    "#{postal_code.blank? ? "" : postal_code + " "}#{country}"
  end
end