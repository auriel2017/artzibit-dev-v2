class InstallationRange < ActiveRecord::Base
  has_many :installation_prices
  validates :min_size, :max_size, presence: true
  accepts_nested_attributes_for :installation_prices, allow_destroy: true
  scope :by_min, -> {order(:min_size)}
  def name
    "#{min_size} cm - #{max_size} cm"
  end
  def current_range_price
    installation_prices.where("effectivity_date <= ?", Time.now).order("effectivity_date desc").first
  end
  def current_price
    current_range_price.try(:price)
  end
  def v2_format
    {
      min: min_size,
      max: max_size,
      price: current_price,
      installation_price_id: id
    }
  end
end
