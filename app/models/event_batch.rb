class EventBatch < ActiveRecord::Base
  belongs_to :event
  has_many :user_checked_in
  validates :start_date, :end_date, presence: true
  validates :event_id, uniqueness: { scope: [:start_date, :end_date] }
  def format_date
    if start_date == end_date
      "#{start_date.strftime('%b %d, %Y')}"
    else
      "#{start_date.strftime('%b %d, %Y')} - #{end_date.strftime('%b %d, %Y')}"
    end
  end
  def format_date_wo_year
    if start_date == end_date
      "#{start_date.strftime('%b %d')}"
    else
      "#{start_date.strftime('%b %d')} - #{end_date.strftime('%b %d')}"
    end
  end
  def format_date_w_day
    if start_date == end_date
      "#{start_date.strftime('%A, %b %d, %Y')}"
    else
      "#{start_date.strftime('%A, %b %d, %Y')} - #{end_date.strftime('%A, %b %d, %Y')}"
    end
  end
  def v1_format mode=""
    unless mode == "show"
      {
        id: id,
        event_name: event.title,
        date: format_date_wo_year,
        image: event.featured_image,
        location: event.location,
        long: event.long,
        lat: event.lat
      }
    else
      {
        id: id,
        event_name: event.title,
        date: format_date_w_day,
        location: event.location,
        description: event.description,
        checked_in: !user_checked_in.blank?,
        artworks: event.artworks.actives.map{|a| a.v1_format}
      }
    end
  end
end
