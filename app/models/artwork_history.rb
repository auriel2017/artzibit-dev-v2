class ArtworkHistory < ActiveRecord::Base
  belongs_to :artwork
  belongs_to :admin_user
  belongs_to :user

  after_create :create_artwork_approval, if: "field_name == 'privacy' and
    new_value == 'public'"

  def create_artwork_approval
    if artwork.right_aspect_ratio? == true
      ArtworkApprovalHdr.create(artwork_id: artwork.id, expiration_date: Time.now + 48.hours)
    end
  end
  def modified_by
    if admin_user.present?
      "Admin:#{admin_user.name}"
    elsif user.present?
      "User: <#{user.name}>"
    else
      "No Record"
    end
  end
end
