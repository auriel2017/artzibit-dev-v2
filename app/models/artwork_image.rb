class ArtworkImage < ActiveRecord::Base
  has_attached_file :image,
    :styles => {:original => {}, :medium => {}},
    :convert_options => {:medium =>  lambda { |instance| instance.resize}},
    :processors => [:cropper]
  # attr_accessor :crop_x1, :crop_y2, :crop_w3, :crop_h4

  belongs_to :artwork

  # validate :minimum_dimension
  validates :image, presence: true
  validates_attachment_content_type :image,
    :content_type => /^image\/(jpg|jpeg|png)$/,
    :size => {in: 0..75.kilobytes}
  validate :maximum_size

  # process_in_background :image, url_with_processing: false

  after_save :set_orientation
  # after_commit :send_submission_email

  # def send_submission_email
  #   if artwork.right_aspect_ratio? == true and artwork.privacy == "public"
  #     ArtworkMailer.send_submission(artwork).deliver_now
  #   end
  # end

  def resize
    if self.image.height.to_f == self.image.width.to_f
      "-resize 500x500! -density 70"
    else
      # landscape
      if self.image.height.to_f < self.image.width.to_f
        unless self.cropping?
          "-resize #{333.34 * self.image.aspect_ratio.round(1)}x333.34! -density 70"
        else
          "-resize 500x333.34! -density 70"
        end
      # portrait
      else
        unless self.cropping?
          "-resize #{self.image.aspect_ratio.round(1) * 500}x500! -density 70"
        else
          "-resize 333.34x500! -density 70"
        end
      end
    end
  end

  def maximum_size
    errors.add(:image, "The max size is 75MB") if self.image.try(:size).to_i > 75000000
  end

  def minimum_dimension
    errors.add(:image, "The min size is 2500px") if self.image.height.to_i < 2500 or self.image.width.to_i < 2500
  end

  def v1_format
    {
      image: image.url(:medium)
    }
  end

  ###################################
  def cropping?
    unless artwork.try(:right_aspect_ratio) == true
      !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
    end
  end

  def avatar_geometry(style = :original)
    @geometry ||= {}
    @geometry ||= Paperclip::Geometry.from_file(image.path)
  end

  def crop_string
    "#{crop_w}x#{crop_h}+#{crop_x}+#{crop_y}"
  end
  def set_orientation
    if self.image.height.to_f == self.image.width.to_f
      orientation = "square"
    else
      if self.image.height.to_f < self.image.width.to_f
        orientation = "landscape"
      else
        orientation = "portrait"
      end
    end
    img = ArtworkImage.where(id: id)
    img.update_all(orientation: orientation)
  end
  ###################################
  private
  def remove_photo
    self.image.clear
    true
  end
end
