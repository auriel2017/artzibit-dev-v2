class UserAuthentication < ActiveRecord::Base
  belongs_to :user
  # attr_accessible :provider, :uid, :user_id
  validates :uid, uniqueness: true
end
