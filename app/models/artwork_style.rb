class ArtworkStyle < ActiveRecord::Base

  has_many :artworks
  has_many :promo_code_dtls, as: :promo_codable
  has_many :promo_codes, through: :promo_code_dtls

  scope :by_name, -> {order(:name)}

  # has_attached_file :banner_image
  attr_accessor :remove_image

  validates :name, presence: true, uniqueness: true
  # validates_attachment_content_type :banner_image,
  #   content_type: /^image\/(jpg|jpeg|pjpeg|png|x-png|gif)$/, unless: "banner_image.blank?"

  def v1_format
    {
      id: id,
      name: name
    }
  end
end
