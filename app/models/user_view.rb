class UserView < ActiveRecord::Base
  belongs_to :user
  belongs_to :artwork
  validates :artwork_id, uniqueness: { scope: :user_id,
    message: "should favorite once" }
end
