class AdminUser < ActiveRecord::Base
  devise :database_authenticatable, :recoverable, :rememberable, :trackable,
    :validatable

  has_many :artwork_histories

  def can_print?
    admin? or printer?
  end

  def can_install?
    admin? or installer?
  end

  def self.default_printer_email
    AdminUser.where("admin is false and installer is true").first
  end

  def self.printer_name
    "Brilliant Prints SG"
  end

  def self.printer_addr
    "50 Genting Lane #06-05, Cideco Industrial Complex, Singapore 349558"
  end

end
