class UserCheckedIn < ActiveRecord::Base
  belongs_to :event_batch
  belongs_to :user
  validates :event_batch_id, :user_id, :checked_in_at, presence: true
  validates :event_batch_id, uniqueness: { scope: :user_id }
end
