class Material < ActiveRecord::Base
  has_many :fixed_size_prices
  has_many :cart_items
  scope :by_name, -> {order(:name)}
  validates :name, presence: true
  accepts_nested_attributes_for :fixed_size_prices, allow_destroy: true
  before_destroy :delete_prices

  def delete_prices
    FixedSizePrice.where(material_id: id).delete_all
  end
  def fixed_size_prices_by_date
    FixedSizePrice.where(material_id: material_id, fixed_size_id: fixed_size_id).by_date
  end
  def name_label
    fixed_size = FixedSize.find_by id: fixed_size_id
    material = Material.find_by id: material_id
    "#{material.try(:name)}|#{fixed_size.try(:name)}"
  end
  def v2_format size_type, selected_id="", fixed_size_id="", price_id=""
    {
      id: id,
      name: name,
      amounts: FixedSize.where(size_type: size_type).by_name.map{|fa| fa.v2_format("material",
        id, fixed_size_id, price_id)},
      selected: selected_id == id
    }
  end
end
