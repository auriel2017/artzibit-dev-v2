class UserFavorite < ActiveRecord::Base
  scope :active, -> {where(deleted: false).order(updated_at: :desc)}
  belongs_to :user
  belongs_to :artwork
  validates :artwork_id, uniqueness: { scope: :user_id,
    message: "should favorite once" }
  validate :check_delete_value
  after_save :update_artwork_fields
  def update_artwork_fields
    art = Artwork.find_by id: self.artwork_id
    art.total_favorites = art.favorites
    art.save(validate: false)
  end
  def check_delete_value
    unless user.user_favorites.where(artwork_id: artwork_id).blank?
      if user.user_favorites.where(artwork_id: artwork_id).first.deleted
        errors.add(:base, "You already favorited the Artwork") if deleted == true
      else
        errors.add(:base, "You already unfavorited the Artwork") if deleted == false
      end
    end
  end
  def v1_format
    {
      id: id,
      artwork_id: artwork.id,
      image: artwork.featured_image
    }
  end
end