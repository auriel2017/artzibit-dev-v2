class FixedSizeFramePrice < ActiveRecord::Base
  scope :by_date, -> {order(effectivity_date: :desc)}
  belongs_to :fixed_size
  belongs_to :frame_type
  validates :price, numericality: true, presence: true
end
