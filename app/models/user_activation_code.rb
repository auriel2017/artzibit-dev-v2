class UserActivationCode < ActiveRecord::Base

  validates :user_id, uniqueness: :true, unless: "user_id.blank?"
  validate :limit_users

  belongs_to :user
  belongs_to :activation_code

  before_create :generate_token
  before_create :generate_expiration

  def limit_users
    unless activation_code.reusable == true
      errors.add(:base, "The activaiton code reaches the limit") unless activation_code.limit.to_i > activation_code.reserved_and_used_count
    end
  end

  protected
  def generate_token
    self.reserved_token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless UserActivationCode.exists?(reserved_token: random_token)
    end
  end
  def generate_expiration
    self.expiration = Time.now + 5.minutes
  end
end
