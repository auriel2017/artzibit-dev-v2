class FrameType < ActiveRecord::Base
  has_many :fixed_size_frame_prices
  has_many :cart_items
  scope :by_name, -> {order(:name)}
  validates :name, presence: true
  accepts_nested_attributes_for :fixed_size_frame_prices, allow_destroy: true
  def fixed_size_frame_prices_by_date
    FixedSizeFramePrice.where(frame_type_id: frame_type_id, fixed_size_id: fixed_size_id).by_date
  end
  def name_label
    fixed_size = FixedSize.find_by id: fixed_size_id
    frame_type = FrameType.find_by id: frame_type_id
    "#{frame_type.try(:name)}|#{fixed_size.try(:name)}"
  end
  def v2_format size_type, selected_id="", fixed_size_id="", price_id=""
    {
      id: id,
      name: name,
      amounts: FixedSize.where(size_type: size_type).by_name.map{|fa| fa.v2_format("frame_type",
        id, fixed_size_id, price_id)},
      selected: selected_id == id
    }
  end
  def self.blank_frame_type selected_id=""
    [{
      id: nil,
      name: "No Frame",
      amounts: [],
      selected: selected_id.blank?
    }]
  end
end
