class Artist < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
  has_many :artists

  def v1_format
    {
      id: id,
      name: name
    }
  end
end
