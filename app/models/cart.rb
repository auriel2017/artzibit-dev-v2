class Cart < ActiveRecord::Base

  scope :available, -> {where("status IS NULL or status = ''")}

  belongs_to :user
  belongs_to :delivery_price
  belongs_to :user_shipping_detail
  belongs_to :promo_code
  belongs_to :user_shipping_detail
  belongs_to :user_bank_detail
  belongs_to :promo_code
  has_many :cart_items, dependent: :destroy
  has_one :order, dependent: :destroy

  accepts_nested_attributes_for :cart_items, allow_destroy: true
  attr_accessor :stripe_token, :card_stripe_id, :fixed_size_prices,
    :created_by_admin, :fixed_size_frame_prices, :actual_promo_code,
    :shippings, :fixed_sizes, :materials, :frame_types, :mark_up_prices

  validates :user_id, presence: true
  validates :user_shipping_detail_id, :user_bank_detail_id, :card_id,
    presence: :true, unless: "status.blank?"
  validates :user_id, uniqueness: {scope: :status}, if: "status.blank?"
  # validate :no_update_when_checkout
  # validate :check_quantity_of_cart_items
  validate :validate_promo_code, unless: "actual_promo_code.blank?"

  # before_save :installation_should_false_if_overseas
  # before_save :frame_for_cart_items
  before_save :installation_and_frame_for_cart_items
  before_save :compute_delivery_charge
  before_save :compute_total_amount
  before_save :add_promo_code_id
  # before_save :check_if_promo_code_saved_promo_code

  after_save :create_order, if: "created_by_admin == 'true'"

  # def check_quantity_of_cart_items
  #   cart_items.each do |c|
  #     if c.artwork.try(:limited_edition) == true
  #       unless c.artwork.current_quantity >= c.qua ntity
  #         errors.add(:base, "Some of the artworks has already been sold out")
  #       end
  #     end
  #   end
  # end

  def add_promo_code_id
    promo_code_hdr = PromoCode.find_by code: actual_promo_code
    unless promo_code_hdr.blank?
      self.promo_code_id = promo_code_hdr.id
      self.discount_amount = total_discount_amount(promo_code_hdr.id).round(2)
      self.total_price = self.total_price - self.discount_amount
    end
    self.check_if_promo_code_saved_promo_code
  end

  def total_discount_amount promo_code_id
    discount = 0
    promo_code_hdr = PromoCode.find_by id: promo_code_id
    unless promo_code_hdr.blank?
      total_amount = (sub_total_amount.round(2) + delivery_charge_amount.round(2))
      # raise sub_total_amount.round(2).inspect
      if promo_code_hdr.price_off == 0 or promo_code_hdr.price_off.blank?
        discount = total_amount * (promo_code_hdr.percentage_off.to_d.to_f / 100).to_d.to_f
      else
        discount = promo_code_hdr.price_off
      end
    end
    discount.round(2)
  end

  def validate_promo_code
    valid, error_message = self.valid_promo_code? actual_promo_code
    errors.add(:base, error_message) unless valid
  end

  def check_if_promo_code_saved_promo_code
    valid, error_message = self.valid_promo_code?
    unless valid
      self.promo_code_id = nil
      self.discount_amount = 0
      self.total_price = (sub_total_amount.round(2) + delivery_charge_amount.round(2))
    else
      self.discount_amount = total_discount_amount(self.promo_code_id).round(2)
      self.total_price = (sub_total_amount.round(2) + delivery_charge_amount.round(2)) - self.discount_amount unless self.total_price <= 0
    end
  end

  def valid_promo_code? param_code = ""
    @valid = true
    unless param_code.blank?
      promo_code_hdr = PromoCode.find_by code: param_code
    else
      promo_code_hdr = PromoCode.find_by id: promo_code_id
    end

    unless promo_code_hdr.blank?
      # limit
      if !(promo_code_hdr.limit == 0 or promo_code_hdr.limit.blank?)
        if promo_code_hdr.limit <= promo_code_hdr.carts.where(status: "checkout").count
          @valid, @error_message = false, "Promo code reaches its limit"
        end
      end
      # start and end date
      if promo_code_hdr.end_date.blank?
        unless promo_code_hdr.start_date <= Time.now
          @valid, @error_message = false, "Promo code is invalid by date"
        end
      end
      if !promo_code_hdr.end_date.blank?
        unless (promo_code_hdr.start_date <= Time.now and Time.now <= promo_code_hdr.end_date)
          @valid, @error_message = false, "Promo code is invalid by date"
        end
      end
      # conditions
      unless promo_code_hdr.promo_code_dtls.blank?
        exist_cart_items = cart_items.reject{|a| a._destroy == true}
        promo_code_hdr.promo_code_dtls.each do |p|
          case p.promo_codable_type
            when "Artwork"
              exist_cart_items = exist_cart_items.reject{|a| a.artwork_id == p.promo_codable_id}
            when "ArtworkStyle"
              exist_cart_items = exist_cart_items.reject{|a| a.artwork.artwork_style_id == p.promo_codable_id}
            when "User"
              exist_cart_items = exist_cart_items.reject{|a| a.artwork.user_id == p.promo_codable_id}
          end
        end
        @valid, @error_message = false, "Cannot apply this promo code" if exist_cart_items.count == cart_items.count
      end
    else
      @valid, @error_message = false, "Promo Code is invalid"
    end

    # cart_items should not be blank
    if @valid
      existing_cart_items = cart_items.reject{|a| a._destroy == true}
      @valid, @error_message = false, "Cannot apply this promo code" if existing_cart_items.blank?
    end

    [@valid, @error_message]
  end

  def compute_delivery_charge
    # if delivery_price.blank?
      self.delivery_price_id = total_delivery_amount.try(:id)
    # end
  end

  def total_delivery_amount saved=""
    if saved.blank?
      saved_cart_items = cart_items.reject{|a| a._destroy == true}
      unless saved_cart_items.blank?
        height = saved_cart_items.sort_by{|a| a.size_height * -1}.first.try(:size_height)
        width = saved_cart_items.sort_by{|a| a.size_width * -1}.first.try(:size_width)
        if height < width
          @big_side = width
        else
          @big_side = height
        end
        @with_installation = saved_cart_items.select{|a| a.installation?}.count > 0
        del_pri = DeliveryPrice.joins(:delivery_range).where("delivery_ranges.with_installation = ? and (delivery_ranges.min_size <= ? and ? <= delivery_ranges.max_size) and delivery_prices.effectivity_date <= ?", @with_installation, @big_side, @big_side, Time.now).order("delivery_prices.effectivity_date asc").last
      else
        del_pri = nil
      end
    else
      del_pri = DeliveryPrice.find_by id: delivery_price_id
    end
    del_pri
  end

  def installation_and_frame_for_cart_items
    ship_dtl = UserShippingDetail.find_by id: user_shipping_detail_id
    cart_items.each do |ci|

      # Installation
      if ci.installation == false or ship_dtl.try(:country) == "Singapore"
        ci.installation_price_id = nil
      else
        if ci.installation_price_id.blank?
          if ci.fixed_size_id.blank?
            height = ci.height
            width = ci.width
          else
            fix_size = FixedSize.where(id: ci.fixed_size_id).first
            height = fix_size.try(:height)
            width = fix_size.try(:width)
          end
          if height < width
            big_side = width
          else
            big_side = height
          end
          ins_pri = InstallationPrice.joins(:installation_range).where("(installation_ranges.min_size <= ? and ? <= installation_ranges.max_size) and installation_prices.effectivity_date <= ?", big_side, big_side, Time.now).last
          ci.installation_price_id = ins_pri.try(:id)
        end
      end

      # Frames
      if ci.frame == false
        ci.frame_type_id = nil
        ci.fixed_size_frame_price_id = nil
      end
    end
  end
  def installation?
    cart_items.select{|a| a.installation == true and (a._destroy == false)}.count > 0
  end
  def capture_charge
    if status == "checkout" and user.stripe_id.present? and card_stripe_id.present?
      charge = Stripe::Charge.create(
        :customer    => user.stripe_id,
        :amount      => (5 * 100).to_i,
        :description => charge_description,
        :currency    => 'usd'
      )
    end
  end
  def charge_description
    "Cart ##{id}
     Items: #{cart_items_format}
    "
  end
  def no_update_when_checkout
    cart = Cart.find_by id: self.id
    if cart.status == "checkout"
      errors.add(:base, "Update is prohibit once checkout")
    end
  end

  def cart_items_format
    items = []
    cart_items.each do |ci|
      items << "#{ci.quantity} #{ci.size} #{ci.artwork.name}"
    end
    items.join(" ")
  end

  def self.update_cart_amount id
    cart = Cart.find id
    cart.delivery_price_id = cart.total_delivery_amount.try(:id)
    tobe_total_price = ((cart.sub_total_amount.round(2) + cart.total_delivery_amount.try(:price).to_s.to_d.to_f.round(2)) - cart.total_discount_amount(cart.promo_code_id)).round(2)
    if tobe_total_price < 0
      cart.total_price = 0
    else
      cart.total_price = tobe_total_price
    end
    cart.check_if_promo_code_saved_promo_code
    cart
  end

  def compute_total_amount
    # tobe_total_price = (sub_total_amount.round(2) + delivery_charge_amount.round(2)).round(2)
    exist_cart_items = cart_items.reject{|a| a._destroy == true}
    sub_amount = (exist_cart_items.map{|c| c.total_amount}.inject(0){|sum,x| sum + x }).to_s.to_d.to_f
    if exist_cart_items.blank?
      del_amount = 0
    else
      del_amount = total_delivery_amount.try(:price).to_s.to_d.to_f
    end
    tobe_total_price = (sub_amount.round(2) + del_amount.round(2)).round(2)
    if tobe_total_price < 0
      self.total_price = 0
    else
      self.total_price = tobe_total_price
    end
  end

  def sub_total_amount saved=""
     (cart_items.reject{|a| a._destroy == true}.map{|c| c.total_amount(saved)}.inject(0){|sum,x| sum + x }).to_s.to_d.to_f
  end
  def installation_fees
    (cart_items.reject{|a| a._destroy == true}.map{|c| c.total_installation_amount}.inject(0){|sum,x| sum + x }).to_s.to_d.to_f
  end
  def framing_fees
    (cart_items.reject{|a| a._destroy == true}.map{|c| c.total_frame_amount}.inject(0){|sum,x| sum + x }).to_s.to_d.to_f
  end
  def delivery_charge_amount saved=""
    unless cart_items.reject{|a| a._destroy == true}.blank?
      total_delivery_amount(saved).try(:price).to_s.to_d.to_f
    else
      0
    end
  end

  def items_count
    # cart_items.count
    CartItem.where(cart_id: id).count
  end
  def v1_format
    {
      id: id,
      user_id: user_id,
      status: status,
      shipping: delivery_charge_amount,
      installation_fees: (installation_fees).round(2),
      framing_fees: (framing_fees).round(2),
      promo_code: promo_code.try(:code),
      discount_amount: (discount_amount.to_s.to_d.to_f).round(2),
      subtotal_amount: (total_price.to_s.to_d.to_f).round(2),
      user_shipping_detail_id: user_shipping_detail_id,
      items: cart_items.order("id desc").map{|c| c.v1_format}
    }
  end
  def v2_format controller=""
    if controller == "update"
      {
        id: id,
        user_id: user_id,
        status: status_name,
        promo_code: promo_code.try(:code),
        subtotal_amount: (sub_total_amount.to_s.to_d.to_f).round(2),
        discount_amount: (discount_amount.to_s.to_d.to_f).round(2),
        shipping_fee: delivery_charge_amount.to_d.round(2),
        total:  total_price.to_d.round(2)
      }
    else
      {
        id: id,
        user_id: user_id,
        status: status_name,
        promo_code: promo_code.try(:code),
        subtotal_amount: (sub_total_amount.to_s.to_d.to_f).round(2),
        discount_amount: (discount_amount.to_s.to_d.to_f).round(2),
        shipping_fee: delivery_charge_amount.to_d.round(2),
        total:  total_price.to_d.round(2),
        items: cart_items.order("id desc").map{|c| c.v2_format}
      }
    end
  end

  # Naming Function
  def status_name
    if status.blank?
      "Available"
    else
      status
    end
  end

  def create_order
    orders = Order.where(cart_id: id)
    if status == "checkout"
      if orders.blank?
        order = Order.new
        order.user_id = user_id
        order.cart_id = id
        order.user_shipping_detail_id = user_shipping_detail_id
        order.card_stripe_id = card_stripe_id
        order.current_status = "order"
        order.save

        order_hist = OrderHistory.new
        order_hist.order_id = order.id
        order_hist.status = "order"
        order_hist.processed_at = updated_at
        order_hist.save
      end
    else
      orders.each do |x|
        x.order_histories.delete_all
      end
      orders.delete_all
    end
  end
end
