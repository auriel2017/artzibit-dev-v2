class ArtworkApproval < ActiveRecord::Base
  belongs_to :admin_user
  belongs_to :artwork

  validate :status_fields
  validate :cannot_be_update

  before_save :add_token
  after_save :send_approval, if: "status.blank?"

  def status_fields
    errors.add(:base, "Invalid status name") unless ((status == "approve" or status == "reject") and !id.blank?)
  end

  def cannot_be_update
    errors.add(:base, "We already receive your Approve or Reject") if (!status.blank? and !id.blank?)
  end

  def add_token
    self.approve_token = Digest::SHA1.hexdigest "#{admin_user.email}-#{artwork.name}"
  end

  def send_approval
    user = User.find_by email: "gravides.dren@gmail.com"
    ArtworkApprovalMailer.send_approval(self, user).deliver_now
  end
end
