class ContentBannerDtl < ActiveRecord::Base
  belongs_to :content_banner

  validates :contentable_id, :contentable_type, presence: true

  attr_accessor :contentable_name

  def contentable_name
    "#{contentable_type}-#{contentable_id}"
  end
  def contentable_name=(contentable_data)
    if contentable_data.present?
      contentable_data = contentable_data.split('-')
      self.contentable_id = contentable_data[1]
    end
  end
end
