class ContentRepository < ActiveRecord::Base
  has_attached_file :carousel_image, styles: {medium: "300x300>"}
  validates :title, :subtitle, presence: true
  validates_attachment_content_type :carousel_image,
    content_type: /^image\/(jpg|jpeg|pjpeg|png|x-png|gif)$/
end
