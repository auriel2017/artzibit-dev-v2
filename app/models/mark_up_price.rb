class MarkUpPrice < ActiveRecord::Base
  belongs_to :artwork

  validates :type, :input_value, presence: true
  attr_accessor :type, :input_value

  before_save :adjust_the_value
  before_create :add_effectivity_date

  # Callback Function
  def adjust_the_value
    unless type.blank?
      if type == "Percentage"
        self.percentage = self.input_value
      else
        self.price = self.input_value
      end
    end
  end
  def add_effectivity_date
    self.effectivity_date = Time.now
  end

  # Field Functions
  def type_name
    unless percentage.blank?
      "percentage"
    else
      "price"
    end
  end
  def value_amount
    unless percentage.blank?
      (percentage.to_f/100)
    else
      price
    end

  end
  def value
    unless percentage.blank?
      "#{percentage}%"
    else
      "$ #{price}"
    end
  end
end
