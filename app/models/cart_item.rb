class CartItem < ActiveRecord::Base
  belongs_to :cart
  belongs_to :artwork
  belongs_to :artwork_size
  belongs_to :artwork_price
  belongs_to :price_per_unit
  belongs_to :installation_price
  belongs_to :fixed_size
  belongs_to :fixed_size_price
  belongs_to :fixed_size_frame_price
  belongs_to :mark_up_price
  belongs_to :material
  belongs_to :frame_type

  validates :artwork_id, presence: true
  validates :fixed_size_id, :fixed_size_price_id, presence: true, if: "height.blank? or width.blank?"
  validates :fixed_size_frame_price_id, presence: true, if: "(height.blank? or width.blank?) and (frame == true)"
  validates :height, :width, presence: true, if: "fixed_size_id.blank?"
  validates :artwork_id, uniqueness: {scope: [:cart_id, :fixed_size_id,
    :material_id, :frame, :frame_type_id, :installation]}, if: "height.blank? or width.blank?"
  validates :artwork_id, uniqueness: {scope: [:cart_id, :height, :width,
    :material_id, :frame, :frame_type_id, :installation]}, if: "fixed_size_id.blank?"
  validate :min_and_max_size, if: "fixed_size_id.blank?"

  before_validation :save_min_fixed_size_price


  def self.update_to_current_price_fields id
    item = CartItem.find_by id: id
    if (item.height.blank? and item.width.blank?)
      # item.fixed_size_price_id = item.fixed_size.current_material_price(item.material_id).try(:id)
      item.fixed_size_price_id = FixedSize.find_by(id: item.fixed_size_id).current_material_price(item.material_id).try(:id)
      if item.frame?
        # item.fixed_size_frame_price_id = item.fixed_size.current_frame_type_price(item.frame_type_id).try(:id)
        item.fixed_size_frame_price_id = FixedSize.find_by(id: item.fixed_size_id).
          current_frame_type_price(item.frame_type_id).try(:id)
      else
        item.frame_type_id = nil
        item.fixed_size_frame_price_id = nil
      end
    else
      min_height_fixed_size = CartItem.minimum_size("height", item.size_type, item.height)
      item.min_height_fixed_size_price_id = min_height_fixed_size.current_material_price(item.material_id).try(:id)
      min_width_fixed_size = CartItem.minimum_size("width", item.size_type, item.width)
      item.min_width_fixed_size_price_id = min_width_fixed_size.current_material_price(item.material_id).try(:id)

      next_height_fixed_size = CartItem.next_fixed_size("height", item.size_type, min_height_fixed_size.try(:height))
      item.next_height_fixed_size_price_id = next_height_fixed_size.current_material_price(item.material_id).try(:id)
      next_width_fixed_size = CartItem.next_fixed_size("width", item.size_type, min_width_fixed_size.try(:width))
      item.next_width_fixed_size_price_id = next_width_fixed_size.current_material_price(item.material_id).try(:id)

      if item.frame?
        min_height_fixed_size = CartItem.minimum_size("height", item.size_type, item.height)
        item.min_height_fixed_size_frame_price_id = min_height_fixed_size.current_frame_type_price(item.frame_type_id).try(:id)
        min_width_fixed_size = CartItem.minimum_size("width", item.size_type, item.width)
        item.min_width_fixed_size_frame_price_id = min_width_fixed_size.current_frame_type_price(item.frame_type_id).try(:id)

        next_height_fixed_size = CartItem.next_fixed_size("height", item.size_type, min_height_fixed_size.try(:height))
        item.next_height_fixed_size_frame_price_id = next_height_fixed_size.current_frame_type_price(item.frame_type_id).try(:id)
        next_width_fixed_size = CartItem.next_fixed_size("width", item.size_type, min_width_fixed_size.try(:width))
        item.next_width_fixed_size_frame_price_id = next_width_fixed_size.current_frame_type_price(item.frame_type_id).try(:id)
      else
        item.frame_type_id = nil
        item.fixed_size_frame_price_id = nil
      end
    end
    if item.installation?
      item.installation_price_id = item.current_installation_range.try(:id)
    end
    item.mark_up_price_id = item.artwork.current_markup.try(:id)
    item
  end

  def save_min_fixed_size_price
    unless (height.blank? and width.blank?)
      min_height_fixed_size = CartItem.minimum_size("height", size_type, height)
      # self.min_height_fixed_size_price_id = min_height_fixed_size.try(:current_fixed_size_price_id)
      self.min_height_fixed_size_price_id = min_height_fixed_size.current_material_price(material_id).try(:id)
      min_width_fixed_size = CartItem.minimum_size("width", size_type, width)
      # self.min_width_fixed_size_price_id = min_width_fixed_size.try(:current_fixed_size_price_id)
      self.min_width_fixed_size_price_id = min_width_fixed_size.current_material_price(material_id).try(:id)

      next_height_fixed_size = CartItem.next_fixed_size("height", size_type, min_height_fixed_size.try(:height))
      # self.next_height_fixed_size_price_id = next_height_fixed_size.try(:current_fixed_size_price_id)
      self.next_height_fixed_size_price_id = next_height_fixed_size.current_material_price(material_id).try(:id)
      next_width_fixed_size = CartItem.next_fixed_size("width", size_type, min_width_fixed_size.try(:width))
      # self.next_width_fixed_size_price_id = next_width_fixed_size.try(:current_fixed_size_price_id)
      self.next_width_fixed_size_price_id = next_width_fixed_size.current_material_price(material_id).try(:id)

      if self.frame?
        min_height_fixed_size = CartItem.minimum_size("height", size_type, height)
        min_width_fixed_size = CartItem.minimum_size("width", size_type, width)
        # self.min_height_fixed_size_frame_price_id = min_height_fixed_size.try(:current_fixed_size_frame_price_id)
        self.min_height_fixed_size_frame_price_id = min_height_fixed_size.current_frame_type_price(frame_type_id).try(:id)
        # self.min_width_fixed_size_frame_price_id = min_width_fixed_size.try(:current_fixed_size_frame_price_id)
        self.min_width_fixed_size_frame_price_id = min_width_fixed_size.current_frame_type_price(frame_type_id).try(:id)
        next_height_fixed_size = CartItem.next_fixed_size("height", size_type, min_height_fixed_size.try(:height))
        next_width_fixed_size = CartItem.next_fixed_size("width", size_type, min_width_fixed_size.try(:width))
        # self.next_height_fixed_size_frame_price_id = next_height_fixed_size.try(:current_fixed_size_frame_price_id)
        self.next_height_fixed_size_frame_price_id = next_height_fixed_size.current_frame_type_price(frame_type_id).try(:id)
        # self.next_width_fixed_size_frame_price_id = next_width_fixed_size.try(:current_fixed_size_frame_price_id)
        self.next_width_fixed_size_frame_price_id = next_width_fixed_size.current_frame_type_price(frame_type_id).try(:id)
      end
    end
  end

  def aspect_ratio
    if size_type == "portrait"
      right_width = (height/3) * 2
      errors.add(:width, "Not in 2:3 aspect ratio. Width should be #{right_width}") unless right_width.round(1) == width.round(1)
    else
      right_width = (height/2) * 3
      errors.add(:width, "Not in 3:2 aspect ratio. Width should be #{right_width}") unless right_width.round(1) == width.round(1)
    end
  end

  def min_and_max_size
    unless (height.blank? and width.blank?)
      min_height = FixedSize.where("size_type = ?",size_type).order("height").first
      max_height = FixedSize.where("size_type = ?",size_type).order("height").last
      min_width = FixedSize.where("size_type = ?",size_type).order("width").first
      max_width = FixedSize.where("size_type = ?",size_type).order("width").last
      if size_type == "square"
        errors.add(:height, "The height range for Square is #{min_height.try(:height)}-#{max_height.try(:height)}") if height < min_height.try(:height) or height > max_height.try(:height)
        errors.add(:width, "The width range for Square is #{min_width.try(:height)}-#{max_width.try(:height)}") if width < min_width.try(:width) or width > max_width.try(:width)
      elsif size_type == "portrait"
        errors.add(:height, "The height range for Portrait is #{min_height.try(:height)}-#{max_height.try(:height)}") if height < min_height.try(:height) or height > max_height.try(:height)
        errors.add(:width, "The width range for Portrait is #{min_width.try(:width)}-#{max_width.try(:width)}") if width < min_width.try(:width) or width > max_width.try(:width)
      else
        errors.add(:height, "The height range for Portrait is #{min_height.try(:height)}-#{max_height.try(:height)}") if height < min_height.try(:height) or height > max_height.try(:height)
        errors.add(:width, "The width range for Portrait is #{min_width.try(:width)}-#{max_width.try(:width)}") if width < min_width.try(:width) or width > max_width.try(:width)
      end
    else
      errors.add(:base, "Please enter size")
    end
  end

  # FIELDS
  def size_type
    if (height.blank? and width.blank?)
      # fixed_size.try(:size_type)
      FixedSize.find_by(id: fixed_size_id).try(:size_type)
    else
      if height == width
        "square"
      elsif height < width
        "landscape"
      else
        "portrait"
      end
    end
  end

  # AMOUNTS
   def base_price saved=""
    total_height(saved).to_d.to_f + total_width(saved).to_d.to_f
  end
  def display_product_amount saved=""
    total_height(saved).to_d.to_f + total_width(saved).to_d.to_f
  end
   def total_base_price saved=""
    (base_price * quantity)
  end
  def product_amount saved=""
    base_price.round(2) + total_markup_price(base_price.round(2), saved)
  end
  def total_product_amount saved=""
    (product_amount(saved) * quantity)
  end
  def total_installation_amount saved=""
    (installation_amount(saved) * quantity)
  end
  def total_frame_amount saved=""
    (frame_amount(saved) * quantity)
  end
  def total_amount saved=""
    total_product_amount(saved).round(2) + total_installation_amount(saved).to_d.to_f.round(2) + total_frame_amount(saved).to_d.to_f.round(2)
  end
  def frame_amount saved=""
    total_frame_height(saved).to_d.to_f + total_frame_width(saved).to_d.to_f
  end
  def markup_percentage saved=""
    if saved.blank?
      artwork.current_markup.try(:percentage)
    else
      mark_up_price.try(:percentage)
    end
  end
  def installation_amount saved=""
    if installation
      if saved.blank?
        current_installation_range.try(:price).to_s.to_d.to_f
      else
        # installation_price.try(:price).to_s.to_d.to_f
        InstallationPrice.find_by(id: installation_price_id).try(:price).to_s.to_d.to_f
      end
    else
      0
    end
  end
  def delivery_amount
    if size_height < size_width
      @big_side = size_width
    else
      @big_side = size_height
    end
    del_pri = DeliveryPrice.joins(:delivery_range).where("delivery_ranges.with_installation = ? and (delivery_ranges.min_size <= ? and ? <= delivery_ranges.max_size) and delivery_prices.effectivity_date <= ?", self.installation?, @big_side, @big_side, Time.now).order("delivery_prices.effectivity_date asc").last.try(:price)
  end


  # HEIGHT/WIDTH COMPUTATION (PRICE)
  def total_height saved=""
    total_amount = 0
    unless height.blank?
      if saved.blank?
        min_size = minimum_size("height", size_type, height)
        minimum_height = min_size.try(:height)
        # minimum_price = min_size.try(:current_price)
        minimum_price = min_size.current_material_price_amount(material_id)
        min_base_price = minimum_price * 0.5
        dif_from_min = (height - minimum_height)
        adj_price = dif_from_min * amt_per_cm("height",minimum_height, minimum_price, nil, material_id)
        total_amount = (min_base_price + adj_price)
      else
        min_size = FixedSizePrice.find_by id: min_height_fixed_size_price_id,
          material_id: material_id
        # minimum_height = min_size.fixed_size.try(:height)
        minimum_height = FixedSize.find_by(id: min_size.fixed_size_id).try(:height)
        minimum_price = min_size.try(:price)
        min_base_price = minimum_price * 0.5
        dif_from_min = (height - minimum_height)
        adj_price = dif_from_min * amt_per_cm("height",minimum_height, minimum_price, saved, material_id)
        total_amount = (min_base_price + adj_price)
      end
    else
      if saved.blank?
        # total_amount = fixed_size_price.fixed_size.try(:current_price).to_s.to_d.to_f * 0.5
        total_amount = (FixedSize.find_by(id: fixed_size_id).
          current_material_price_amount(material_id).to_s.to_d.to_f) * 0.5
      else
        # total_amount = fixed_size_price.try(:price).to_s.to_d.to_f * 0.5
        total_amount = FixedSizePrice.find_by(id: fixed_size_price_id).try(:price).
          to_s.to_d.to_f * 0.5
      end
    end
    total_amount
  end
  def total_width saved=""
    total_amount = 0
    unless width.blank?
      # if (min_height_fixed_size_price_id.blank? || min_width_fixed_size_price_id.blank?)
      if saved.blank?
        min_size = minimum_size("width", size_type, width)
        minimum_width = min_size.try(:width)
        # minimum_price = min_size.try(:current_price)
        minimum_price = min_size.current_material_price_amount(material_id)
        min_base_price = minimum_price * 0.5
        dif_from_min = (width - minimum_width)
        adj_price = dif_from_min * amt_per_cm("width",minimum_width, minimum_price, nil, material_id)
        total_amount = (min_base_price + adj_price).to_d.to_f
      else
        # min_size = FixedSizePrice.find_by id: min_width_fixed_size_price_id
        min_size = FixedSizePrice.find_by id: min_width_fixed_size_price_id,
          material_id: material_id
        minimum_width = min_size.fixed_size.try(:width)
        minimum_price = min_size.try(:price)
        min_base_price = minimum_price * 0.5
        dif_from_min = (width - minimum_width)
        adj_price = dif_from_min * amt_per_cm("width",minimum_width, minimum_price, saved,
          material_id)
        total_amount = (min_base_price + adj_price)
      end
    else
      if saved.blank?
        # total_amount = fixed_size_price.fixed_size.try(:current_price).to_s.to_d.to_f * 0.5
        # total_amount = fixed_size_price.fixed_size.current_material_price_amount(material_id).to_s.to_d.to_f * 0.5
        total_amount = (FixedSize.find_by(id: fixed_size_id).
          current_material_price_amount(material_id).to_s.to_d.to_f) * 0.5
      else
        total_amount = fixed_size_price.try(:price).to_s.to_d.to_f * 0.5
      end
    end
    total_amount
  end
  def total_markup_price total_amount=0, saved= ""
    # if saved.blank?
      # return_amount = artwork.markup_price(total_amount).to_s.to_d.round(2)
    # else
      saved_markup_price = MarkUpPrice.find_by id: mark_up_price_id
      unless saved_markup_price.blank?
        mark_up_percentage = (saved_markup_price.percentage.to_d.to_f / 100).to_d.to_f
        return_amount = total_amount * mark_up_percentage
      else
        return_amount = 0
      end
    # end
    return_amount
  end
  def total_frame_height saved=""
    total_amount = 0
    if frame
      unless height.blank?
        if saved.blank?
          min_size = minimum_size("height", size_type, height)
          minimum_height = min_size.try(:height)
          # minimum_price = min_size.try(:current_frame_price)
          minimum_price = min_size.current_frame_type_price_amount(frame_type_id)
          min_base_price = minimum_price * 0.5
          dif_from_min = (height - minimum_height)
          adj_price = dif_from_min * amt_per_frame_cm("height",minimum_height, minimum_price, nil, frame_type_id)
          total_amount = (min_base_price + adj_price)
        else
          min_size = FixedSizeFramePrice.find_by id: min_height_fixed_size_frame_price_id,
            frame_type_id: frame_type_id
          minimum_height = min_size.fixed_size.try(:height)
          # minimum_price = min_size.fixed_size.try(:current_frame_price)
          minimum_price = min_size.fixed_size.current_frame_type_price_amount(frame_type_id)
          min_base_price = minimum_price * 0.5
          dif_from_min = (height - minimum_height)
          adj_price = dif_from_min * amt_per_frame_cm("height",minimum_height, minimum_price, saved, frame_type_id)
          total_amount = (min_base_price + adj_price)
        end
      else
        if saved.blank?
          # total_amount = fixed_size_frame_price.fixed_size.current_frame_type_price_amount(frame_type_id).to_s.to_d.to_f * 0.5
          total_amount = FixedSize.find_by(id: fixed_size_id).
            current_frame_type_price_amount(frame_type_id).to_s.to_d.to_f * 0.5
        else
          # total_amount = fixed_size_frame_price.try(:price).to_s.to_d.to_f * 0.5
          total_amount = FixedSizeFramePrice.find_by(id: fixed_size_frame_price_id).
            try(:price).to_s.to_d.to_f * 0.5
        end
      end
    end
    total_amount
  end
  def total_frame_width saved=""
    total_amount = 0
    if frame
      unless width.blank?
        if saved.blank?
          min_size = minimum_size("width", size_type, width)
          minimum_width = min_size.try(:width)
          # minimum_price = min_size.try(:current_frame_price)
          minimum_price = min_size.current_frame_type_price_amount(frame_type_id)
          min_base_price = minimum_price * 0.5
          dif_from_min = (width - minimum_width)
          adj_price = dif_from_min * amt_per_frame_cm("width",minimum_width, minimum_price, nil, frame_type_id)
          total_amount = (min_base_price + adj_price).to_d.to_f
        else
          min_size = FixedSizeFramePrice.find_by id: min_width_fixed_size_frame_price_id,
            frame_type_id: frame_type_id
          minimum_width = min_size.fixed_size.try(:width)
          # minimum_price = min_size.fixed_size.try(:current_frame_price)
          minimum_price = min_size.fixed_size.current_frame_type_price_amount(frame_type_id)
          min_base_price = minimum_price * 0.5
          dif_from_min = (width - minimum_width)
          adj_price = dif_from_min * amt_per_frame_cm("width",minimum_width, minimum_price, saved, frame_type_id)
          total_amount = (min_base_price + adj_price)
        end
      else
        if saved.blank?
          # total_amount = fixed_size_frame_price.fixed_size.current_frame_type_price_amount(frame_type_id).to_s.to_d.to_f * 0.5
          total_amount = FixedSize.find_by(id: fixed_size_id).
            current_frame_type_price_amount(frame_type_id).to_s.to_d.to_f * 0.5
        else
          total_amount = fixed_size_frame_price.try(:price).to_s.to_d.to_f * 0.5
        end
      end
    else
      total_amount = 0
    end
    total_amount
  end

  ##################################
  # LOCAL PROCEDURES
  def minimum_size side, size_type, size_value, saved=""
    FixedSize.where("size_type = ? and #{side} <= ?", size_type, size_value.to_d.to_f).
      order(side).last
  end
  def next_fixed_size side, minimum_value, saved=""
    FixedSize.where("size_type = ? and #{side} > ?", size_type, minimum_value.to_d.to_f).order(side).first
  end
  def amt_per_cm side, minimum_value, minimum_price, saved="", material_id=""
    if saved.blank?
      next_fixed = next_fixed_size(side, minimum_value)
      unless next_fixed.blank?
        next_size = next_fixed.try(side)
        # next_price = next_fixed.try(:current_price)
        next_price = next_fixed.current_material_price_amount(material_id)
        (((next_price - minimum_price).to_d.to_f/2).to_d.to_f / (next_size - minimum_value)).to_d.to_f
      else
        min_size = minimum_size(side, size_type, minimum_value)
        next_size = min_size.try(side) if next_size.blank?
        # next_price = min_size.try(:current_price) if next_price.blank?
        next_price = min_size.current_material_price_amount(material_id) if next_price.blank?
        (next_price)/(next_size).to_d.to_f
      end
    else
      if side == "height"
        next_fixed = FixedSizePrice.find_by id: next_height_fixed_size_price_id,
          material_id: material_id
      else
        next_fixed = FixedSizePrice.find_by id: next_width_fixed_size_price_id,
          material_id: material_id
      end
      unless next_fixed.blank?
        next_size = next_fixed.fixed_size.try(side)
        next_price = next_fixed.try(:price)
        (((next_price - minimum_price).to_d.to_f/2).to_d.to_f / (next_size - minimum_value)).to_d.to_f
      else
        if side == "height"
          min_size = FixedSizePrice.find_by id: min_height_fixed_size_price_id,
            material_id: material_id
        else
          min_size = FixedSizePrice.find_by id: min_width_fixed_size_price_id,
            material_id: material_id
        end
        next_size = min_size.fixed_size.try(side)
        next_price = min_size.try(:price)
        (next_price)/(next_size).to_d.to_f
      end
    end
  end
  def amt_per_frame_cm side, minimum_value, minimum_price, saved="", frame_type_id=""
    if saved.blank?
      next_fixed = next_fixed_size(side, minimum_value)
      unless next_fixed.blank?
        next_size = next_fixed.try(side)
        # next_price = next_fixed.try(:current_frame_price)
        next_price = next_fixed.current_frame_type_price_amount(frame_type_id)
        (((next_price - minimum_price).to_d.to_f/2).to_d.to_f / (next_size - minimum_value)).to_d.to_f
      else
        min_size = minimum_size(side, size_type, minimum_value)
        next_size = min_size.try(side) if next_size.blank?
        # next_price = min_size.try(:current_frame_price) if next_price.blank?
        next_price = min_size.current_frame_type_price_amount(frame_type_id) if next_price.blank?
        (next_price)/(next_size).to_d.to_f
      end
    else
      if side == "height"
        next_fixed = FixedSizeFramePrice.find_by id: next_height_fixed_size_frame_price_id,
          frame_type_id: frame_type_id
      else
        next_fixed = FixedSizeFramePrice.find_by id: next_height_fixed_size_frame_price_id,
          frame_type_id: frame_type_id
      end
      unless next_fixed.blank?
        next_size = next_fixed.fixed_size.try(side)
        next_price = next_fixed.try(:price)
        (((next_price - minimum_price).to_s.to_d.to_f/2).to_s.to_d.to_f / (next_size - minimum_value)).to_s.to_d.to_f
      else
        if side == "height"
          min_size = FixedSizeFramePrice.find_by id: min_height_fixed_size_frame_price_id,
            frame_type_id: frame_type_id
        else
          min_size = FixedSizeFramePrice.find_by id: min_width_fixed_size_frame_price_id,
            frame_type_id: frame_type_id
        end
        next_size = min_size.fixed_size.try(side)
        next_price = min_size.try(:price)
        (next_price)/(next_size).to_d.to_f
      end
    end
  end


  ##################################
  # GLOBAL PROCEDURES

  def self.amt_per_cm side, size_type, minimum_value, minimum_price
    next_fixed = self.next_fixed_size(side, size_type, minimum_value)
    unless next_fixed.blank?
      next_size = next_fixed.try(side)
      next_price = next_fixed.try(:current_price)
      (((next_price - minimum_price).to_d.to_f/2).to_d.to_f / (next_size - minimum_value)).to_d.to_f
    else
      min_size = self.minimum_size(side, size_type, minimum_value)
      next_size = min_size.try(side) if next_size.blank?
      next_price = min_size.try(:current_price) if next_price.blank?
      (next_price)/(next_size).to_d.to_f
    end
  end
  def self.next_fixed_size side, size_type, minimum_value
    FixedSize.where("size_type = ? and #{side} > ?", size_type, minimum_value.to_d.to_f).order(side).first
  end
  def self.minimum_size side, size_type, size_value
    FixedSize.where("size_type = ? and #{side} <= ?", size_type, size_value.to_d.to_f).
      order(side).last
  end

  def self.amt_per_cm_v2 side, size_type, minimum_value, minimum_price, material_id=""
    next_fixed = self.next_fixed_size(side, size_type, minimum_value)
    unless next_fixed.blank?
      next_size = next_fixed.try(side)
      next_price = next_fixed.current_material_price_amount(material_id)
      (((next_price - minimum_price).to_d.to_f/2).to_d.to_f / (next_size - minimum_value)).to_d.to_f
    else
      min_size = self.minimum_size(side, size_type, minimum_value)
      next_size = min_size.try(side) if next_size.blank?
      next_price = min_size.current_material_price_amount(material_id) if next_price.blank?
      (next_price)/(next_size).to_d.to_f
    end
  end
  def self.amt_per_frame_cm_v2 side, size_type, minimum_value, minimum_price, frame_type_id=""
    next_fixed = self.next_fixed_size(side, size_type, minimum_value)
    unless next_fixed.blank?
      next_size = next_fixed.try(side)
      next_price = next_fixed.current_frame_type_price_amount(frame_type_id)
      (((next_price - minimum_price).to_d.to_f/2).to_d.to_f / (next_size - minimum_value)).to_d.to_f
    else
      min_size = self.minimum_size(side, size_type, minimum_value)
      next_size = min_size.try(side) if next_size.blank?
      next_price = min_size.current_frame_type_price_amount(frame_type_id) if next_price.blank?
      (next_price)/(next_size).to_d.to_f
    end
  end
  ##################################

  def current_installation_range
    if size_height < size_width
      @big_side = size_width
    else
      @big_side = size_height
    end
    installation_range = InstallationPrice.joins(:installation_range).where("(installation_ranges.min_size <= ? and ? <= installation_ranges.max_size) and installation_prices.effectivity_date <= ?", @big_side, @big_side, Time.now).order("installation_prices.effectivity_date asc").last
  end

  def v1_format
    {
      id: id,
      image: artwork.featured_image,
      name: artwork.name,
      base_price: product_amount,
      price: total_product_amount,
      size: size,
      quantity: quantity,
      installation: installation,
      installation_amount: total_installation_amount,
      base_installation_price: installation_amount,
      frame: frame,
      frame_amount: total_frame_amount,
      base_frame_price: frame_amount,
      delivery_amount: delivery_amount,
      current_quantity: artwork.current_quantity,
      artwork_id: artwork.id
    }
  end

  def v2_format
    {
      id: id,
      artwork_id: artwork.try(:id),
      image: artwork.featured_image,
      name: artwork.name,
      artist_name: artwork.user.try(:name),
      size: size_v2,
      material: material.try(:name),
      frame_type: frame_name,
      price: total_amount.to_d.round(2),
      quantity: quantity,
      fixed_size_id: fixed_size_id,
      material_id: material_id,
      frame_type_id: frame_type_id,
      installation: installation,
      frame: frame
    }
  end


  def frame_name
    if frame_type.blank?
      "No Frame"
    else
      frame_type.try(:name)
    end
  end

  # def installation
  #   if super == ""
  #     false
  #   else
  #     super
  #   end
  # end

  def installation_name
    if installation
      "Yes"
    else
      "No"
    end
  end

  def size
    unless height.blank?
      "#{width} X #{height}"
    else
      fixed_size.try(:name_with_no)
    end
  end

  def size_v2
    unless height.blank?
      "#{width} X #{height}"
    else
      fixed_size.try(:size)
    end
  end

  def size_height
    unless height.blank?
      height
    else
      # fixed_size.try(:height)
      FixedSize.where(id: fixed_size_id).pluck(:height).first
    end
  end

  def size_width
    unless width.blank?
      width
    else
      # fixed_size.try(:width)
      FixedSize.where(id: fixed_size_id).pluck(:width).first
    end
  end
end
