class UserBankDetail < ActiveRecord::Base
  belongs_to :user
  attr_accessor :card_id, :number, :to_delete
  def cards
    customer = Stripe::Customer.retrieve(stripe_id)
    def_source = customer.default_source
    customer.sources.map{|a|
      {
        id: id,
        card_id: a.try(:id),
        is_default: def_source == a.try(:id),
        brand: a.try(:brand),
        last4: a.try(:last4),
        exp_month: a.try(:exp_month),
        exp_year: a.try(:exp_year)
      }
    }
  end
  def default_card
    customer_stripe_id = stripe_id
    begin
      customer = Stripe::Customer.retrieve(customer_stripe_id)
    rescue
      {}
    else
      begin
        card = customer.sources.retrieve(customer.default_source)
      rescue
        {}
      else
        card_v2_format customer.default_source, card
      end
    end
  end

  def retrieve_card card_id
    customer_stripe_id = stripe_id
    begin
      customer = Stripe::Customer.retrieve(customer_stripe_id)
    rescue
      {}
    else
      begin
        card = customer.sources.retrieve(card_id)
      rescue
        {}
      else
        card_v2_format customer.default_source, card
      end
    end
  end

  def card_v2_format def_source, card
      {
        id: id,
        card_id: card.try(:id),
        is_default: def_source == card.try(:id),
        brand: card.try(:brand),
        last4: card.try(:last4),
        exp_month: card.try(:exp_month),
        exp_year: card.try(:exp_year)
      }
  end
end
