class Review < ActiveRecord::Base
  belongs_to :user
  belongs_to :artwork

  scope :by_date, -> {order(created_at: :desc)}

  validates :user_id, :content, presence: true
  has_attached_file :image
  validates_attachment_content_type :image,
    :content_type => /^image\/(jpg|jpeg|png)$/,
    :size => {in: 0..75.kilobytes}, unless: "image.blank?"

  # Field Functions
  def image_url
    if image.blank?
      nil
    else
      "https:#{image.url}"
    end
  end

  # API Functions
  def v2_format
    {
      id: id,
      image: image_url,
      user_name: User.find_by(id: user_id).try(:full_name),
      content: content,
      created_at: created_at.strftime("%b %d, %Y %H:%M:%S")
    }
  end
end
