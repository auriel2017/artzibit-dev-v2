class FixedSize < ActiveRecord::Base
  SIZE_LABELS = {"small" => "Small", "medium" => "Medium", "large" => "Large",
    "xlarge" => "Extra Large"}
  SIZE_TYPES = {"square" => "Square", "landscape" => "Landscape",
    "portrait" => "Portrait"}

  scope :by_name, -> {where("size_type IS NOT NULL").order("fixed_sizes.size_type, (fixed_sizes.width + fixed_sizes.height)")}

  validates :label, :size_type, :height, :width, presence: true
  validates :label, uniqueness: {scope: [:size_type, :height, :width]}

  has_many :fixed_size_prices
  has_many :fixed_size_frame_prices
  has_many :cart_items

  accepts_nested_attributes_for :fixed_size_prices, allow_destroy: true
  accepts_nested_attributes_for :fixed_size_frame_prices, allow_destroy: true

  before_destroy :delete_prices

  def delete_prices
    FixedSizePrice.where(fixed_size_id: id).delete_all
    FixedSizeFramePrice.where(fixed_size_id: id).delete_all
  end

  # Naming Functions
  def name
    "#{SIZE_TYPES[size_type]}: #{SIZE_LABELS[label]} (#{size})"
  end
  def name_with_no
    "#{SIZE_LABELS[label]}: #{width}cm X #{height}cm"
  end
  def size
    "#{width} cm X #{height} cm"
  end
  def fixed_size_prices_by_date
    FixedSizePrice.where(fixed_size_id: id).by_date
  end
  def fixed_size_frame_prices_by_date
    FixedSizeFramePrice.where(fixed_size_id: id).by_date
  end

  # Current Price Functions
  def current_fixed_size_price
    FixedSizePrice.where("fixed_size_id = ? and effectivity_date <= ?", id, Time.now).order("effectivity_date desc").first
  end
  def current_fixed_size_price_id
    current_fixed_size_price.try(:id)
  end
  def current_price
    current_fixed_size_price.try(:price)
  end
  def current_fixed_size_frame_price
    fixed_size_frame_prices.where("effectivity_date <= ?", Time.now).order("effectivity_date desc").first
  end
  def current_fixed_size_frame_price_id
    current_fixed_size_frame_price.try(:id)
  end
  def current_frame_price
    current_fixed_size_frame_price.try(:price)
  end

  # Naming Functions -- V2 Functions
  def current_material_price material_id
    fixed_size_prices.where("material_id = ? and effectivity_date <= ?",
      material_id, Time.now).order("effectivity_date desc").first
  end
  def current_material_price_amount material_id
    current_material_price(material_id).try(:price)
  end
  def current_frame_type_price frame_type_id
    fixed_size_frame_prices.where("frame_type_id = ? and effectivity_date <= ?",
      frame_type_id, Time.now).order("effectivity_date desc").first
  end
  def current_frame_type_price_amount frame_type_id
    current_frame_type_price(frame_type_id).try(:price)
  end
  def amount_per_unit side, size_type, material_id
    next_size = FixedSize.where("size_type = '#{size_type}' and #{side} > #{self.try(side)}").first
    unless next_size.blank?
      ((next_size.current_material_price_amount(material_id) - current_material_price_amount(material_id))/(next_size.try(side) - self.try(side))/2).to_s.to_d.to_f.round(3)
    else
      0
    end
  end
  def amount_frame_per_unit side, size_type, frame_type_id
    next_size = FixedSize.where("size_type = '#{size_type}' and #{side} > #{self.try(side)}").first
    unless next_size.blank?
      ((next_size.current_frame_type_price_amount(frame_type_id) - current_frame_type_price_amount(frame_type_id))/(next_size.try(side) - self.try(side))/2).to_s.to_d.to_f.round(3)
    else
      0
    end
  end

  # API Functions
  def v1_format artwork=nil
    current_p = current_price.to_s.to_d.to_f
    current_p += artwork.markup_price(current_price)

    height_min = CartItem.minimum_size("height", size_type, height)
    width_min = CartItem.minimum_size("width", size_type, width)

    height_amt_per_cm = CartItem.amt_per_cm("height",size_type,height_min.try(:height),
      height_min.try(:current_price)).to_s.to_d.to_f
    height_amt_per_cm += artwork.markup_price(height_amt_per_cm)

    width_amt_per_cm = CartItem.amt_per_cm("width",size_type,width_min.try(:width),
      width_min.try(:current_price)).to_s.to_d.to_f
    width_amt_per_cm += artwork.markup_price(width_amt_per_cm)

    {
      label: name_with_no,
      fixed_size_id: id,
      height: height.to_i,
      width: width.to_i,
      fixed_size_price_id: current_fixed_size_price.try(:id),
      fixed_size_frame_price_id: current_fixed_size_frame_price.try(:id),
      price: current_p,
      height_amount_per_cm: height_amt_per_cm,
      width_amount_per_cm: width_amt_per_cm
    }
  end
  def v2_format model="", model_id="", selected_id="", price_id=""
    height_min = CartItem.minimum_size("height", size_type, height)
    width_min = CartItem.minimum_size("width", size_type, width)
    case model
      when "material"
        height_amt_per_cm = amount_per_unit("height", size_type, model_id).to_s.to_d.to_f.round(3)
        width_amt_per_cm = amount_per_unit("width", size_type, model_id).to_s.to_d.to_f.round(3)
        {
          price: current_material_price_amount(model_id),
          width: width,
          height: height,
          height_per_cm: height_amt_per_cm,
          width_per_cm: width_amt_per_cm,
          fixed_size_id: id,
          fixed_size_price_id: current_material_price(model_id).try(:id),
          selected: (id == selected_id and price_id == current_material_price(model_id).try(:id))
        }
      when "frame_type"
        height_amt_per_frame_cm = amount_frame_per_unit("height", size_type, model_id).to_s.to_d.to_f.round(3)
        width_amt_per_frame_cm = amount_frame_per_unit("width", size_type, model_id).to_s.to_d.to_f.round(3)
        {
          price: current_frame_type_price_amount(model_id),
          width: width,
          height: height,
          height_per_cm: height_amt_per_frame_cm,
          width_per_cm: width_amt_per_frame_cm,
          fixed_size_id: id,
          fixed_size_frame_price_id: current_frame_type_price(model_id).try(:id),
          selected: (id == selected_id and price_id == current_frame_type_price(model_id).try(:id))
        }
      else
        {
          id: id,
          name: name_with_no,
          width: width,
          height: height,
          selected: id == selected_id
        }
    end
  end
end
