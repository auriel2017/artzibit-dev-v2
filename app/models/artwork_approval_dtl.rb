class ArtworkApprovalDtl < ActiveRecord::Base
  STATUS = [["Approve", "approve"], ["Reject", "reject"], ["Pending","pending"]]

  scope "pendings", -> {where(status: "pending")}
  scope "approves", -> {where(status: "approve")}
  scope "rejects", -> {where(status: "reject")}

  belongs_to :artwork_approval_hdr
  belongs_to :admin_user

  validate :status_fields

  before_create :add_hash_token
  before_update :send_status_artworks
  after_save :send_approval, if: "status == 'pending'"

  def send_status_artworks
    if artwork_approval_hdr.artwork.status == "pending"
      no_of_approval = NoOfApproval.where("effectivity_date <= ?", Time.now).order(:effectivity_date).last.try(:count)
      no_of_approval ||= 2

      imaginary_pending = artwork_approval_hdr.artwork_approval_dtls.pendings.count - 1
      imaginary_approves = artwork_approval_hdr.artwork_approval_dtls.approves.count
      imaginary_rejects = artwork_approval_hdr.artwork_approval_dtls.rejects.count

      if status == "approve"
        imaginary_approves += 1
      else
        imaginary_rejects += 1
      end

      next_status = ""

      if imaginary_approves >= no_of_approval
        next_status = "approve"
      else
        if (imaginary_pending >= (no_of_approval - imaginary_approves) and artwork_approval_hdr.expiration_date.to_i >= Time.now.to_i)
          next_status = "pending"
        else
          next_status = "reject"
        end
      end

      if next_status == "approve"
        ArtworkApprovalMailer.send_approve_artwork(self, admin_user).deliver_now
      elsif next_status == "reject"
        ArtworkApprovalMailer.send_reject_artwork(self, admin_user).deliver_now
      end
    end

  end
  def status_fields
    approval = ArtworkApprovalDtl.find_by_id id
    unless approval.blank?
      unless (approval.status == self.status and approval.processed_at.blank?)
        errors.add(:status, "You already decided for this Artwork") if approval.status != "pending"
      end
    end
  end
  def add_hash_token
    hash_token = Digest::SHA1.hexdigest "#{id}-#{admin_user.email}-#{
      artwork_approval_hdr.artwork.name}-#{Time.now}"
    self.approve_token = hash_token
  end
  def send_approval
    ArtworkApprovalMailer.send_approval(self, admin_user).deliver_now
  end
end
