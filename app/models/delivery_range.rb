class DeliveryRange < ActiveRecord::Base
  validates :min_size, :max_size, presence: true
  has_many :delivery_prices
  accepts_nested_attributes_for :delivery_prices, allow_destroy: true
  def name
    "#{min_size} - #{max_size} #{'(with Installation)' if with_installation?}"
  end
  def size
    "#{min_size} cm - #{max_size} cm"
  end
  def current_delivery_price
    delivery_prices.where("effectivity_date <= ?",Time.now).order("effectivity_date desc").first
  end
  def current_price
    current_delivery_price.try(:price)
  end
end
