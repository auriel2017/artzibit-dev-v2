class User < ActiveRecord::Base
  ARTIST_TYPE = {"" => "Not an artist", "photographer" => "Photographer",
    "visual_artist" => "Visual Artist"}
  acts_as_token_authenticatable

  scope :artists, -> {where("artist_type = 'photographer' or artist_type = 'visual_artist' ")}
  scope :photographers, -> {where("artist_type = 'photographer' ")}
  scope :visual_artists, -> {where("artist_type = 'visual_artist' ")}
  scope :artists, -> {where("artist_type = 'photographer' or artist_type =
    'visual_artist'")}
  scope :by_name, -> {order(:name)}
  scope :active, -> {where(active: true).order(:name)}
  scope :inactive, -> {where(active: false).order(:name)}

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable
  has_one :user_authentication, dependent: :destroy
  has_many :user_favorites, dependent: :destroy
  has_one :user_verified, dependent: :destroy
  has_many :carts, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :user_bank_details, dependent: :destroy
  has_many :user_shipping_details, dependent: :destroy
  has_many :artworks, dependent: :destroy
  has_many :artwork_histories, dependent: :destroy
  has_many :user_views, dependent: :destroy
  has_many :user_launch_app_counters, dependent: :destroy
  has_many :user_app_tracking_logs, dependent: :destroy
  has_many :user_activation_codes, dependent: :destroy
  has_many :activation_codes, through: :user_activation_codes
  has_many :promo_code_dtls, as: :promo_codable, dependent: :destroy
  has_many :promo_codes, through: :promo_code_dtls

  has_attached_file :avatar
  validates_attachment_content_type :avatar,
    content_type: /^image\/(jpg|jpeg|pjpeg|png|x-png|gif)$/
  validates :country, presence: true
  validates :username, uniqueness: true, unless: "username.blank?"
  validates :name, presence: true, if: "first_name.blank? or last_name.blank?"
  validates :first_name, :last_name, presence: true, if: "name.blank?"
  validates :contact_no, presence: true

  validate :number_per_activation_code
  validates :email, :email_format => { :message => 'is not in valid format' }

  accepts_nested_attributes_for :user_authentication, allow_destroy: true
  attr_accessor :reserved_token, :current_password

  after_create :update_activation_code, unless: "by_pass_activation_code == true"
  after_create :send_signup_email

  # Callback Functions
  def send_signup_email
    UserMailer.delay.successful_signup self
  end
  def update_activation_code
    @activation_code = UserActivationCode.find_by reserved_token: reserved_token
    @activation_code.user_id = id
    @activation_code.save
  end

  # Authentication Functions
  def self.authenticate(params={})
    user = User.find_for_authentication(:email => params[:email])
    return nil if user.blank?
    user.valid_password?(params[:password]) ? user : nil
  end
  def password_required?
    super && user_authentication.blank?
  end
  def confirmation_required?
    if log_thru_facebook?
      skip_confirmation! unless destroyed?
      false
    end
  end
  def log_thru_facebook?
    user_authentication.present?
  end
  def auth_exists?
    if User.find_by_email(email).present?
      unless auth.blank?
        true
      else
        false
      end
    else
      false
    end
  end
  def auth
    user = User.find_by_email(email)
    uauth = user_authentication
    unless UserAuthentication.where(provider: uauth.provider, uid: uauth.uid,
      user_id: user.id).blank?
      user
    else
      nil
    end
  end
  # (V2)
  def thru_email?
    email.present? and password.present? and user_authentication.blank?
  end
  def thru_fb?
    email.present? and password.blank? and user_authentication.present?
  end
  def authenticate
    user = User.find_for_authentication email: email
    return nil if user.blank?
    user.valid_password?(password) ? user : nil
  end
  def has_facebook?
    UserAuthentication.where(user_id: id).present?
  end

  # Sequence Functions
  def artwork_sequence
    first_artwork = artworks.actives.order("id asc").first.try(:id)
  end
  def index_in_first_artwork
    ind_order = User.all.map{|a| [a.id, a.artwork_sequence]}.sort{|a,b| a[1].to_i <=> b[1].to_i}.reject{|a| a[1].nil?}.map.with_index{|a,index| [a[0],index]}.select{|a| a[0] == id}[0]
    (ind_order[1] + 1) unless ind_order.blank?
  end

  # Activation Code Functions
  def number_per_activation_code
    if by_pass_activation_code == false
      if id.blank?
        @activation_code = UserActivationCode.find_by reserved_token: reserved_token
        unless @activation_code.blank?
          errors.add(:activation_code_id, "Reserve Token expires") if \
            @activation_code.expiration < Time.now
        else
          errors.add(:activation_code_id, "Activation Code Not found")
        end
      end
    end
  end

  # Counter Functions
  def artwork_view_counter
    user_views.map{|u| u.counter}.inject(0){|sum,x| sum + x}
  end
  def app_launch_counter
    user_launch_app_counters.map{|u| u.counter}.inject(0){|sum,x| sum + x}
  end
  def artwork_view_duration
    total_duration = user_app_tracking_logs.artwork.map{|u| u.duration}.inject(0){|sum,x| sum + x}
    Time.at(total_duration).utc.strftime("%H:%M")
  end
  def app_stay_duration
    total_duration = user_app_tracking_logs.app.map{|u| u.duration}.inject(0){|sum,x| sum + x}
    Time.at(total_duration).utc.strftime("%H:%M")
  end

  # Stripe Functions
  def add_stripe_id token
    customer = Stripe::Customer.create(
      :email => email,
      :source => token
    )
    UserBankDetail.create(user_id: id, stripe_id: customer.id)
  end
  def stripe_id
    user_bank_details.order("updated_at desc").first.try(:stripe_id)
  end
  def stripe_details
    stripe_id
  end

  # Verify Functions
  def verified
    if user_verified.blank?
      false
    else
      !user_verified.deleted
    end
  end
  def verify
    user_verify = UserVerified.new(user_id: self.id, deleted:false, effectivity_date: Time.now)
    user_verify
  end
  def unverify
    user_unverify = UserVerified.find_by_user_id self.id
    user_unverify.deleted = !user_unverify.deleted
    user_unverify
  end

  # Naming Functions
  def shippings_by_name
    UserShippingDetail.where(user_id: id, active: true).order(:first_name)
  end
  def name_with_email
    "#{name} : <#{email}>"
  end
  def last_shipping_detail
    ship_details = user_shipping_details.actives.where(is_default: true)
    ship_details = user_shipping_details.actives.order(:created_at).first if ship_details.blank?
    ship_details
  end
  def first_name
    super.blank? ? name : super
  end
  def full_name
    "#{first_name} #{last_name}"
  end

  # For Content Repository
  def content_name
    "User-#{id}"
  end

  # API Functions
  def v1_format
    {
      id: id,
      name: name,
      artworks: artworks.actives.count
    }
  end
  def v2_format controller=""
    if controller == "filter"
      {
        id: id,
        name: full_name
      }
    else
      {
        id: id,
        email: email,
        authentication_token: authentication_token,
        first_name: first_name,
        last_name: last_name,
        country: country,
        username: username
      }
    end
  end

  def current_cart
    current = Cart.where("user_id = ?", id).available.first
    current ||= Hash.new
  end
  def current_cart_id
    Cart.where("user_id = ?", id).available.
      first.try(:id)
  end
  def default_shipping_detail
    default = UserShippingDetail.where(user_id: id, is_default: true,
      active: true).last
    default ||= Hash.new
  end
  def default_bank_detail
    # UserBankDetail.where(user_id: id, is_default: true, active: true).last
    UserBankDetail.where(user_id: id).order("updated_at desc").first
  end
end