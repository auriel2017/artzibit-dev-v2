class Support < ActiveRecord::Base
  belongs_to :user
  validates :user_id, :content, presence: true

  after_create :send_support_email

  def send_support_email
    SupportMailer.send_support(self).deliver_now
  end
end
