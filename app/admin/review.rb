ActiveAdmin.register Review do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Users"
  actions :all
  index do
    selectable_column
    column :id
    column "User" do |r|
      link_to User.find_by(id: r.user_id).try(:full_name),
        admin_user_path(r.user_id)
    end
    column "Artwork" do |r|
      link_to Artwork.where(id: r.artwork_id).pluck(:name).first,
        admin_artwork_path(r.artwork_id)
    end
    column "Image" do |r|
      image_tag(url_for(r.image.url), style: "width:30%;") \
        unless r.image.blank?
    end
    column "Content" do |r|
      truncate r.content
    end
    column "Created At" do |r|
      r.created_at.strftime("%b %d, %Y %H:%M:%S")
    end
    actions
  end
  show do
    panel(I18n.t('active_admin.details',
      model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :user
        row :artwork
        row "Image" do
          image_tag(url_for(resource.image.url), style: "width:25%") unless resource.image.blank?
        end
        row :content
        row "Created At" do
          resource.created_at.strftime("%b %d, %Y %H:%M:%S")
        end
      end
    end
  end
  permit_params :id, :user_id, :artwork_id, :content, :image
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :user, input_html: {class: "chosen-input",
        style: "width:75% !important"}, collection: User.by_name.map{|u|[u.name,
        u.id]}, include_blank: false
      f.input :artwork, input_html: {class: "chosen-input",
          style: "width:75% !important"}, collection: Artwork.by_name.
          map{|u|[u.name, u.id]}, include_blank: false
      if f.object.new_record?
        f.input :image, as: :file
      else
        f.input :image, as: :file, hint: (f.object.image.blank? ?
          "No Image" : image_tag(f.object.image.url, style: "width: 25%") )
      end
      f.input :content
    end
    f.actions
  end
end