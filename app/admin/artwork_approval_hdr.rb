# ActiveAdmin.register ArtworkApprovalHdr do
#   menu if: proc{ current_admin_user.admin? }
#   actions :all
#   filter :artwork
#   index do
#     selectable_column
#     column :id
#     column :artwork
#     column :expiration_date
#     column "Pending" do |a|
#       a.artwork_approval_dtls.pendings.count
#     end
#     column "Approve" do |a|
#       a.artwork_approval_dtls.approves.count
#     end
#     column "Reject" do |a|
#       a.artwork_approval_dtls.rejects.count
#     end
#     actions
#   end
#   show do
#     panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
#       attributes_table_for resource do
#         row :id
#         row "Artwork" do
#           resource.artwork.name
#           resource.expiration_date
#         end
#       end
#     end
#     panel "Approvals" do
#       table_for resource.artwork_approval_dtls.order(:id) do
#         column "Vetter" do |a|
#           a.admin_user.name
#         end
#         column :status
#         column :processed_at
#       end
#     end
#   end
#   form do |f|
#     f.semantic_errors *f.object.errors.keys
#     f.inputs do
#       f.input :artwork, input_html: { class: "chosen-input", style: "width:75%
#         !important"}, collection: @artwork, prompt: true, include_blank: true,
#         allow_blank: true, selected: f.object.artwork_id
#       f.input :expiration_date, :as => :string, :input_html => {:class => "hasDatetimePicker", value: Time.now}
#       f.has_many :artwork_approval_dtls, heading: "Approvals", allow_destroy: true,
#         new_record: "Add Approval" do |approval|
#           approval.input :admin_user, collection: AdminUser.where(vetter: true).
#             map{|u|[u.name, u.id]}, allow_blank: false, prompt: "Select an Admin User",
#             input_html: {class: "chosen-input", style: "width:75% !important"}
#           approval.input :status, collection: ArtworkApprovalDtl::STATUS
#           approval.input :processed_at
#       end
#     end
#     f.actions
#   end
#   permit_params :id, :artwork_id, :expiration_date, artwork_approval_dtls_attributes: [:id,
#     :artwork_approval_hdr_id, :admin_user_id, :status, :processed_at]
# end