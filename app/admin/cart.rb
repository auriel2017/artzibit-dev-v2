ActiveAdmin.register Cart do
  menu if: proc{ current_admin_user.admin? }
  menu label: "Carts", priority: 800
  form do |f|
    @chose = "chosen-input"
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :shippings, as: :hidden,
        collection: option_groups_from_collection_for_select(User.by_name,
        :shippings_by_name, :name_with_email, :id, :name_w_addr)
      f.input :user, allow_blank: true, prompt: true,
        collection: User.by_name.order(:name).map{|u| [u.name_with_email, u.id]},
        input_html: {class: "chosen-input", style: "width:75% !important",
        onchange: "upd_populate_user_shipping(this.id, 'cart');"}
      f.input :user_shipping_detail, allow_blank: true,
        input_html: {class: "chosen-input", style: "width:75% !important"},
        collection: option_groups_from_collection_for_select(User.by_name,
        :shippings_by_name, :name_with_email, :id, :name_w_addr,
        f.object.user_shipping_detail_id)
      f.input :status, as: :select, include_blank: true, prompt: false,
        collection: [["Available",nil], ["Checkout","checkout"]],
        input_html: {class: "chosen-input", value: f.object.status}
      f.input :delivery_price, include_blank: false, prompt: false,
        input_html: {style: "width:75% !important", class: "chosen-input"},
        collection: DeliveryPrice.all.map{|u|[u.name, u.id]}
      f.input :created_by_admin, input_html: {value: true, hidden: true}
      f.input :fixed_sizes, input_html: {style:"display:none;"},
        collection: option_groups_from_collection_for_select(Artwork.by_name,
        :artwork_fixed_sizes, :name, :id, :name)
      f.input :fixed_size_prices, input_html: {style: "display:none;"},
        collection: option_groups_from_collection_for_select(FixedSize.all,
        :fixed_size_prices_by_date, :name, :id, :name)
      f.input :fixed_size_frame_prices, input_html: {style: "display:none"},
        collection: option_groups_from_collection_for_select(FixedSize.all,
        :fixed_size_frame_prices_by_date, :name, :id, :price)
      f.input :materials, input_html: {style: "display:none"},
      collection: option_groups_from_collection_for_select(Material.all.joins(
        fixed_size_prices: :fixed_size).select("fixed_sizes.id as fixed_size_id,
        materials.id as material_id").order("materials.name, fixed_sizes.label asc,
        fixed_sizes.width"), :fixed_size_prices_by_date, :name_label, :id, :price)
      f.input :frame_types, input_html: {style: "display:none"},
      collection: option_groups_from_collection_for_select(FrameType.all.joins(
        fixed_size_frame_prices: :fixed_size).select("fixed_sizes.id as fixed_size_id,
        frame_types.id as frame_type_id").order("frame_types.name, fixed_sizes.label asc,
        fixed_sizes.width"), :fixed_size_frame_prices_by_date, :name_label, :id,
        :price)
      f.input :mark_up_prices, input_html: {style:"display:none;"},
        collection: option_groups_from_collection_for_select(Artwork.by_name,
        :mark_up_prices_by_date, :name, :id, :value)

      f.has_many :cart_items, heading: "Artworks", allow_destroy: true,
        new_record: "Add Artwork", input_html: {class: "chosen-input"} do |art|
        art.input :artwork, collection: Artwork.by_name.map{|u|[u.name, u.id]},
          allow_blank: true, prompt: "Select an Artwork",
          input_html: {style: "width:75% !important", class: "chosen-input",
          onclick: "upd_populate_artwork_fixed_sizes(this.id, 'cart_cart_items_attributes');
            upd_populate_artwork_markup_price(this.id, 'cart_cart_items_attributes');",
          onchange: "upd_populate_artwork_fixed_sizes(this.id, 'cart_cart_items_attributes');
            upd_populate_artwork_markup_price(this.id, 'cart_cart_items_attributes');"}
        art.input :quantity, input_html: {value: 1}
        art.input :mark_up_price,
          input_html: {style: "width:75% !important", class: "chosen-input"},
          collection: option_groups_from_collection_for_select(Artwork.by_name,
          :mark_up_prices_by_date, :name, :id, :value, art.object.mark_up_price_id)
        art.input :fixed_size, allow_blank: true, include_blank: true,
          input_html: {style: "width:75% !important", class: "chosen-input fixed_size_select",
          onchange: "upd_populate_artwork_fixed_prices(this.id,'cart_cart_items_attributes');
            upd_populate_artwork_frame_prices(this.id,'cart_cart_items_attributes');
            upd_populate_artwork_markup_price(this.id,'cart_cart_items_attributes');"},
          collection: option_groups_from_collection_for_select(Artwork.by_name,
          :artwork_fixed_sizes, :name, :id, :name, art.object.fixed_size_id)
        art.input :material, allow_blank: false, include_blank: false,
          input_html: {style: "width:75% !important", class: "chosen-input",
          onchange: "upd_populate_artwork_fixed_prices_from_material(this.id, 'cart_cart_items_attributes');",
          onclick: "upd_populate_artwork_fixed_prices_from_material(this.id, 'cart_cart_items_attributes');"},
          collection: Material.all.map{|m| [m.name, m.id]}
        art.input :fixed_size_price, allow_blank: true,
          input_html: {style: "width:75% !important", class: "chosen-input"},
          collection: option_groups_from_collection_for_select(Material.all.
          joins(fixed_size_prices: :fixed_size).select("fixed_sizes.id as fixed_size_id,
          materials.id as material_id"), :fixed_size_prices_by_date, :name_label,
          :id, :price, art.object.fixed_size_price_id)
        art.input :frame, as: :boolean, value: false
        art.input :frame_type,
          input_html: {style: "width:75% !important", class: "chosen-input",
          onchange: "upd_populate_artwork_frame_prices_from_frame_type(this.id, 'cart_cart_items_attributes');",
          onclick: "upd_populate_artwork_frame_prices_from_frame_type(this.id, 'cart_cart_items_attributes');"},
          collection: FrameType.all.map{|m| [m.name, m.id]}
        art.input :fixed_size_frame_price, allow_blank: true,
          input_html: {style: "width:75% !important", class: "chosen-input"},
          collection: option_groups_from_collection_for_select(FrameType.all.joins(
          fixed_size_frame_prices: :fixed_size).select("fixed_sizes.id as fixed_size_id,
          frame_types.id as frame_type_id").order("frame_types.name, fixed_sizes.label asc,
          fixed_sizes.width"), :fixed_size_frame_prices_by_date, :name_label, :id,
          :price, art.object.fixed_size_frame_price_id)
        art.input :height, as: :number, label: "Height (leave blank if not scalable)"
        art.input :width, as: :number, label: "Width (leave blank if not scalable)"
        art.input :installation, as: :boolean, value: false
        art.input :installation_price, input_html: {style: "width:75% !important",
          class: "chosen-input"},
          collection: InstallationPrice.all.map{|u|[u.name, u.id]}, prompt: false,
          include_blank: false
      end
    end
    f.actions
  end
  permit_params :user_id, :user_shipping_detail_id, :status, :delivery_price_id,
    :created_by_admin, cart_items_attributes: [:id, :cart_id, :_destroy, :artwork_id,
    :fixed_size_id, :fixed_size_price_id, :frame, :fixed_size_frame_price_id,
    :height, :width, :quantity, :installation, :installation_price_id,
    :mark_up_price_id, :material_id, :frame_type_id]
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :user
        row :status
        row "Sub Total Amount" do
          resource.sub_total_amount("true")
        end
        row "Delivery Charge" do
          resource.delivery_charge_amount("true")
        end
        row "Promo Code" do
          h3 resource.promo_code.try(:code)
        end
        row "Discount Amount" do
          h3 resource.discount_amount
        end
        row "Total Amount" do
          h3 resource.total_price
        end
        row "Order" do
          (link_to resource.order.try(:name), admin_order_path(resource.order)) unless resource.order.blank?
        end
        row "Shipping Details" do
          (link_to resource.order.user_shipping_detail.try(:name_w_addr), admin_user_shipping_detail_path(resource.order.user_shipping_detail)) unless resource.order.try(:user_shipping_detail).blank?
        end
      end
    end
    panel "Items" do
      table_for resource.cart_items.each do
        column "ID" do |x|
          x.id
        end
        column "Artwork" do |x|
          x.artwork
        end
        column "Size" do |x|
          x.size
        end
        column "Qty" do |x|
          x.quantity
        end
        column "Base Price" do |x|
          "#{x.total_base_price('true')} #{'(' + x.base_price('true').to_s + ')' unless x.quantity == 1}"
        end
        column "MarkUp Price" do |x|
          "#{x.markup_percentage('true')}%"
        end
        column :material
        column "Product Price" do |x|
          "#{x.total_product_amount('true')} #{'(' + x.product_amount('true').to_s + ')' unless x.quantity == 1}"
        end
        column :frame_type
        column "Frame Price" do |x|
          "#{x.total_frame_amount('saved')} #{'(' + x.frame_amount('saved').to_s + ')' unless x.quantity == 1}"
        end
        column "Installation Price" do |x|
          "#{x.total_installation_amount('saved')} #{'(' + x.installation_amount('saved').to_s + ')' unless x.quantity == 1}"
        end
        column "Total Amount" do |x|
          x.total_amount('true')
        end
      end
    end
  end
  index do
    column :id
    column "User" do |cart|
      link_to User.find_by(id: cart.user_id).try(:name), admin_user_path(id: cart.user_id)
    end
    column :status
    column :total_price
    column "Items count" do |x|
      x.items_count
    end
    column :created_at
    actions
  end
end