ActiveAdmin.register Order do
  menu label: "Orders", priority: 900
  actions :index, :show
  scope :all, if: proc{ current_admin_user.admin?}
  scope :order_submitted, default: true, if: proc{ current_admin_user.can_print?}
  scope :printing, if: proc{ current_admin_user.can_print?}
  scope :ready_to_deliver, if: proc{ current_admin_user.can_print?}
  scope :on_delivery, if: proc{ current_admin_user.can_print?}
  scope :completed, if: proc{ current_admin_user.can_print?}
  scope :assigned_ready_to_deliver, if: proc{ current_admin_user.can_install? and !current_admin_user.admin?}
  scope :assigned_on_delivery, if: proc{ current_admin_user.can_install? and !current_admin_user.admin?}
  scope :assigned_completed, if: proc{ current_admin_user.can_install? and !current_admin_user.admin?}
  filter :id, label: "Order No"
  filter :user_email, as: :string
  filter :order_assignments_admin_user_email, as: :string, label: "Assign"
  filter :created_at
  filter :card_stripe_id
  filter :current_status
  index do
    selectable_column
    column "Order" do |o|
      "Order ##{o.id}"
    end
    column "User" do |o|
      link_to User.find_by(id: o.user_id).try(:full_name),
        admin_user_path(o.user_id)
    end
    column "Receiver" do |o|
      link_to UserShippingDetail.find_by(id: o.user_shipping_detail_id).try(:name),
        admin_user_shipping_detail_path(o.user_shipping_detail_id)
    end
    column :installation? if params[:scope] == "ready_to_deliver"
    column "Status" do |o|
      OrderHistory::STATUS[o.current_status]
    end
    column "Status Update" do |o|
      o.last_process.try(:formatted_processed_at)
    end
    if params[:scope] == "ready_to_deliver"
      column "Assign to" do |o|
        o.assignee.try(:name)
      end
    end
    column "Actions" do |o|
      case o.current_status
        when "order"
          order_item_status = "Start Printing"
          x_order_item_status = "Error"
        when "printing"
          order_item_status = "Finish Printing"
          x_order_item_status = "Move to Orders"
        when "ready_to_deliver"
          order_item_status = "Start Delivery"
          x_order_item_status = "Move to Printing "
        when "on_delivery"
          x_order_item_status = "Move to Ready to Deliver"
          order_item_status = "Completed"
        else
          x_order_item_status = "Move to On Delivery"
          order_item_status = nil
      end
      unless order_item_status.blank?
        if o.installation? and params[:scope] == "ready_to_deliver" and o.order_assignments.blank?
          (link_to "Assign to", new_admin_order_assignment_path(order_id: o.id), method: :get) + " " + (o.current_status == "order" ? " " : (link_to x_order_item_status, change_movement_admin_order_path(id: o.id, status: ("_" + o.current_status)), method: :post))
        else
          (link_to order_item_status, change_movement_admin_order_path(id: o.id, status: o.current_status), method: :post) + " " + (o.current_status == "order" ? " " : (link_to x_order_item_status, change_movement_admin_order_path(id: o.id, status: ("_" + o.current_status)), method: :post))
        end
      end
    end
    actions do |order|
      (link_to "Order Form", download_pdf_admin_order_path(order, format: "pdf"), method: :get) + "  " +
      (link_to "Download Artwork", download_image_admin_order_path(order, format: "zip"), method: :get)
    end
  end
  member_action :download_image, method: :get do
    @order = Order.find params[:id]
    Aws.config.update({
      region: ENV['aws_region'],
      access_key_id: ENV['aws_access_key_id'],
      secret_access_key: ENV['aws_secret_access_key']
    })
    s3 = Aws::S3::Resource.new
    bucket = s3.bucket(ENV['s3_bucket_name'])
    folder = "uploads/images"
    # Open the zip stream
    zip_stream = Zip::OutputStream.write_buffer do |zip|
      # Loop through the files we want to zip
      @order.cart.cart_items.each do |ci|
        # Get the file object
        artwork = ci.artwork.artwork_images.first
        file_obj = open("https:" + artwork.image.url.to_s)
        # Give a name to the file and start a new entry
        zip.put_next_entry("Order_No_#{@order.id}_#{artwork.id}.jpg")
        # Write the file data to zip
        zip.print file_obj.read
      end
    end

    # Create s3 bucket object
    zip_file = bucket.object("#{folder}/Order_No_#{@order.id}.zip")
    # Rewind the IO stream so we can read it
    zip_stream.rewind
    # Create a temp file for the zip
    tempZip = Tempfile.new(['Order_No_#{@order.id}','.zip'])
    # Write the stringIO data
    tempZip.binmode
    tempZip.write zip_stream.read
    # Send it to s3
    zip_file.upload_file(tempZip)
    # Clean up the tmp file
    tempZip.close
    tempZip.unlink

    s3_client = Aws::S3::Client.new
    # File.open('filename', 'wb') do |file|
    resp = s3_client.get_object(bucket: ENV['s3_bucket_name'], key:"#{folder}/Order_No_#{@order.id}.zip")
    send_data resp.body.read, filename: "Order_No_#{@order.id}.zip",
                            type: "application/zip",
                            disposition: "attachment"
  end
  member_action :download_pdf, method: :get do
    @order = Order.find params[:id]
      pdf = OrderPdf.new(@order, view_context)
      send_data pdf.render, filename: "Order_No_#{@order.id}.pdf",
                            type: "application/pdf",
                            disposition: "attachment"
  end
  member_action :change_movement, method: :post do
    order = Order.find_by id: params[:id]
    order_item = OrderHistory.new(order_id: params[:id], processed_at: Time.now)
    case params[:status]
      when "order"
        order_item.status = "printing"
        scope = "order_submitted"
      when "printing"
        order_item.status = "ready_to_deliver"
        scope = "printing"
      when "_printing"
        order_item.status = "order"
        scope = "printing"
      when "ready_to_deliver"
        order_item.status = "on_delivery"
        scope = "ready_to_deliver"
        scope = "assigned_ready_to_deliver" if order.installation?
      when "_ready_to_deliver"
        order_item.status = "printing"
        scope = "ready_to_deliver"
      when "on_delivery"
        order_item.status = "completed"
        scope = "on_delivery"
        scope = "assigned_on_delivery" if order.installation?
      when "_on_delivery"
        order_item.status = "ready_to_deliver"
        scope = "on_delivery"
      when "_completed"
        order_item.status = "on_delivery"
        scope = "completed"
      else
        order_item.status = nil
    end
    if order_item.save
      if params[:status] == "_ready_to_deliver"
        order_assigns = OrderAssignment.where(order_id: order.id)
        order_assigns.delete_all
      end
      redirect_to({action: :index, scope: scope}, notice: "You successfully updated the order.")
    else
      redirect_to({action: :index}, alert: order_item.errors.full_messages.first )
    end
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :user
        row :cart
        row "Status" do |x|
          OrderHistory::STATUS[x.current_status]
        end
        row "Shipping Details" do
          (link_to resource.user_shipping_detail.try(:name_w_addr), admin_user_shipping_detail_path(resource.user_shipping_detail)) unless resource.user_shipping_detail.blank?
        end
      end
    end
    panel "Courier" do
      table_for resource.order_couriers do
        column :name
        column :tracking_no
      end
    end
    panel "History" do
      table_for resource.order_histories.order("processed_at") do
        column "Status" do |x|
          OrderHistory::STATUS[x.status]
        end
        column :processed_at
      end
    end
  end
  controller do
    def scoped_collection
      if current_admin_user.admin?
        Order.all
      else
        if current_admin_user.can_install?
          Order.all.joins(:order_assignments).where("order_assignments.admin_user_id = ?", current_admin_user.id)
        else
          Order.all.joins("LEFT OUTER JOIN order_assignments ON orders.id = order_assignments.order_id").where("order_assignments.admin_user_id IS NULL")
        end
      end
    end
  end
end

