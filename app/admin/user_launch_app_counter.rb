# ActiveAdmin.register UserLaunchAppCounter do
#   menu if: proc{ current_admin_user.admin? }
#   menu parent: "Users", label: "Launch App", priority: 302
#   actions :all, except: :show
#   filter :name
#   index do
#     selectable_column
#     column :user
#     column :counter
#     actions
#   end
#   permit_params :id, :user_id, :artwork_id, :counter
#   form do |f|
#     f.semantic_errors *f.object.errors.keys
#     f.inputs do
#       f.input :user, input_html: { class: "chosen-input", style: "width:75% !important" }, collection: User.by_name.map{|u|[u.name, u.id]}, include_blank: false
#       f.input :counter
#     end
#     f.actions
#   end
#   controller do
#     def create
#       super do |success,failure|
#         success.html { redirect_to admin_user_launch_app_counters_path }
#       end
#     end
#     def update
#       super do |success,failure|
#         success.html { redirect_to admin_user_launch_app_counters_path }
#       end
#     end
#   end
# end