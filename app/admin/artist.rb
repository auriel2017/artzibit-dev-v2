# ActiveAdmin.register Artist do
#   actions :all
#   filter :name
#   index do
#     selectable_column
#     column :id
#     column :name
#     actions
#   end
#   permit_params :id, :bio, :name
#   form do |f|
#     f.semantic_errors *f.object.errors.keys
#     f.inputs do
#       f.input :name
#     end
#     f.actions
#   end
#   show do
#     panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
#       attributes_table_for resource do
#         row :name
#       end
#     end
#   end
# end