# ActiveAdmin.register ArtworkSize do
#   menu parent: 'Artworks', label: "Size"
#   form do |f|
#     @artwork = controller.instance_variable_get(:@artwork)
#     @artwork = Artwork.all if @artwork.blank?
#     @artwork_sizes = controller.instance_variable_get(:@artwork_sizes)
#     f.semantic_errors *f.object.errors.keys
#     f.inputs do
#       f.input :artwork, input_html: { class: "chosen-input", style: "width:75% !important", readonly: true }, collection: @artwork.map{|u|[u.name, u.id]}, include_blank: false
#       f.input :featured, as: :boolean, label: "Featured #{'(Only Presets can be featured)'}"
#       f.input :height, label: "#{'Min' if @artwork.first.try(:scalable?)} Height"
#       f.input :width, label: "#{'Min' if @artwork.first.try(:scalable?)} Width"
#       f.input :available_until, as: :date_picker, input_html:
#         {style: "width:75% !important"}, label: "Available Until (can be blank)"
#     end
#     f.actions
#     unless @artwork_sizes.blank?
#       panel "Size History" do
#         table_for @artwork_sizes do
#           column :height
#           column :width
#           column :available_until
#         end
#       end
#     end
#   end
#   permit_params :id, :artwork_id, :height, :width, :available_until, :featured
#   controller do
#     def new
#       @artwork = Artwork.where id: params[:artwork_id]
#       unless @artwork.blank?
#         @artwork_sizes = @artwork.first.artwork_sizes
#       end
#       super
#     end
#     def create
#       super do |success,failure|
#         success.html { redirect_to admin_artwork_path(resource.artwork) }
#       end
#     end
#     def update
#       super do |success,failure|
#         success.html { redirect_to admin_artwork_path(resource.artwork) }
#       end
#     end
#   end
# end