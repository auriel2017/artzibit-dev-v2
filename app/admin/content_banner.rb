ActiveAdmin.register ContentBanner do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Masterfiles"

  actions :all
  scope :active, default: true
  scope :inactive
  scope :all

  permit_params :id, :sequence, :active, :name, :banner_image,
    content_banner_dtls_attributes: [:id, :content_banner_id, :_destroy,
    :contentable_type, :contentable_name]

  index do
    selectable_column
    column :id
    column :active
    column :sequence
    column :name
    column :redirect_tos
    column "Banner Image" do |medium|
      image_tag(url_for(medium.banner_image.url), style: "height:50%") \
        unless medium.banner_image.blank?
    end
    column :created_at
    actions
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :active
        row :sequence
        row :name
        row :redirect_tos
        row "Banner Image" do
          image_tag url_for(resource.banner_image.url) unless resource.banner_image.blank?
        end
        row :created_at
        row :updated_at
      end
    end
  end
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :sequence
      f.input :active
      f.input :name
      if f.object.new_record?
        f.input :banner_image, as: :file
      else
        f.input :banner_image, as: :file, hint: (f.object.banner_image.blank? ?
          "No Image" : image_tag(f.object.banner_image.url) )
      end
      f.input("models", label: "", input_html: {style: "display:none;"},
        collection: option_groups_from_collection_for_select([Artwork,
          Category, Tag, User], :by_name, :name, :content_name, :name))
    end
    f.inputs "Condition" do
      f.object.content_banner_dtls.build if f.object.content_banner_dtls.blank?
      f.has_many :content_banner_dtls, heading: "Redirect To",
        allow_destroy: true, new_record: true do |dtl|
        dtl.input :contentable_type, allow_blank: true, include_blank: true,
          input_html: {style: "width:75% !important", class: "chosen-input",
          onchange: "populate_dynamic_select(this.id);"},
          collection: ["Artwork", "Category", "Tag", "User"], label: "Type"
        if dtl.object.contentable_id.blank?
          dtl.input :contentable_name, input_html: {style: "width:75% !important",
            class: "chosen-input"},
            collection: option_groups_from_collection_for_select([Artwork,
            Category, Tag, User], :by_name, :name, :content_name, :name,
            dtl.object.contentable_name), label: "Id"
        else
          dtl.input :contentable_name, input_html: {style: "width:75% !important",
            class: "chosen-input"},
            collection: option_groups_from_collection_for_select([dtl.object.contentable_type.constantize],
            :by_name, :name, :content_name, :name, dtl.object.contentable_name),
            label: "Id"
        end
      end
    end
    f.actions
  end
end