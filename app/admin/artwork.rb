ActiveAdmin.register Artwork do
  menu if: proc{ current_admin_user.admin? }
  menu label: "Artworks", priority: 600
  scope :actives, default: true
  scope :deleted
  scope :top_picks
  actions :all
  filter :name
  filter :user_name, as: :string, label: "Artist"
  index do
    selectable_column
    column :top_pick
    column :id
    column :orientation
    column "Images" do |art|
      ArtworkImage.where(artwork_id: art.id).map{|a| image_tag(url_for(a.image.
        url(:medium)), style: "height:50%")}.join("&nbsp;&nbsp;").html_safe unless \
        ArtworkImage.where(artwork_id: art.id).blank?
    end
    column :name
    column "Artist" do |art|
      User.find_by(id: art.user_id).try(:name)
    end
    column "Style" do |art|
      ArtworkStyle.find_by(id: art.artwork_style_id).try(:name)
    end
    column "Status" do |art|
      art.status
    end
    column "Privacy" do |art|
      if art.privacy == "public"
        "Open Marketplace"
      else
        art.privacy
      end
    end
    column "Current Mark Up" do |art|
      "#{art.current_markup.try(:value)}"
    end
    column "Description" do |art|
      truncate art.description
    end
    column "Sales" do |art|
      art.total_sales.to_s.to_d.to_f.round(2)
    end
    column "Pieces Sold" do |art|
      art.total_pieces_sold
    end
    column "Favorites" do |art|
      art.total_favorites
    end
    column "Actions" do |row|
      (link_to "Change MarkUp Price", new_admin_mark_up_price_path(artwork_id: row),
        method: :get)
    end
    actions
  end
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs multipart: true do
      f.input :modified_admin_user_id, input_html: {value: current_admin_user.id},
        as: :hidden
      f.input :top_pick
      f.input :sequence
      f.input :active
      f.object.artwork_images.build if f.object.artwork_images.blank?
      f.fields_for :artwork_images, new_record: false, header: "" do |img|
        if f.object.new_record?
          img.input :image, as: :file
        end
      end
      f.input :name
      f.input :description, input_html: {rows: 3}
      f.input :user, input_html: {class: "chosen-input"}, label: "Artist",
        collection: User.by_name.map{|u|[u.name, u.id]}
      f.input :category, input_html: {class: "chosen-input"},
        collection: Category.all.by_name.map{|u|[u.name, u.id]}
      f.input :privacy, as: :radio,
        collection: [["Open Marketplace", "public"], ["Private", "private"]]
        f.input :tag_ids, input_html: {multiple: true,
        class: "can-create-chosen can-create-mul",
        style: "width:75% !important", include_blank: false },
        collection: f.object.tag_ids
      f.object.mark_up_prices.build if f.object.mark_up_prices.blank?
      f.fields_for :mark_up_prices, new_record: false, header: "" do |m|
        if f.object.new_record?
          li b "Mark-Up Price"
          m.input :type, as: :radio, collection: ["Percentage", "Price"]
          m.input :input_value
          m.input :effectivity_date, as: :datetime_picker, input_html:
          {style: "width:75% !important", value: Time.now.strftime("%Y-%m-%dT%H:%M:%S")},
          label: "Effectivity Date"
        end
      end
      f.actions
    end
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :top_pick
        row :sequence
        row :active
        row :name
        row :description
        row :status
        row :category
        row "User" do
          resource.user(:name)
        end
        row "Privacy" do
          if resource.privacy == "public"
            "Open Marketplace"
          else
            "Private"
          end
        end
        row "Tags" do
          b resource.tag_ids.join(", ")
        end
        row "Current Mark Up Price" do
          "#{resource.current_markup.try(:value)}"
        end
        row "Images" do
          resource.artwork_images.map{|a| image_tag url_for(a.image.url(:medium))}.
            join("&nbsp;&nbsp;").html_safe unless resource.artwork_images.blank?
        end
        row "Dimensions" do
          resource.artwork_images.first.image.aspect_ratio unless resource.artwork_images.blank?
        end
      end
    end
    panel "Price Mark Up" do
      table_for resource.mark_up_prices.order(:effectivity_date) do
        column :value
        column :effectivity_date
      end
    end
    panel "History" do
      table_for resource.artwork_histories.order(:id) do
        column :field_name
        column :old_value
        column :new_value
        column "Modified By" do |x|
          x.modified_by
        end
      end
    end
  end
  permit_params :id, :name, :description, :user_id, :category_id, :privacy,
    :modified_admin_user_id, :active, :top_pick, :sequence, tag_ids: [],
    artwork_images_attributes: [:id, :artwork_id, :_destroy, :image],
    mark_up_prices_attributes: [:id, :artwork_id, :type, :input_value,
    :effectivity_date]
  controller do
    before_filter :create_new_record, :only => [:create, :update]
    def create_new_record
      # tag_ids
      tag_ids = params[:artwork][:tag_ids]
      tag_ids.count.to_i.times do |t|
        next if tag_ids[t].blank?
        new_value = tag_ids[t].gsub("*", "").humanize
        tag_ids[t] = Tag.where(name: new_value).first_or_create(name: new_value).id
      end
      params[:artwork][:tag_ids] = tag_ids - [""]
    end
    def destroy
      if resource.can_be_deleted?
        begin
          resource.destroy
        rescue
          resource.active = false
          resource.save
        end
      else
        resource.active = false
        resource.save
      end
      redirect_to admin_user_artworks_path
    end
  end
end