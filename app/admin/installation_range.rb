ActiveAdmin.register InstallationRange do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Prices", priority: 403
  index do
    selectable_column
    column :id
    column "Size" do |x|
      x.name
    end
    column "Current Price" do |x|
      "$ #{x.current_price}"
    end
    column "Actions" do |row|
      (link_to "Change Price", new_admin_installation_price_path(installation_range_id: row),
        method: :get)
    end
    actions
  end
  action_item :new_installation_price_path, method: :get, only: :show do
    (link_to "Change Price", new_admin_installation_price_path(installation_range_id: resource),
        method: :get)
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row "Minimum Size" do
          "#{resource.min_size} cm"
        end
        row "Maximum Size" do
          "#{resource.max_size} cm"
        end
        row "Current Price" do
          "$ #{resource.current_price}"
        end
      end
    end
    panel "Price History" do
      table_for resource.installation_prices.order(:effectivity_date) do
        column :price
        column :effectivity_date
      end
    end
  end
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :min_size
      f.input :max_size
      if f.object.new_record?
        f.object.installation_prices.build if f.object.installation_prices.blank?
        f.fields_for :installation_prices, new_record: false, header: "" do |price|
          price.input :price
          price.input :effectivity_date, as: :datetime_picker, input_html:
          {style: "width:75% !important", value: Time.now.strftime("%Y-%m-%dT%H:%M:%S")}
        end
      end
    end
    f.actions
  end
  permit_params :id, :min_size, :max_size, installation_prices_attributes: [:id,
    :price, :effectivity_date, :delivery_range_id]
end