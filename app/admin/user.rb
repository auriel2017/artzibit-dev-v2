ActiveAdmin.register User do
  menu if: proc{ current_admin_user.admin? }
  scope :active, default: true
  scope :inactive
  actions :all
  filter :email
  filter :name
  filter :country
  index do
    selectable_column
    column :id
    column :email
    column :first_name
    column :last_name
    column :country
    column :contact_no
    column "Action" do |row|
      (link_to "Add Shipping Details", new_admin_user_shipping_detail_path(user_id: row), method: :get)
    end
    actions
  end
  csv do
    column :id
    column :active
    column :name
    column :email
    column :contact_no
    column :country
    column :username
    column :customer_stripe_id
    column :artist_type
    column :artwork_sequence_number
    column :by_pass_activation_code
    column :website_portfolio
    column :created_at
    column :updated_at
  end
  action_item :new_shipping_detail, method: :get, only: :show do
    (link_to "Add Shipping Details", new_admin_user_shipping_detail_path(user_id: resource), method: :get)
  end
  member_action :verify, method: [:post, :put] do
    user = User.find(params[:id])
    if request.post?
      user_verify = user.verify
    else
      user_verify = user.unverify
    end
    if user_verify.save
      redirect_to({action: :index}, notice: "You successfully updated the user.")
    else
      redirect_to({action: :index}, alert: user_unverify.errors.full_messages.first )
    end
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :active
        row :email
        row :full_name
        row :username
        row :country
        row :contact_no
        row :authentication_token
        row "Artist Type" do |user|
          "#{User::ARTIST_TYPE[user.artist_type]}"
        end
        row "Facebook" do |user|
          resource.user_authentication.present?
        end
        row "Notify" do |user|
          resource.notify
        end
        row "Stripe ID" do
          resource.stripe_details
        end
      end
    end
    panel "Shipping Details" do
      table_for resource.user_shipping_details.order(:updated_at) do
        column :is_default
        column :id
        column :country
        column "Name" do |u|
          "#{u.first_name} #{u.last_name}"
        end
        column :phone_number
        column "Detailed Address" do |u|
          link_to "#{u.detailed_addr}", admin_user_shipping_detail_path(u)
        end
      end
    end
  end
  permit_params :id, :email, :password, :password_confirmation, :active,
    :remember_me, :first_name, :last_name, :username, :country, :avatar, :notify,
    :artist_type, :by_pass_activation_code, :contact_no
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :active
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :first_name
      f.input :last_name
      f.input :username
      f.input :contact_no
      f.input :country, as: :string
      f.input :by_pass_activation_code, as: :boolean
      f.input :notify, as: :boolean
      f.input :artist_type, as: :radio, collection: [["Not an Artist",""],["Photographer","photographer"],
        ["Visual Artist", "visual_artist"]]
    end
    f.actions
  end
  controller do
    def update_resource(object, attributes)
      update_method = attributes.first[:password].present? ? :update_attributes : :update_without_password
      object.send(update_method, *attributes)
    end
  end
end