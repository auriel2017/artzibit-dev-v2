ActiveAdmin.register Support do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Users", priority: 406
  actions :all
  index do
    selectable_column
    column :id
    column "User" do |r|
      link_to User.find_by(id: r.user_id).try(:full_name),
        admin_user_path(r.user_id)
    end
    column :contact_no
    column :content
    actions
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :user
        row :contact_no
        row :content
      end
    end
  end
  permit_params :id, :user_id, :contact_no, :content
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :user, input_html: {class: "chosen-input"},
        collection: User.by_name.map{|u|[u.name, u.id]}
      f.input :contact_no
      f.input :content
    end
    f.actions
  end
end