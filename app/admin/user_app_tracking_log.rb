ActiveAdmin.register UserAppTrackingLog do
  menu if: proc{ current_admin_user.admin? }
  menu parent: 'Statistics', label: "Logs"
  actions :all, except: [:show]
  scope :app, default: true
  scope :artwork
  index do
    selectable_column
    column :user
    if params[:scope] == "artwork"
      column :artwork
    end
    column :start_time
    column :end_time
    column "Duration (HH:MM:SS)" do |log|
      Time.at(log.duration).utc.strftime("%H:%M:%S")
    end
    actions
  end
  permit_params :id, :user_id, :artwork_id, :start_time, :end_time
  form do |f|
    f.semantic_errors *f.object.errors.keys
     f.object.start_time ||= DateTime.now
     f.object.end_time ||= DateTime.now
    f.inputs do
      f.input :user, input_html: { class: "chosen-input", style: "width:75% !important" }, collection: User.by_name.map{|u|[u.name, u.id]}, include_blank: false
      f.input :artwork, input_html:   { class: "chosen-input", style: "width:75% !important" }, collection: Artwork.by_name.map{|u|[u.name, u.id]}, include_blank: true, allow_blank: true
      f.input :start_time
      f.input :end_time
    end
    f.actions
  end
  controller do
    def create
      super do |success,failure|
        success.html { redirect_to admin_user_app_tracking_logs_path }
      end
    end
    def update
      super do |success,failure|
        success.html { redirect_to admin_user_app_tracking_logs_path }
      end
    end
  end
end