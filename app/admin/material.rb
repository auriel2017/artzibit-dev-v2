ActiveAdmin.register Material do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Prices", priority: 406
  actions :all
  filter :name
  index do
    selectable_column
    column :id
    column :name
    column "Actions" do |row|
      (link_to "Change Price", new_admin_fixed_size_price_path(material_id: row),
        method: :get)
    end
    actions
  end
  action_item :new_price_path, method: :get, only: :show do
    (link_to "Change Price", new_admin_fixed_size_price_path(material_id: resource),
        method: :get)
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :name
      end
    end
    panel "Current Price" do
      table_for FixedSize.all.by_name do
        column :name
        column "Price" do |r|
          "$ #{r.current_material_price_amount(resource.id)}"
        end
        column "Effectivity Date" do |r|
          r.current_material_price(resource.id).try(:effectivity_date)
        end
      end
    end
  end
  permit_params :id, :description, :name, fixed_size_prices_attributes: [:id,
    :price, :fixed_size_id, :effectivity_date, :material_id]
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      @fixed_sizes = controller.instance_variable_get(:@fixed_sizes)
      f.input :name
      if f.object.fixed_size_prices.blank?
        f.object.fixed_size_prices.build
        li hr
        li b "Price per Size"
        @fixed_sizes.each do |fs|
          f.fields_for :fixed_size_prices, new_record: false, header: "" do |price|
            price.input :price, label: "#{fs.name}"
            price.input :material_id, input_html: {value: f.object.id}, as: :hidden
            price.input :fixed_size_id, input_html: {value: fs.id}, as: :hidden
            price.input :effectivity_date, input_html: {style: "width:75% !important",
              value: Time.now.strftime("%Y-%m-%dT%H:%M:%S")}, as: :hidden
          end
        end
      end
    end
    f.actions
  end
  controller do
    before_filter :fixed_sizes, only: [:new, :create, :update]
    def fixed_sizes
      @fixed_sizes = FixedSize.all.sort_by{|x| [x.size_type, (x.width + x.height)]}
    end
  end
end