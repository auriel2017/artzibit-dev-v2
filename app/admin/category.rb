ActiveAdmin.register Category do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Masterfiles", priority: 402
  actions :all
  filter :name
  index do
    selectable_column
    column :id
    column "Image" do |category|
      unless category.cover_image.blank?
        image_tag url_for(category.cover_image.url)
      else
        "No Image"
      end
    end
    column :name
    column :description
    column :sequence
    actions
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :name
        row :description
        row :sequence
        row "Image" do
          unless resource.cover_image.blank?
            image_tag url_for(resource.cover_image.url)
          else
            "No Image"
          end
        end
      end
    end
  end
  permit_params :id, :description, :name, :cover_image, :remove_image, :sequence
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :name
      f.input :description, as: :text
      if f.object.new_record?
        f.input :cover_image, as: :file
      else
        f.input :cover_image, as: :file,
          hint: (f.object.cover_image.blank? ? "No Image" :
          image_tag(f.object.cover_image.url) )
        f.input :remove_image, as: :boolean, required: :false, label: 'Remove image' unless f.object.cover_image.blank?
      end
      f.input :sequence
    end
    f.actions
  end
end