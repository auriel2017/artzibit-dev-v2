# ActiveAdmin.register UserView do
#   menu if: proc{ current_admin_user.admin? }
#   menu parent: 'Users', label: "Views", priority: 304
#   actions :all, except: :show
#   filter :name
#   index do
#     selectable_column
#     column :user
#     column :artwork
#     column :counter
#     actions
#   end
#   permit_params :id, :user_id, :artwork_id, :counter
#   form do |f|
#     f.semantic_errors *f.object.errors.keys
#     f.inputs do
#       f.input :user, input_html: { class: "chosen-input", style: "width:75% !important" }, collection: User.all.map{|u|[u.name, u.id]}, include_blank: false
#       f.input :artwork, input_html:   { class: "chosen-input", style: "width:75% !important" }, collection: Artwork.all.map{|u|[u.name, u.id]}, include_blank: false
#       f.input :counter
#     end
#     f.actions
#   end
#   controller do
#     def create
#       super do |success,failure|
#         success.html { redirect_to admin_user_views_path }
#       end
#     end
#     def update
#       super do |success,failure|
#         success.html { redirect_to admin_user_views_path }
#       end
#     end
#   end
# end