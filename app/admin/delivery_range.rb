ActiveAdmin.register DeliveryRange do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Prices", priority: 401
  index do
    selectable_column
    column :id
    column :with_installation
    column :size
    column "Current Price" do |x|
      "$ #{x.current_price}"
    end
    column "Actions" do |row|
      (link_to "Change Price", new_admin_delivery_price_path(delivery_range_id: row),
        method: :get)
    end
    actions
  end
  action_item :change_price, method: :get, only: :show do
    (link_to "Change Price", new_admin_delivery_price_path(delivery_range_id: resource),
        method: :get)
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :with_installation
        row "Minimum Size" do
          resource.min_size
        end
        row "Maximum Size" do
          resource.max_size
        end
      end
    end
    panel "Price History" do
      table_for resource.delivery_prices.order(:effectivity_date) do
        column :price
        column :effectivity_date
      end
    end
  end
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :min_size
      f.input :max_size
      f.input :with_installation, as: :boolean
      if f.object.new_record?
        f.object.delivery_prices.build if f.object.delivery_prices.blank?
        f.fields_for :delivery_prices, new_record: false, header: "" do |price|
          price.input :price
          price.input :effectivity_date, as: :datetime_picker, input_html:
          {style: "width:75% !important", value: Time.now.strftime("%Y-%m-%dT%H:%M:%S")}
        end
      end
    end
    f.actions
  end
  permit_params :id, :min_size, :max_size, :with_installation,
    delivery_prices_attributes: [:id, :price, :effectivity_date,
    :delivery_range_id]
end