ActiveAdmin.register OrderAssignment do
  menu parent: "Orders", label: "Assignees", priority: 901
  actions :index, :new, :create, :destroy
  index do
    selectable_column
    column "Order" do |o|
      Order.where(id: o.order_id).pluck(:id).first
    end
    column "Admin User" do |o|
      AdminUser.where(id: o.admin_user_id).pluck(:name).first
    end
    column :created_at
    actions
  end
  form do |f|
    @orders = controller.instance_variable_get(:@orders)
    @orders ||= Order.all
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :order, collection: options_from_collection_for_select(@orders,
        :id, :name, f.object.order_id), include_blank: false,
        input_html: {class: "chosen-input", style: "width: 75%"}
      f.input :admin_user, collection: AdminUser.all{|u| [u.id, u.name]},
        input_html: {class: "chosen-input", style: "width: 75%"}
    end
    f.actions
  end
  permit_params :id, :order_id, :admin_user_id
  controller do
    def new
      unless params[:order_id].blank?
        @orders = Order.where(id: params[:order_id])
      end
      super
    end
    def create
      super do |success,failure|
        success.html { redirect_to admin_orders_path(scope: "ready_to_deliver") }
      end
    end
    def update
      super do |success,failure|
        success.html { redirect_to admin_orders_path(scope: "ready_to_deliver") }
      end
    end
  end
end