ActiveAdmin.register PromoCode do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Masterfiles", priority: 404
  actions :all
  index do
    selectable_column
    column :id
    column :code
    column :limit
    column "Duration" do |promo|
      if promo.end_date.blank?
        "#{promo.start_date.strftime("%b %d, %Y %H:%M:%S")}"
      else
        "#{promo.start_date.strftime("%b %d, %Y %H:%M:%S")} - #{promo.end_date.strftime("%b %d, %Y %H:%M:%S")}"
      end
    end
    column "Off" do |promo|
      if promo.percentage_off.present?
        "#{promo.percentage_off}%"
      else
        "$#{promo.price_off.to_d}%"
      end
    end
    actions
  end

  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :code
        row :limit
        row "Duration" do
          if resource.end_date.blank?
            "#{resource.start_date.strftime("%b %d, %Y %H:%M:%S")}"
          else
            "#{resource.start_date.strftime("%b %d, %Y %H:%M:%S")} - #{resource.end_date.strftime("%b %d, %Y %H:%M:%S")}"
          end
        end
        row "Off" do
          if resource.percentage_off.present?
            "#{resource.percentage_off}%"
          else
            "$#{resource.price_off.to_d}%"
          end
        end
      end
    end
    panel "Conditions" do
      table_for resource.promo_code_dtls.order(:id) do
        column "Type" do |x|
          case x.promo_codable_type
            when "Artwork"
              "Artwork"
            when "ArtworkStyle"
              "Style"
            when "User"
              "Artist"
          end
        end
        column :promo_codable
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :code
      f.input :limit
      f.input :start_date, input_html: {value: DateTime.now}
      f.input :end_date
      f.input :percentage_off
      f.input :price_off
      f.has_many :promo_code_dtls, heading: "Condition (Type: 'Artwork, xxx', 'ArtworkStyle, xxx')", allow_destroy: true, new_record: "Add Condition" do |dtl|
        dtl.input :promo_codable_identifier, collection: (ArtworkStyle.by_name + Artwork.by_name + User.by_name).map { |i| [ "#{i.class.to_s}, #{i.name}", "#{i.class.to_s}-#{i.id}"] }, input_html: { class: "chosen-input", style: "width:75% !important", onclick: "trigger_chose(this)"}, label: "Condition"
      end
    end
    f.actions
  end

  permit_params :id, :code, :start_date, :end_date, :limit, :percentage_off,
    :price_off, promo_code_dtls_attributes: [:id, :promo_codable_identifier, :_destroy]
end