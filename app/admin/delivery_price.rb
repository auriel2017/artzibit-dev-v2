ActiveAdmin.register DeliveryPrice do
  menu false
  actions :new, :create, :index
  form do |f|
    @del_ranges = controller.instance_variable_get(:@del_ranges)
    @del_ranges = DeliveryRange.all if @del_ranges.blank?
    f.inputs do
      f.input :delivery_range, input_html: { class: "chosen-input",
        style: "width:75% !important", readonly: true },
        collection: @del_ranges.map{|u|[u.name, u.id]}, include_blank: false
      f.input :price
      f.input :effectivity_date, as: :datetime_picker, input_html:
          {style: "width:75% !important", value: Time.now.strftime("%Y-%m-%dT%H:%M:%S")}
    end
    f.actions
  end
  permit_params :id, :delivery_range_id, :price, :effectivity_date
  controller do
    def new
      @del_ranges = DeliveryRange.where id: params[:delivery_range_id]
      super
    end
    def create
      super do |success,failure|
        success.html { redirect_to admin_delivery_range_path(resource.delivery_range) }
      end
    end
    def update
      super do |success,failure|
        success.html { redirect_to admin_delivery_range_path(resource.delivery_range) }
      end
    end
  end
end