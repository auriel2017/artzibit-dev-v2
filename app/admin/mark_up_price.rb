ActiveAdmin.register MarkUpPrice do
  menu parent: "Prices", priority: 404
  form do |f|
    f.inputs do
      @artwork = controller.instance_variable_get(:@artwork)
      @artwork = Artwork.all.order(:name) if @artwork.blank?
      f.input :artwork, input_html: { class: "chosen-input", style: "width:75% !important"}, collection: @artwork.map{|u|[u.name, u.id]}, include_blank: false
      f.input :type, as: :radio, collection: ["Percentage", "Price"]
      f.input :input_value
      f.input :effectivity_date, as: :datetime_picker, input_html:
          {style: "width:75% !important", value: Time.now.strftime("%Y-%m-%dT%H:%M:%S")}
    end
    f.actions
  end
  permit_params :id, :artwork_id, :type, :input_value, :effectivity_date
  controller do
    def new
      @artwork = Artwork.where id: params[:artwork_id]
      super
    end
    def create
      super do |success,failure|
        success.html { redirect_to admin_artwork_path(resource.artwork) }
      end
    end
    def update
      super do |success,failure|
        success.html { redirect_to admin_artwork_path(resource.artwork) }
      end
    end
  end
end