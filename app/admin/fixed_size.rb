ActiveAdmin.register FixedSize do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Prices", priority: 402
  actions :all
  index do
    selectable_column
    column :id
    column "Type" do |x|
      FixedSize::SIZE_TYPES[x.size_type]
    end
    column "Label" do |x|
      FixedSize::SIZE_LABELS[x.label]
    end
    column :height
    column :width
    column "Actions" do |row|
      (link_to "Change Price", new_admin_fixed_size_price_path(fixed_size_id: row),
        method: :get) + " " +
      (link_to "Change Frame Price", new_admin_fixed_size_frame_price_path(fixed_size_id: row),
        method: :get)
    end
    actions
  end
  action_item :new_price_path, method: :get, only: :show do
    (link_to "Change Price", new_admin_fixed_size_price_path(fixed_size_id: resource.id),
        method: :get) + " " +
    (link_to "Change Frame Price", new_admin_fixed_size_frame_price_path(fixed_size_id: resource.id),
        method: :get)
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row "Type" do
          FixedSize::SIZE_TYPES[resource.size_type]
        end
        row "Label" do
          FixedSize::SIZE_LABELS[resource.label]
        end
        row "Height" do
          "#{resource.height} cm"
        end
        row "Width" do
          "#{resource.width} cm"
        end
      end
    end
    panel "Price History" do
      Material.all.order(:name).each do |m|
        b m.name
        table_for resource.fixed_size_prices.where(material_id: m.id).
          order(:effectivity_date) do
          column :price
          column :effectivity_date
        end
      end
    end
    panel "Frame Price History" do
      FrameType.all.order(:name).each do |ft|
        b ft.name
        table_for resource.fixed_size_frame_prices.where(frame_type: ft.id).
          order(:effectivity_date) do
          column :price
          column :effectivity_date
        end
      end
    end
  end
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :size_type, as: :radio, collection: FixedSize::SIZE_TYPES.map{|key,value| [value,key]}
      f.input :label, as: :radio, collection: FixedSize::SIZE_LABELS.map{|key,value| [value,key]}
      f.input :height
      f.input :width
      if f.object.new_record?
        li hr
        li b "Price per Material"
        f.object.fixed_size_prices.build if f.object.fixed_size_prices.blank?
        @materials = Material.all.order(:name)
        @materials.each do |m|
          f.fields_for :fixed_size_prices, new_record: false, header: "" do |price|
            price.input :price, label: "#{m.name}"
            price.input :material_id, input_html: {value: m.id}, as: :hidden
            price.input :effectivity_date, input_html: {style: "width:75% !important", value: Time.now.strftime("%Y-%m-%dT%H:%M:%S")},as: :hidden
          end
        end
        li hr
        li b "Price per Frame Type"
        f.object.fixed_size_frame_prices.build if f.object.fixed_size_frame_prices.blank?
        @frame_types = FrameType.all.order(:name)
        @frame_types.each do |ft|
          f.fields_for :fixed_size_frame_prices, new_record: false, header: "" do |price|
            price.input :price, label: "#{ft.name}"
            price.input :frame_type_id, input_html: {value: ft.id}, as: :hidden
            price.input :effectivity_date, input_html: {style: "width:75% !important",
              value: Time.now.strftime("%Y-%m-%dT%H:%M:%S")}, as: :hidden
          end
        end
      end
    end
    f.actions
  end
  permit_params :id, :label, :size_type, :height, :width,
    fixed_size_prices_attributes: [:id, :price, :effectivity_date, :material_id],
    fixed_size_frame_prices_attributes: [:id, :price, :effectivity_date,
      :frame_type_id]
end