ActiveAdmin.register ArtworkApprovalDtl do
  menu label: "Artwork Approval", priority: 700
  actions :index, :show
  scope :pendings, default: true
  scope :approves
  scope :rejects

  index do
    selectable_column
    column :id
    column "Admin User" do |a|
      AdminUser.find_by(id: a.admin_user_id).try(:name)
    end
    column "Artwork" do |a|
      Artwork.find_by(id: (ArtworkApprovalHdr.find_by(id: a.artwork_approval_hdr_id).artwork_id)).try(:name)
    end
    column :status
    column :processed_at if params[:scope] != "pendings"
    column :created_at
    actions
  end
  show do
    artwork = resource.artwork_approval_hdr.artwork
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for artwork do
        row :name
        row "Images" do
          artwork.artwork_images.map{|a| image_tag url_for(a.image.url(:medium))}.
            join("&nbsp;&nbsp;").html_safe unless artwork.artwork_images.blank?
        end
        row "User" do
          artwork.user(:name)
        end
        row "Description" do
          artwork.description
        end
      end
    end
  end
  action_item :update_status, method: :get, only: :show do
    (link_to "Approve", update_status_admin_artwork_approval_dtl_path(resource,
      status: "approve"), method: :put)  + " " +
    (link_to "Reject", update_status_admin_artwork_approval_dtl_path(resource,
      status: "reject"), method: :put) if resource.status == "pending"
  end
  member_action :update_status, method: [:put] do
    approval_dtl = ArtworkApprovalDtl.find params[:id]
    approval_dtl.status = params[:status]
    approval_dtl.processed_at = Time.now
    if approval_dtl.save
      redirect_to({action: :index}, notice: "You successfully #{params[:status]} the user.")
    else
      redirect_to({action: :index}, alert: approval_dtl.errors.full_messages.first )
    end
  end
  controller do
    def scoped_collection
      if current_admin_user.admin?
        super
      else
        super.where(admin_user_id: current_admin_user.id)
      end
    end
  end
end