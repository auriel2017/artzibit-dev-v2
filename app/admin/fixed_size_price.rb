ActiveAdmin.register FixedSizePrice do
  menu false
  form do |f|
    @fixed_sizes = controller.instance_variable_get(:@fixed_sizes)
    @materials = controller.instance_variable_get(:@materials)
    @fixed_sizes = FixedSize.all.sort_by{|x| [x.size_type, (x.width + x.height)]} if @fixed_sizes.blank?
    @materials = Material.all.order(:name) if @materials.blank?
    f.inputs do
      f.input :fixed_size, input_html: { class: "chosen-input",
        style: "width:75% !important", readonly: true },
        collection: @fixed_sizes.map{|u|[u.name, u.id]}, include_blank: false
      f.input :material, input_html: { class: "chosen-input",
        style: "width:75% !important", readonly: true },
        collection: @materials.map{|u|[u.name, u.id]}, include_blank: false
      f.input :price
      f.input :effectivity_date, as: :datetime_picker, input_html:
        {style: "width:75% !important", value: Time.now.strftime("%Y-%m-%dT%H:%M:%S")}
    end
    f.actions
  end
  permit_params :id, :fixed_size_id, :material_id, :price, :effectivity_date
  controller do
    def new
      @fixed_sizes = FixedSize.where id: params[:fixed_size_id]
      @materials = Material.where id: params[:material_id]
      super
    end
    def create
      super do |success,failure|
        success.html { redirect_to admin_material_path(resource.material) }
      end
    end
    def update
      super do |success,failure|
        success.html { redirect_to admin_material_path(resource.material) }
      end
    end
  end
end