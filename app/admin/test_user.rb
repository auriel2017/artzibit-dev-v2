ActiveAdmin.register TestUser do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Accounts", label: "Test Users", priority: 202
  index do
    selectable_column
    column :id
    column :email
    actions
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :email
      end
    end
  end
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :email
    end
    f.actions
  end
  permit_params :id, :email
end