ActiveAdmin.register User, :as => 'Users - Statistics' do
    menu if: proc{ current_admin_user.admin? }
    menu label: "Users", parent: "Statistics"
    actions :index
    filter :name

    index do
      column :id
      column :username
      column :country
      column "Times viewed an artwork" do |user|
        user.artwork_view_counter
      end
      column "Times open the app" do |user|
        user.app_launch_counter
      end
      column "Hours stays in the artwork" do |user|
        user.artwork_view_duration
      end
      column "Hours stays in the app" do |user|
        user.app_stay_duration
      end
    end
end