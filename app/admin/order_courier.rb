ActiveAdmin.register OrderCourier do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Orders", label: "Courier", priority: 902
  actions :index, :new, :create, :destroy
  form do |f|
    @orders = controller.instance_variable_get(:@orders)
    @orders ||= Order.all
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :order, collection: options_from_collection_for_select(@orders,
        :id, :name, f.object.order_id), include_blank: false,
        input_html: {class: "chosen-input", style: "width: 75%"}
      f.input :name
      f.input :tracking_no
    end
    f.actions
  end
  permit_params :id, :order_id, :name, :tracking_no
  controller do
    def new
      unless params[:order_id].blank?
        @orders = Order.where(id: params[:order_id])
      end
      super
    end
    def create
      super do |success,failure|
        success.html { redirect_to admin_orders_path(scope: "ready_to_deliver") }
      end
    end
    def update
      super do |success,failure|
        success.html { redirect_to admin_orders_path(scope: "ready_to_deliver") }
      end
    end
  end
end