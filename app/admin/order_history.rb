ActiveAdmin.register OrderHistory do
  menu parent: "Orders", label: "History", priority: 903
  actions :index, :new, :create, :destroy
  index do
    selectable_column
    column :id
    column "Order" do |o|
      "Order ##{o.id}"
    end
    column :status
    column :processed_at
    actions
  end
  form do |f|
    @orders = controller.instance_variable_get(:@orders)
    @orders ||= Order.all
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :order, collection: options_from_collection_for_select(@orders,
        :id, :name, f.object.order_id), include_blank: false,
        input_html: {class: "chosen-input", style: "width: 75%"}
      f.input :status, collection: OrderHistory::STATUS.map{|key,value| [value,key]}
      f.input :processed_date_only, as: :date_picker, input_html: {style:
        "width: 75%", value: Date.today}, label: "Processed Date"
      f.input :processed_time, as: :time_picker
    end
    f.actions
  end
  permit_params :id, :order_id, :status, :processed_at
  controller do
    def new
      unless params[:order_id].blank?
        @orders = Order.where(id: params[:order_id])
      end
      super
    end
    def create
      pdate = params[:order_history][:processed_date_only].to_datetime.strftime("%Y-%m-%d").inspect
      ptime = params[:order_history][:processed_time]
      params[:order_history][:processed_at] = "#{pdate} #{ptime}"
      super do |success,failure|
        success.html { redirect_to admin_order_path(resource.order) }
      end
    end
  end
end