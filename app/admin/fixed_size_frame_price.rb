ActiveAdmin.register FixedSizeFramePrice do
  menu false
  form do |f|
    @fixed_sizes = controller.instance_variable_get(:@fixed_sizes)
    @frame_types = controller.instance_variable_get(:@frame_types)
    @fixed_sizes = FixedSize.by_name if @fixed_sizes.blank?
    @frame_types = FrameType.all if @frame_types.blank?
    f.inputs do
      f.input :fixed_size, input_html: { class: "chosen-input",
        style: "width:75% !important"},
        collection: @fixed_sizes.map{|u|[u.name, u.id]}, include_blank: false
      f.input :frame_type, input_html: { class: "chosen-input",
        style: "width:75% !important"},
        collection: @frame_types.map{|f| [f.name, f.id]},
        include_blank: false
      f.input :price
      f.input :effectivity_date, as: :datetime_picker, input_html:
        {style: "width:75% !important", value: Time.now.strftime("%Y-%m-%dT%H:%M:%S")}
    end
    f.actions
  end
  permit_params :id, :fixed_size_id, :frame_type_id, :price, :effectivity_date
  controller do
    def new
      @fixed_sizes = FixedSize.where id: params[:fixed_size_id]
      @frame_types = FrameType.where id: params[:frame_type_id]
      super
    end
    def create
      super do |success,failure|
        success.html { redirect_to admin_frame_type_path(resource.frame_type) }
      end
    end
    def update
      super do |success,failure|
        success.html { redirect_to admin_frame_type_path(resource.frame_type) }
      end
    end
  end
end