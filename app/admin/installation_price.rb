ActiveAdmin.register InstallationPrice do
  menu false
  # menu parent: "Installation Ranges", label: "Price"
  form do |f|
    @ins_ranges = controller.instance_variable_get(:@ins_ranges)
    @ins_ranges = InstallationRange.all if @ins_ranges.blank?
    f.inputs do
      f.input :installation_range, input_html: { class: "chosen-input",
        style: "width:75% !important", readonly: true },
        collection: @ins_ranges.map{|u|[u.name, u.id]}, include_blank: false
      f.input :price
      f.input :effectivity_date, as: :datetime_picker, input_html:
          {style: "width:75% !important", value: Time.now.strftime("%Y-%m-%dT%H:%M:%S")}
    end
    f.actions
  end
  permit_params :id, :installation_range_id, :price, :effectivity_date
  controller do
    def new
      @ins_ranges = InstallationRange.where id: params[:installation_range_id]
      super
    end
    def create
      super do |success,failure|
        success.html { redirect_to admin_installation_range_path(resource.installation_range) }
      end
    end
    def update
      super do |success,failure|
        success.html { redirect_to admin_installation_range_path(resource.installation_range) }
      end
    end
  end
end