ActiveAdmin.register Artwork, :as => 'Artworks - Statistics' do
  menu if: proc{ current_admin_user.admin? }
  menu label: "Artwork", parent: "Statistics"
  actions :index
  filter :name
  filter :user_name, as: :string, label: "Artist"
  filter :category_name, as: :string, label: "Category"
  filter :artwork_style_name, as: :string, label: "Style"
  index do
    column :id
    column :name
    column "Artist" do |art|
        art.user(:name)
    end
    column "Number of Views" do |art|
        art.views_count
    end
    column "Number of Favorites" do |art|
        art.favorites_count
    end
    column "Users spent to view this image" do |art|
        art.user_views_duration_count
    end
  end
end

# Track how many views each artwork got
# Track how many times each artwork has been favourited
# Track how long each user spent on an image

# Track how long users stay in the Artzibit app
# Track how many artworks did each user view
# Track how many times user opens the Artzibit app