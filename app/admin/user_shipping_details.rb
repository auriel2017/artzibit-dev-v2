ActiveAdmin.register UserShippingDetail do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Users", label: "Shipping Details", priority: 303
  scope :actives, default: true
  scope :deleted
  # actions :all, except: [:edit, :update]
  index do
    selectable_column
    column :id
    column :is_default
    column "User" do |r|
      link_to User.find_by(id: r.user_id).try(:full_name),
        admin_user_path(r.user_id)
    end
    column "Receiver" do |u|
      u.name
    end
    column :email
    column :phone_number
    column :country
    column "Detailed Address" do |u|
      truncate u.detailed_addr
    end
    column "" do |row|
      (link_to "Make Default", default_admin_user_shipping_detail_path(row),
        method: :put) unless row.is_default?
    end
    actions
  end
  member_action :default, method: :put do
    shipping_detail = UserShippingDetail.find(params[:id])
    shipping_detail.is_default = true
    if shipping_detail.save
      redirect_to({action: :index},
        notice: "You successfully updated the Shipping Details.")
    else
      redirect_to({action: :index},
        alert: shipping_detail.errors.full_messages.first )
    end
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row "User" do
          resource.user(:name)
        end
        row "Shipping Detail - Contact Person" do
          "#{resource.first_name} #{resource.last_name}"
        end
        row "Contact Number" do
          resource.phone_number
        end
        row :email
        row :detailed_addr
        row :country
        row :is_default
        row "Active" do
          resource.active.blank? ? true : resource.active
        end
      end
    end
  end
  form do |f|
    @users = controller.instance_variable_get(:@users)
    @users = User.all.by_name if @users.blank?
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :active
      f.input :user, input_html: { class: "chosen-input",
        style: "width:75% !important" }, collection: @users.all.map{|u|[u.name,
        u.id]}, include_blank: false
      f.input :is_default
      f.input :email
      f.input :first_name
      f.input :last_name
      f.input :phone_number
      f.input :detailed_addr
      f.input :country, as: :select, collection: ["Singapore"]
    end
    f.actions
  end
  permit_params :id, :user_id, :first_name, :last_name, :phone_number,
    :detailed_addr, :city, :country, :is_default, :active, :email
  controller do
    def new
      @users = User.where id: params[:user_id]
      super
    end
    def create
      super do |success,failure|
        success.html { redirect_to admin_user_path(resource.user) }
      end
    end
    def update
      super do |success,failure|
        success.html { redirect_to admin_user_path(resource.user) }
      end
    end
    def destroy
      if resource.can_be_deleted?
        begin
          resource.destroy
        rescue
          resource.active = false
          resource.is_default = false
          resource.save
        end
      else
        resource.active = false
        resource.is_default = false
        resource.save
      end
      redirect_to admin_user_shipping_details_path
    end
  end
end