ActiveAdmin.register UploadArtworkLimit do
  menu if: proc{current_admin_user.admin?}
  menu parent: "Masterfiles", label: "Upload Limits", priority: 406
  actions :all
  filter :name
  index do
    selectable_column
    column :id
    column :from_counter
    column :to_counter
    column :limit
    actions
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :from_counter
        row :to_counter
        row :limit
      end
    end
  end
  permit_params :id, :from_counter, :to_counter, :limit
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :from_counter
      f.input :to_counter
      f.input :limit
    end
    f.actions
  end
end