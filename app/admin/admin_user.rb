ActiveAdmin.register AdminUser do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Accounts", priority: 201
  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at
  index do
    selectable_column
    id_column
    column :name
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :admin
    column :printer
    column :installer
    column :vetter
    column :created_at
    actions
  end
  permit_params :id, :name, :email, :password, :password_confirmation,
    :remember_me, :admin, :printer, :installer, :vetter
  form do |f|
    f.inputs "Admin Details" do
      f.input :name
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :admin
      f.input :printer
      f.input :vetter
      f.input :installer
    end
    f.actions
  end
  controller do
    # def update
    #   if permitted_params[:password].blank? && permitted_params[:password_confirmation].blank?
    #     permitted_params.delete(:password)
    #     permitted_params.delete(:password_confirmation)
    #   end
    #   super
      # # get the currently logged in AdminUser's id
      # current_id = current_admin_user.id
      # # load the AdminUser being updated
      # @admin_user = AdminUser.find(params[:id])
      # # update the AdminUser with new attributes
      # # if successful, this will sign out the AdminUser being updated
      # if @admin_user.update_attributes(permitted_params[:admin_user])
      #   # if the updated AdminUser was the currently logged in AdminUser, sign them back in
      #   if @admin_user.id == current_id
      #     sign_in(AdminUser.find(current_id), :bypass => true)
      #   end
      #   flash[:notice] = I18n.t('devise.passwords.updated_not_active')
      #   redirect_to '/admin/admin_users'
      # # display errors
      # else
      #   render :action => :edit
      # end
    # end
    def update_resource object, attributes
      attributes.each do |attr|
        if attr[:password].blank? and attr[:password_confirmation].blank?
          attr.delete :password
          attr.delete :password_confirmation
        end
      end
      object.send :update_attributes, *attributes
    end
  end
end