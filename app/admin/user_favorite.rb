ActiveAdmin.register UserFavorite do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Users", label: "Favorites", priority: 301
  actions :all, except: :show
  # filter :users_name
  # filter :artworks_name
  index do
    selectable_column
    column "User" do |r|
      link_to User.find_by(id: r.user_id).try(:full_name),
        admin_user_path(r.user_id)
    end
    column "Artwork" do |r|
      link_to Artwork.where(id: r.artwork_id).pluck(:name).first,
        admin_artwork_path(r.artwork_id)
    end
    column :deleted
    actions
  end
  permit_params :id, :user_id, :artwork_id, :deleted
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :user, input_html: { class: "chosen-input", style: "width:75% !important" }, collection: User.by_name.map{|u|[u.name, u.id]}, include_blank: false
      f.input :artwork, input_html:   { class: "chosen-input", style: "width:75% !important" }, collection: Artwork.by_name.map{|u|[u.name, u.id]}, include_blank: false
      f.input :deleted
    end
    f.actions
  end
  controller do
    def create
      super do |success,failure|
        success.html { redirect_to admin_user_favorites_path }
      end
    end
    def update
      super do |success,failure|
        success.html { redirect_to admin_user_favorites_path }
      end
    end
  end
end