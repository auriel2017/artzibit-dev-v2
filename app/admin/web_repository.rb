ActiveAdmin.register WebRepository do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Masterfiles"
  actions :all
  index do
    selectable_column
    column :id
    column :name
    column "HTML Content" do |w|
      truncate w.content_html
    end
    column :page
    column :category
    actions
  end
  show do
    panel(I18n.t('active_admin.details',
      model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :name
        row :content_html
        row :page
        row :category
      end
    end
  end
  permit_params :id, :name, :content_html, :page, :category
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :name
      f.input :content_html
      f.input :page
      f.input :category
    end
    f.actions
  end
end