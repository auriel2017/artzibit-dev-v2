ActiveAdmin.register UserBankDetail do
  menu if: proc{ current_admin_user.admin? }
  menu priority: 11, parent: 'Users', label: "Bank Details"
  actions :index, :show
  index do
    selectable_column
    column :id
    column "User" do |r|
      link_to User.find_by(id: r.user_id).try(:full_name),
        admin_user_path(r.user_id)
    end
    actions
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :user
        row :created_at
        row :updated_at
      end
    end
    resource.cards.each do |c|
      panel("ID: #{c[:card_id]}") do
        attributes_table_for c do
          row :is_default
          row :brand
          row :last4
          row :exp_month
          row :exp_year
        end
      end
    end
  end
end