ActiveAdmin.register ActivationCode do
  menu if: proc{current_admin_user.admin?}
  menu parent: "Masterfiles", priority: 401
  actions :all, if: proc{ current_admin_user.admin? }
  index do
    selectable_column
    column :id
    column :code
    column :reusable
    column :limit
    column "Users used" do |a|
      a.reserved_and_used_count
    end
    actions
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :code
        row :reusable
        row :limit
      end
    end
  end
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :code
      f.input :reusable
      f.input :limit
    end
    f.actions
  end
  permit_params :id, :code, :reusable, :limit
end