ActiveAdmin.register NoOfApproval do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Masterfiles", priority: 403
  form do |f|
    f.inputs do
      f.input :count
      f.input :effectivity_date
    end
    f.actions
  end
  permit_params :id, :count, :effectivity_date
end