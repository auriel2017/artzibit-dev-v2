class TestUsersController < ApplicationController
  def create
    @test_user = TestUser.new test_user_param
    if @test_user.save
      flash[:notice] = "You are successfully added as a Test user"
      redirect_to root_path
    else
      flash[:error] = @test_user.errors.full_messages.first
      redirect_to root_path
    end
  end
  private
  def test_user_param
    params.required(:test_user).permit(:email)
  end
end