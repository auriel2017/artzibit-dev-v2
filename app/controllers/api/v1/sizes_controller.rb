class Api::V1::SizesController < UserModuleController
  respond_to :json
  def index
    @sizes = []
    Artwork::SIZE_FILTER.each do |keys,value|
      @sizes << ({
        key: keys,
        name: "#{keys.capitalize}: #{value[:max_h]}\" X #{value[:max_w]}\""
      })
    end
    render json: {status: true, data: @sizes}
  end
end