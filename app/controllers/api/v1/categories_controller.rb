class Api::V1::CategoriesController < UserModuleController
  respond_to :json
  def index
    @categories = Category.all.map{|c| c.v1_format }
    respond_with status: true, data: @categories
  end
end