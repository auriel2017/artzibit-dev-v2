class Api::V1::UserViewsController < UserModuleController
  def create
    error_message = nil
    user_view_params.values[0].each do |w|
      @user_view = UserView.where(artwork_id: w[:artwork_id], user_id: w[:user_id]).
        first_or_initialize
      @user_view.counter = @user_view.counter.to_i + w[:counter]
      if @user_view.save
        error_message = nil
      else
        break
        error_message = @user_view.errors.full_messages.first
      end
    end

    if error_message.blank?
      render json: {status: true, data: @user_view}
    else
      render json: {status: false, message: error_message}
    end
  end
  private
  def user_view_params
    params.required(:user_view).permit(values: [:artwork_id, :user_id, :counter])
  end
end