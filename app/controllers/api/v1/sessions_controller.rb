class Api::V1::SessionsController < Users::SessionsController
  respond_to :json
  def create
    user = User.new user_params
    if user.log_thru_facebook?
      if user.auth_exists?
        render json: {data: user.auth, status: true}
      else
        render json: {data: params[:user], status: false}
      end
    else
      override_create
    end
  end
  def destroy
    super
  end
  def override_create
    @user = User.authenticate user_params
    if @user.blank?
      render json: {status: false, message: "Invalid email address or password"}
    else
      render json: {status: true, data: @user}
    end
  end
  private
  def user_params
    params.required(:user).permit(:email, :password, user_authentication_attributes: [:provider, :uid])
  end
  def invalid_login_attempt
    warden.custom_failure!
    render :json=> {:success=>false, :message=>"Error with your login or password"}, :status=>401
  end
end

# class Api::V1::SessionsController < Users::SessionsController
#   prepend_before_filter :require_no_authentication, :only => [:create ]
#   include Devise::Controllers::Registrations

#   before_filter :ensure_params_exist

#   respond_to :json

#   def create
#     build_resource
#     resource = User.find_for_database_authentication(user_params)
#     return invalid_login_attempt unless resource

#     if resource.valid_password?(params[:user_login][:password])
#       sign_in("user", resource)
#       render :json=> {:success=>true, :auth_token=>resource.authentication_token, :login=>resource.login, :email=>resource.email}
#       return
#     end
#     invalid_login_attempt
#   end

#   def destroy
#     sign_out(resource_name)
#   end

#   protected
#   def ensure_params_exist
#     return unless user_params.blank?
#     render :json=>{:success=>false, :message=>"missing user_login parameter"}, :status=>422
#   end

#   def invalid_login_attempt
#     warden.custom_failure!
#     render :json=> {:success=>false, :message=>"Error with your login or password"}, :status=>401
#   end

#   private
#   def user_params
#     params.required(:user).permit(:email, :password, user_authentication_attributes: [:provider, :uid])
#   end
# end