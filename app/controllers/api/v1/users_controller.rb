
class Api::V1::UsersController < Users::RegistrationsController
  respond_to :json
  def update
    @user = User.find_by(id: params[:id])
    unless @user.blank?
      if @user.update_attributes(user_params)
        render json: {status: true, data: @user}
      else
        render json: {status: false, message: @user.errors.full_messages.first}
      end
    else
      render json: {status: false, message: "No User Found"}
    end
  end

  def current_user_details
    begin
      result = {status: false, data: {}}
      result[:data] = current_user
      result[:status] = true
    rescue => error
      result[:data] =  error.inspect
    end
    render json: result
  end

  private
  def user_params
    params.required(:user).permit(:id, :username, :name, :email, :first_name, :middle_name, :last_name, :contact_number, :country, :website_portfolio)
  end
end