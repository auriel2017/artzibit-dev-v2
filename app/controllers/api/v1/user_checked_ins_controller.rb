class Api::V1::UserCheckedInsController < UserModuleController
  def create
    @user_checked_in = UserCheckedIn.new user_checked_in_params
    @user_checked_in.user_id = current_user.id
    @user_checked_in.checked_in_at = Date.today
    if @user_checked_in.save
      render json: {status: true, data: @user_checked_in}
    else
      render json: {status: false, message: @user_checked_in.errors.
        full_messages.first}
    end
  end
  private
  def user_checked_in_params
    params.required(:user_checked_in).permit(:event_batch_id)
  end
end