class Api::V1::UserBankDetailsController < UserModuleController
  def index
    @bank_details = current_user.user_bank_details.map{|a| a.v1_format}[0]
    @bank_details = [] if @bank_details.blank?
    render json: {status: true, data: @bank_details}
  end
  def create
    customer_stripe_id = current_user.stripe_id
    @token_param = token_params
    # CREATE
    if (!@token_param[:number].blank? and @token_param[:card_id].blank?)
      if customer_stripe_id.blank?
        begin
          current_user.add_stripe_id token_params[:number]
        rescue => e
          render json: {status: false, message: e.message}
        else
          unless current_user.stripe_id.blank?
            @customer = Stripe::Customer.retrieve(current_user.stripe_id)
            render json: {status: true, data: @customer}
          else
            render json: {status: false, message: "Error adding card 1"}
          end
        end
      else
        @customer = Stripe::Customer.retrieve(current_user.stripe_id)
        begin
          @card_detail = @customer.sources.create({source: token_params[:number]})
        rescue => e
          # render json: {status: false, message: "Error adding card 2"}
          render json: {status: false, message: e.message}
        else
          if token_params[:is_default] == true
            @customer.default_source = @card_detail.id
            @customer.save
          end
          render json: {status: true, data: @card_detail}
        end
      end
    # UPDATE
    elsif (@token_param[:number].blank? and !@token_param[:card_id].blank?)
      unless @token_param[:card_id].blank?
        @customer = Stripe::Customer.retrieve(customer_stripe_id)
        @last_card_id = @customer.sources.map{|a| a.id}.last
        if (@token_param[:delete].blank? and @token_param[:is_default].blank?)
          render json: {status: false, message: "Wrong parameters received"}
        else
          if @token_param[:delete] == true #and @token_param[:is_default].blank?
            if (@customer.default_source == @token_param[:card_id])
              @customer.default_source = @last_card_id
            end
            begin
              @customer.save
            rescue
              render json: {status: false, message: "Error deleting card"}
            else
              begin
                @customer.sources.retrieve(@token_param[:card_id]).delete
              rescue
                render json: {status: false, message: "Error deleting card"}
              else
                render json: {status: true, message: "Successfully Deleted"}
              end
            end
          elsif @token_param[:is_default] == true
            @customer.default_source = @token_param[:card_id]
            begin
              @customer.save
              @customer_default_source = @customer.sources.retrieve(@customer.default_source)
            rescue
              render json: {status: false, message: "Error updating card"}
            else
              render json: {status: true, data: v1_format(@customer_default_source)}
            end
          end

        end
      else
        render json: {status: false, message: "Card ID cannot be blank"}
      end
    else
      render json: {status: false, message: "Wrong parameters received"}
    end
  end
  def v1_format source
    {
      id: source.id,
      is_default: true,
      brand: source.brand,
      last4: source.last4,
      exp_month: source.exp_month,
      exp_year: source.exp_year
    }
  end
  private
  def token_params
    params.required(:token).permit(:number, :card_id, :is_default, :delete)
  end
end