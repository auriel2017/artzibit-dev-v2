class Api::V1::OrdersController < UserModuleController
  def index
    @orders = current_user.orders
    render json: {status: true, data: @orders.map{|o| o.v1_format}}
  end
  def show
    @order = Order.find_by id: params[:id]
    unless @order.blank?
      render json: {status: true, data: @order.v1_format("show")}
    else
      render json: {status: false, message: "No order found"}
    end
  end
end