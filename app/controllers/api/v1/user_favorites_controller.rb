class Api::V1::UserFavoritesController < UserModuleController
  def index
    @user_favorites = current_user.user_favorites.where(deleted: false)
    render json: {status: true, data: @user_favorites.map{|f| f.v1_format}}
  end
  def create
    @user_favorite = UserFavorite.new user_favorite_params
    @user_favorite.deleted = false
    @user_favorite.user_id = current_user.id
    if @user_favorite.save
      render json: {status: true, data: @user_favorite}
    else
      render json: {status: false, message: @user_favorite.errors.full_messages.
        first}
    end
  end
  def update
    @user_favorite = UserFavorite.find params[:id]
    if @user_favorite.update_attributes user_favorite_params
      render json: {status: true, data: @user_favorite}
    else
      render json: {status: false, message: @user_favorite.errors.full_messages.
        first}
    end
  end
  private
  def user_favorite_params
    params.required(:user_favorite).permit(:artwork_id, :deleted)
  end
end