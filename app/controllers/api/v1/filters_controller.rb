class Api::V1::FiltersController < AllUsersModuleController
  skip_before_action :authenticate_user!
  respond_to :json
  def index
    @filters = [{name: "artists",key: "artist_id", values: User.all.order(:name).
      reject{|a| a.artworks.actives.count == 0}.map{|a| a.v1_format}},
      {name: "categories",key: "category_id",values: Category.all.order(:name).map{|c| c.v1_format}},
      {name: "styles",key: "artwork_style_id",values: ArtworkStyle.all.order(:name).map{|as| as.v1_format}}]
    respond_with status: true, data: @filters
  end
end