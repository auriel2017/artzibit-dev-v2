class Api::V2::UserBankDetailsController < UserModuleController
  before_filter :check_is_default_field, only: :update
  def index
    cards = Array.new
    current_user.user_bank_details.map{|u| cards += u.cards}
    render json: {status: true, data: cards}
  end
  def create
    customer_stripe_id = current_user.try(:stripe_id)
    if customer_stripe_id.blank?
      begin
        customer = Stripe::Customer.create(
          :email => current_user.email,
          :source => card_params[:number]
        )
      rescue => e
        render json: {status: false, message: e.message}
      else
        user_bank_detail = UserBankDetail.new card_params
        user_bank_detail.stripe_id = customer.id
        if user_bank_detail.save
          # cards = Array.new
          # current_user.user_bank_details.map{|u| cards += u.cards}
          card = user_bank_detail.retrieve_card(customer.default_source)
          render json: {status: true, data: card}
        else
          render json: {status: false, message: user_bank_detail.errors.
            full_messages.first}
        end
      end
    else
      begin
        customer = Stripe::Customer.retrieve(customer_stripe_id)
      rescue => e
        render json: {status: false, message: e.message}
      else
        begin
          card = customer.sources.create({source: card_params[:number]})
        rescue => e
          render json: {status: false, message: e.message}
        else
          if card_params[:is_default].to_s.to_bool
            customer.default_source = card.id
            customer.save
          end
          # cards = Array.new
          # current_user.user_bank_details.map{|u| cards += u.cards}
          bank_detail = current_user.try(:default_bank_detail)
          unless bank_detail.blank?
            card_detail = bank_detail.retrieve_card card.id
            render json: {status: true, data: card_detail}
          else
            render json: {status: false, message: "Something went wrong but succesfully created"}
          end
        end
      end
    end
  end
  def update
    customer_stripe_id = current_user.try(:stripe_id)
    error = ""
    unless customer_stripe_id.blank?
      begin
        customer = Stripe::Customer.retrieve(customer_stripe_id)
      rescue => e
        render json: {status: false, message: e.message}
      else
        if card_params[:is_default].to_s.to_bool
          customer.default_source = card_params[:card_id]
          customer.save
        end
        if card_params[:to_delete].to_s.to_bool
          begin
            customer.sources.retrieve(card_params[:card_id]).delete
            customer.save
          rescue => e
            error = e.message
          end
        end
        if error.blank?
          bank_detail = current_user.try(:default_bank_detail)
          unless bank_detail.blank?
            card_detail = bank_detail.retrieve_card card_params[:card_id]
            render json: {status: true, data: card_detail}
          else
            render json: {status: false,
              message: "Something went wrong but succesfully created"}
          end
        else
          render json: {status: false, message: error}
        end
      end
    else
      render json: {status: false, message: "No user bank details saved"}
    end
  end
  def check_is_default_field
    if params[:user_bank_detail][:is_default].to_s.to_bool == false
      render json: {status: false, message: "You cannot undefault a card"} unless \
        params[:user_bank_detail][:is_default].blank?
    elsif params[:user_bank_detail][:to_delete].to_s.to_bool == false
      render json: {status: false, message: "You cannot undelete a card"} unless \
        params[:user_bank_detail][:to_delete].blank?
    end
  end
  private
  def card_params
    params[:user_bank_detail][:user_id] = current_user.try(:id)
    params.required(:user_bank_detail).permit(:id, :card_id, :number, :user_id,
      :is_default, :to_delete)
  end
end