class Api::V2::SupportsController < UserModuleController
  def create
    support = Support.new support_params
    if support.save
      render json: {status: true, data: support}
    else
      render json: {status: false, message: support.errors.full_messages.
        first}
    end
  end
  private
  def support_params
    params[:support][:user_id] = current_user.try(:id)
    params.required(:support).permit(:id, :user_id, :contact_no, :content)
  end
end