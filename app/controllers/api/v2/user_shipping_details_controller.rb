class Api::V2::UserShippingDetailsController < UserModuleController
  before_filter :check_is_default_field, only: :update
  def index
    shipping_details = current_user.user_shipping_details.actives.by_name.
      map{|a| a.v2_format}
    render json: {status: true, data: shipping_details}
  end
  def create
    shipping_detail = UserShippingDetail.new shipping_params
    if shipping_detail.save
      render json: {status: true, data: shipping_detail.v2_format}
    else
      render json: {status: false, message: shipping_detail.errors.
        full_messages.first}
    end
  end
  def update
    shipping_detail = UserShippingDetail.find_by id: params[:id]
    unless shipping_detail.blank?
      if shipping_detail.update_attributes shipping_params
        render json: {status: true, data: shipping_detail.v2_format}
      else
        render json: {status: false, message: shipping_detail.errors.
          full_messages.first}
      end
    else
      render json: {status: false, message: "No shipping detail found"}
    end
  end
  def destroy
    # raise params[:id].inspect
    shipping_detail = UserShippingDetail.find_by(id: params[:id])
    unless shipping_detail.blank?
      if shipping_detail.can_be_deleted?
        begin
          shipping_detail.destroy
        rescue
          shipping_detail.is_default = false
          shipping_detail.active = false
          shipping_detail.save validate: false
        end
      else
        shipping_detail.is_default = false
        shipping_detail.active = false
        shipping_detail.save validate: false
      end
      render json: {status: true, message: "Address succesfully deleted"}
    else
      render json: {status: false, message: "No shipping detail found"}
    end
  end
  private
  def check_is_default_field
    if params[:user_shipping_detail][:is_default].to_s.to_bool == false
      render json: {status: false, message: "You cannot undefault a shipping detail"}
    end
  end
  def shipping_params
    params[:user_shipping_detail][:detailed_addr] = params[:user_shipping_detail][:address] \
      unless params[:user_shipping_detail][:address].blank?
    params[:user_shipping_detail][:user_id] = current_user.try(:id)
    params.required(:user_shipping_detail).permit(:id, :first_name, :last_name,
      :user_id, :phone_number, :detailed_addr, :postal_code, :country,
      :is_default)
  end
end