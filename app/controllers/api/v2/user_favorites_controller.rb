class Api::V2::UserFavoritesController < UserModuleController
  before_filter :check_artwork_id, only: [:create]
  def index
    # user_favorites = UserFavorite.where("user_favorites.user_id = ?
    #   and user_favorites.deleted = ? and artworks.active = ?", current_user.try(:id),
    #   false, true).joins(:artwork)

    user_favorites = Artwork.joins(:user_favorites).where("user_favorites.user_id = ?
      and user_favorites.deleted = ? and artworks.active = ?", current_user.try(:id),
      false, true)

    squares = FixedSize.where(size_type: "square").by_name
    portraits = FixedSize.where(size_type: "portrait").by_name
    landscapes = FixedSize.where(size_type: "landscape").by_name
    size = Hash.new
    size["square"] = {print_sizes: squares.map{|s| s.v2_format}}
    size["portrait"] = {print_sizes: portraits.map{|s| s.v2_format}}
    size["landscape"] = {print_sizes: landscapes.map{|s| s.v2_format}}
    material = Hash.new
    material["square"] = {materials: Material.by_name.map{|a|
      {id: a.id, name: a.name, amounts: squares.map{|s| s.v2_format("material",
      a.id)}}}}
    material["portrait"] = {materials: Material.by_name.map{|a|
      {id: a.id, name: a.name, amounts: portraits.map{|s| s.v2_format("material",
      a.id)}}}}
    material["landscape"] = {materials: Material.by_name.map{|a|
      {id: a.id, name: a.name, amounts: landscapes.map{|s| s.v2_format("material",
      a.id)}}}}
    frame = Hash.new
    frame["square"] = {frame_types: FrameType.blank_frame_type + FrameType.by_name.map{|a|
      {id: a.id, name: a.name, amounts: squares.map{|s| s.v2_format("frame_type", a.id)}}}}
    frame["portrait"] = {frame_types: FrameType.blank_frame_type + FrameType.by_name.map{|a|
      {id: a.id, name: a.name, amounts: portraits.map{|s| s.v2_format("frame_type", a.id)}}}}
    frame["landscape"] = {frame_types: FrameType.blank_frame_type + FrameType.by_name.map{|a|
      {id: a.id, name: a.name, amounts: landscapes.map{|s| s.v2_format("frame_type", a.id)}}}}
    installs = {installation_amounts: InstallationRange.by_min.map{|i| i.v2_format}}
    size_dtls = Hash.new
    size_dtls["square"] = size["square"].merge(material["square"]).merge(frame["square"]).merge(installs)
    size_dtls["portrait"] = size["portrait"].merge(material["portrait"]).merge(frame["portrait"]).merge(installs)
    size_dtls["landscape"] = size["landscape"].merge(material["landscape"]).merge(frame["landscape"]).merge(installs)

    render json: {status: true,
      data: user_favorites.map{|u| u.v2_format(current_user.try(:id)).
        merge(size_dtls["#{u.orientation}"])}}
  end
  def create
    user_favorite = UserFavorite.where(user_id: current_user.id,
      artwork_id: favorite_params[:artwork_id]).first_or_initialize
    if UserFavorite.exists? user_favorite.id
      user_favorite.deleted = !user_favorite.deleted
    else
      user_favorite.deleted = false
    end
    if user_favorite.save
      render json: {status: true,
        data: user_favorite.reload.artwork.v2_format(current_user.try(:id))}
    else
      render json: {status: false, message: user_favorite.errors.
        full_messages.first}
    end
  end
  def check_artwork_id
    artwork = Artwork.find_by id: favorite_params[:artwork_id],
      active: true
    if artwork.blank?
      render json: {status: false, message: "Artwork not found"}
    end
  end
  private
  def favorite_params
    params.required(:user_favorite).permit(:artwork_id, :deleted)
  end
end