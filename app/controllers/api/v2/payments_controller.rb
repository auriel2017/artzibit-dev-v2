class Api::V2::PaymentsController < AllUsersModuleController

	def create
		currency = "SGD"
		valid_params = permitted_params
		result = {}
    begin
	    # Charge the user's card:
	    amount = (valid_params[:amount]).to_i
	    amount_desc = Money.new(amount, currency).format
			charge = Stripe::Charge.create(
			  :amount => amount,
			  :currency => currency,
			  :description => "#{valid_params[:token][:email]} paid a total amount of #{currency} #{amount_desc}",
			  :source => valid_params[:token][:id],
			)	
	    result =  {status: 200, message:'Card charged successfully.'}
	  rescue Stripe::CardError => e
	    # CardError; display an error message.
	    result = {status: false, message: 'Error on card', data: e}
	  rescue => e
	    result = {status: false, message: 'Something went wrong', data: e}
	  end

	  render json: result
	end

	private

	def permitted_params
		params.permit(:amount, :token => [:id, :email])
	end

end
