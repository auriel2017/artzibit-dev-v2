class Api::V2::SessionsController < Users::SessionsController
  respond_to :json
  def create
    user = User.new user_params
    if user.thru_email?
      sign_in_user = user.authenticate
      unless sign_in_user.blank?
        render json: {status: true, data: sign_in_user.v2_format}
      else
        render json: {status: false, message: "Invalid Email/Password"}
      end
    elsif user.thru_fb?
      fb_user = User.find_by(email: user.email)
      if fb_user.present?
        auth = UserAuthentication.find_by user_id: fb_user.id, provider: \
          user.user_authentication.provider, uid: user.user_authentication.uid
        unless auth.blank?
          render json: {status: true, data: fb_user.v2_format}
        else
          render json: {status: false, message: "Invalid Email/Password"}
        end
      else
        if user.save(validate: false)
          render json: {status: true, data: user.v2_format}
        else
          render json: {status: false, message: user.errors.full_messages.first}
        end
      end
    else
      render json: {status: false, message: "Wrong parameters"}
    end
  end
  def destroy
    super
  end
  private
  def user_params
    params[:user][:by_pass_activation_code] = true
    params.required(:user).permit(:email, :password, :first_name, :last_name,
      :country, :contact_no, :by_pass_activation_code,
      user_authentication_attributes: [:id, :user_id, :provider, :uid])
  end
  def invalid_login_attempt
    warden.custom_failure!
    render json: {success:false, message: "Error with your login or password"},
      status: 401
  end
end