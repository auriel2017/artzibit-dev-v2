class Api::V2::CartItemsController < UserModuleController
  def index
    cart = Hash.new
    shipping_dtl = Hash.new
    bank_dtl = Hash.new
    cart = current_user.try(:current_cart)
    if cart.try(:user_shipping_detail_id).blank?
      default_shipping = current_user.default_shipping_detail.v2_format unless
        current_user.default_shipping_detail.blank?
      shipping_dtl = {user_shipping_detail: default_shipping} unless default_shipping.blank?
    else
      shipping_dtl = {user_shipping_detail: cart.user_shipping_detail.v2_format}
    end
    if cart.try(:card_id).blank? # card.try(:user_bank_detail_id).blank?
      default_bank = current_user.try(:default_bank_detail)
      default_card = default_bank.try(:default_card)
      bank_dtl = {user_bank_detail: default_card} unless default_card.blank?
    else
      bank = UserBankDetail.find_by user_id: current_user.try(:id),
        id: cart.try(:user_bank_detail_id)
      card = bank.retrieve_card cart.try(:card_id)
      bank_dtl = {user_bank_detail: card} unless card.blank?
    end
    update_cart = update_cart_items_price
    cart = update_cart.v2_format unless update_cart.blank?
    render json: {status: true, data: cart.merge(shipping_dtl).merge(bank_dtl)}
  end
  def create
    cart = Cart.available.where(user_id: current_user.try(:id)).first_or_initialize
    if Cart.exists? cart.id
      item_param_wo_quantity = item_param
      item_param_wo_quantity.delete(:quantity)
      cart_item = CartItem.where(cart_id: cart.id).where(item_param_wo_quantity).
        first_or_initialize
      items = cart_item.attributes.symbolize_keys
      items[:quantity] = item_param[:quantity].to_i
      cart_items = {cart_items_attributes: [items]}
      if cart.update_attributes cart_items
        cart_item = CartItem.where(cart_id: cart.id).where(item_param_wo_quantity).
          first
        render json: {status: true, message: "Item Added", data: cart_item.reload.
          v2_format}
      else
        render json: {status: false, message: cart.errors.full_messages.first}
      end
    else
      cart_item = CartItem.where(cart_id: cart.id).where(item_param).
        first_or_initialize
      cart_items = cart_item.attributes.symbolize_keys
      cart.cart_items_attributes = [cart_items]
      if cart.save
        cart_item = CartItem.where(cart_id: cart.id).where(item_param).
          first_or_initialize
        render json: {status: true, message: "Item Added", data: cart_item.reload.
          v2_format}
      else
        render json: {status: false, message: cart.errors.full_messages.first}
      end
    end
  end
  def update
    cart_item = CartItem.find_by id: params[:id]
    unless cart_item.blank?
      item_param_wo_quantity = item_param
      item_param_wo_quantity.delete(:quantity)
      ini_cart_item = CartItem.where(cart_id: cart_item.cart_id).
        where(item_param_wo_quantity).first_or_initialize
      if ini_cart_item.id.blank?
        items = ini_cart_item.attributes.symbolize_keys
        items = items.reject {|k,v| k == :id or k == :created_at or k == :updated_at}
        items[:quantity] = item_param[:quantity].to_i
        if cart_item.update_attributes items
          render json: {status: true, data: cart_item.v2_format}
        else
          render json: {status: false, message: cart_item.errors.
            full_messages.first}
        end
      else
        items = ini_cart_item.attributes.symbolize_keys
        items[:quantity] = item_param[:quantity].to_i
        if ini_cart_item.update_attributes items
          cart_item.destroy if cart_item.id != ini_cart_item.id
          render json: {status: true, data: ini_cart_item.v2_format}
        else
          render json: {status: false, message: ini_cart_item.errors.
            full_messages.first}
        end
      end
    else
      render json: {status: false, message: "No Cart Item found"}
    end
  end
  def destroy
    cart_item = CartItem.find_by id: params[:id]
    unless cart_item.blank?
      cart_item.destroy
      cart = Hash.new
      update_cart = update_cart_items_price
      cart = update_cart.v2_format unless update_cart.blank?
      render json: {status: true, data: cart}
    else
      render json: {status: false, message: "No Cart Item found"}
    end
  end
  def update_cart_items_price
    current_cart = current_user.try(:current_cart)
    unless current_cart.blank?
      current_cart.cart_items.each do |item|
        if item.total_amount != item.total_amount("save")
          item = CartItem.update_to_current_price_fields item.id
          item.save(validate: false)
        end
      end
      update_cart = Cart.update_cart_amount current_cart.id
      update_cart.save
      update_cart
    else
      current_cart
    end
  end
  private
  def item_param
    if params[:cart_item][:frame].blank? or params[:cart_item][:frame].to_s.to_bool == false
      params[:cart_item][:frame] = false
      params[:cart_item][:frame_type_id] = nil
      params[:cart_item][:fixed_size_frame_price_id] = nil
    end
    # if params[:cart_item][:fixed_size_price_id].blank?
    #   fixed_size_id = FixedSize.find_by(width: params[:cart_item][:width],
    #     height: params[:cart_item][:height])
    # end
    params[:cart_item][:installation] = false if params[:cart_item][:installation].blank?
    params[:cart_item][:mark_up_price_id] = nil if params[:cart_item][:mark_up_price_id].blank?
    params.required(:cart_item).permit(:id, :artwork_id, :fixed_size_id,
      :material_id, :frame, :frame_type_id, :fixed_size_price_id, :mark_up_price_id,
      :fixed_size_frame_price_id, :quantity, :installation, :width, :height)
  end
end