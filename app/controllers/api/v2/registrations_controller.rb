require 'base64'
class Api::V2::RegistrationsController < Users::RegistrationsController

  respond_to :json

  acts_as_token_authentication_handler_for User
  before_action :authenticate_user!

  prepend_before_filter :require_no_authentication, only: [ :new, :create, :edit, :update, :cancel ]
  prepend_before_filter :authenticate_scope!, only: [:destroy]
  acts_as_token_authentication_handler_for User, fallback_to_devise: false

  # Disable token authentication for all actions
  # See https://github.com/gonzalo-bulnes/simple_token_authentication/blob/master/lib/simple_token_authentication/acts_as_token_authentication_handler.rb#L12-L15
  skip_before_filter :authenticate_user_from_token!
  skip_before_filter :authenticate_user!

  # Enable token authentication only for the :update, :destroy actions
  before_filter :authenticate_user_from_token!, :only => [:update, :destroy]
  before_filter :authenticate_user!, :only => [:update, :destroy]

  before_filter :configure_permitted_parameters, if: :devise_controller?

  def create
    format_image!
    override_create
  end

  def update
    override_update
  end

  def destroy
    super
  end

  def format_image!
    params_avatar = params[:user][:avatar]
    unless params_avatar.blank?
      avatar =  Base64.encode64(open(params_avatar).read).to_s
      params[:user][:avatar] = format_image avatar
    end
  end
  def format_image image_data
    data = nil
    unless image_data.blank?
      data = StringIO.new(Base64.decode64(image_data))
      data.class.class_eval {attr_accessor :original_filename, :content_type}
      data.original_filename = "User/Image" + Time.now.to_s.gsub(" ","_")
      data.content_type = "image/jpg"
    end
    return data
  end
  protected
  def override_create
    build_resource(sign_up_params)
    resource.save
    if resource.persisted?
      render json: {status: true, data: resource.v2_format}
    else
      if "Password confirmation doesn't match Password" == resource.errors.full_messages.first
        @error = "The password you entered did not match."
      else
        @error = resource.errors.full_messages.first
      end
      render json: {status: false, message: @error}
    end
  end
  def override_update
    unless current_user.blank?
      resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
      resource_updated = update_resource(resource, account_update_params)
      if resource_updated
        render json: {status: true, data: account_update_params}
      else
        render json: {status: false, message: resource.errors.full_messages.first}
      end
    else
      render json: {status: false, message: "Error parameters"}
    end
  end
  def configure_permitted_parameters
    params[:user][:by_pass_activation_code] = true
    devise_parameter_sanitizer.permit(:sign_up, keys: [:email, :password,
      :password_confirmation, :first_name, :last_name, :country, :contact_no,
      :by_pass_activation_code, user_authentication_attributes: [:provider, :uid]])

    devise_parameter_sanitizer.permit(:account_update, keys: [:email, :password,
      :password_confirmation, :current_password, :first_name, :last_name,
      :country, :contact_no, :website_portfolio])
  end
  def update_resource(resource, params)
    if params[:password].blank?
      resource.update_without_password(params)
    else
      resource.update_with_password(params)
    end
  end
end