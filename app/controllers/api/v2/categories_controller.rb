class Api::V2::CategoriesController < AllUsersModuleController
  def index
    @banners = ContentBanner.active.map{|cb| cb.v2_format(current_user.try(:id))}
    @categories = Category.by_sequence.map{|c| c.v2_format}
  if params[:source] == "microsite"
      @artworks = Artwork.top_picks.reject{|a| (a.status != "approve")}.map{|a| a.v2_format(current_user.try(:id))}
      @home = {
        bannered_contents: @banners,
        featured_categories: @categories,
        featured_artworks: @artworks
      }
    else
      @home = {
        bannered_contents: @banners,
        featured_categories: @categories
      }
    end
    render json: {status: true, data: @home}
  end
end