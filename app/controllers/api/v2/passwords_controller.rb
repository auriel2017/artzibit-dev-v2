class Api::V2::PasswordsController < Users::PasswordsController
  respond_to :json
  def create
    user = User.find_by user_params
    unless user.blank?
      if user.has_facebook?
        render json: {message: "Your user not log thru facebook", status: false}
      else
        super
      end
    else
      render json: {message: "User not found", status: false}
    end
  end
  private
  def user_params
    params.required(:user).permit(:email)
  end
end