class Api::V2::CartsController < UserModuleController
  def create
    cart = current_user.try(:current_cart)
    unless cart.blank?
      if cart_params[:status] == "checkout"
        cart_wo_status = cart_params
        cart_wo_status.delete(:status)
        if cart.update_attributes cart_wo_status
          order = Order.new
          order.user_id = cart.user_id
          order.cart_id = cart.id
          order.user_shipping_detail_id = cart.user_shipping_detail_id
          order.card_stripe_id = cart.card_id
          order.current_status = "order"
          if order.valid? and cart.valid?
            begin
              charge = Stripe::Charge.create(
                :customer    => cart.user_bank_detail.stripe_id,
                :amount      => (cart.total_price * 100).to_i,
                :description => cart.charge_description,
                :source      => cart.card_id,
                :currency    => 'usd'
              )
            rescue => e
              render json: {status: false, message: e.message}
            else
              order.save
              order_items = [{
                order_id: order.id,
                status: "order",
                processed_at: cart.updated_at
              }]
            order.order_histories_attributes = order_items
              order.save
              cart.update_attributes cart_params
              render json: {status: true, data: order}
            end
          else
            unless order.errors.blank?
              render json: {status: false, message: order.errors.full_messages.first}
            else
              render json: {status: false, message: cart.errors.full_messages.first}
            end
          end
        else
          render json: {status: false, message: cart.errors.full_messages.first}
        end
      else
        if cart.update_attributes cart_params
          render json: {status: true, data: cart.v2_format("update")}
        else
          render json: {status: false, message: cart.errors.full_messages.first}
        end
      end
    else
      render json: {status: false, message: "No found cart"}
    end
  end
  private
  def cart_params
    params.required(:cart).permit(:status, :user_shipping_detail_id,
      :user_bank_detail_id, :card_id, :actual_promo_code)
  end
end