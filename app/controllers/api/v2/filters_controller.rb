class Api::V2::FiltersController < ApplicationController
  def index
    artists = User.by_name.map{|u| u.v2_format("filter")}
    categories = Category.by_sequence.map{|c| c.v2_format("filter")}
    filters = Array.new
    filters << {
      name: "Artists",
      keyword: "artist_ids",
      values: artists
    }
    filters << {
      name: "Categories",
      keyword: "category_ids",
      values: categories
    }
    render json: {status: true, data: filters}
  end
end