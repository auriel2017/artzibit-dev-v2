class Api::V2::WebRepositoriesController < AllUsersModuleController
  def index
    web_repos = filtered_data
    unless web_repos.blank?
      render json: {status: true, data: web_repos } 
    else
      render json: {status: false, message: "No web repository found"}
    end
  end

  private

  # return an Array of filterred data if params[:list]
  def filtered_data
  	data = nil
    if !params["list"] 
    	data = WebRepository.find_by(filter_params)
    	data = data.v2_format if data.present?
    else
    	data = WebRepository.where(filter_params)
    	data = data.map{|i| i.v2_format } if data.present?
    	if params[:group_by_field].present? && WebRepository.column_names.include?(params[:group_by_field])
    		data = group_by_field(params[:group_by_field], data) 
    	end
    end
  	data
  end

  # return list of Array grouped by the given key /param[:group_by_field]
  def group_by_field(key, items)
  	key = key.to_sym
  	group = {}
    items.map{|i|  !group.key?(i[key]) ? group[i[key]] = [i] : group[i[key]] << i }
    group
  end


  def filter_params
  	model_keys = WebRepository.column_names 
  	permitted_params.select{|k| model_keys.include?(k.to_s)}
  end

  def permitted_params
  	permitted = []
  	permitted += WebRepository.column_names 
  	permitted += %w(list group_by_field)
  	params.permit(permitted)
  end

end