class Api::V2::ReviewsController < AllUsersModuleController
  before_action :authenticate_login_user!, unless: :prohibited
  def prohibited
    ["index", "show"].include? action_name
  end
  def index
    reviews = Review.where(artwork_id: params[:artwork_id]).by_date
    render json: {status: true, data: reviews.map{|r| r.v2_format}}
  end
  def show
    review = Review.find_by id: params[:id]
    unless review.blank?
      render json: {status: true, data: review.v2_format}
    else
      render json: {status: false, message: "No review found"}
    end
  end
  def create
    review = Review.new review_params
    if review.save
      render json: {status: true, data: review.v2_format}
    else
      render json: {status: false, message: review.errors.full_messages.first}
    end
  end
  private
  def format_image image_data
    data = nil
    unless image_data.blank?
      data = StringIO.new(Base64.decode64(image_data))
      data.class.class_eval {attr_accessor :original_filename, :content_type}
      data.original_filename = "User/Image" + Time.now.to_s.gsub(" ","_")
      data.content_type = "image/jpg"
    end
    data
  end
  def review_params
    params[:review][:user_id] = current_user.try(:id)
    params[:review][:image] = format_image(params[:review][:image]) unless \
      params[:review][:image].blank?
    params.required(:review).permit(:id, :user_id, :artwork_id, :image, :content)
  end
end