class Api::V2::ArtworksController < AllUsersModuleController
  before_action :authenticate_login_user!, unless: :prohibited
  def prohibited
    ["show", "index"].include? action_name
  end
  def index
    if params[:marketplace].to_s.to_bool or params[:marketplace].blank?
      @artworks = Artwork.for_marketplace.filter_by_keyword(params[:keyword])

      @artworks = @artworks.filter_by_ids(params[:artwork_ids]) unless \
        params[:artwork_ids].blank?

      @artworks = @artworks.filter_by_artists(params[:artist_ids]) unless \
        params[:artist_ids].blank?

      @artworks = @artworks.filter_by_categories(params[:category_ids]) unless \
        params[:category_ids].blank?

      @artworks = @artworks.filter_by(params[:sort]) unless \
        params[:sort].blank?

      # If filter == 'popularity'
      @artworks = @artworks.sort_by{|a| (a.popularity * -1)} if params[:sort] == "popularity" or params[:sort].blank?

      @artworks = @artworks.reject {|a| (a.status != "approve") }.map{|a| a} unless \
        @artworks.blank?

    else
      @artworks = Artwork.where(user_id: current_user.try(:id)).actives.by_name.
        filter_by_privacy_v2(params[:privacy]).
        filter_by_artists(params[:artist_ids])
    end

    # Fixed Sizes
    squares = FixedSize.where(size_type: "square").by_name
    portraits = FixedSize.where(size_type: "portrait").by_name
    landscapes = FixedSize.where(size_type: "landscape").by_name

    # Print Sizes
    size = Hash.new
    size["square"] = {print_sizes: squares.map{|s| s.v2_format}}
    size["portrait"] = {print_sizes: portraits.map{|s| s.v2_format}}
    size["landscape"] = {print_sizes: landscapes.map{|s| s.v2_format}}

    # Materials
    material = Hash.new
    material["square"] = {materials: Material.by_name.map{|a|
      {id: a.id, name: a.name, amounts: squares.map{|s| s.v2_format("material", a.id)}}}}
    material["portrait"] = {materials: Material.by_name.map{|a|
      {id: a.id, name: a.name, amounts: portraits.map{|s| s.v2_format("material", a.id)}}}}
    material["landscape"] = {materials: Material.by_name.map{|a|
      {id: a.id, name: a.name, amounts: landscapes.map{|s| s.v2_format("material", a.id)}}}}

    # Frame Types
    frame = Hash.new
    frame["square"] = {frame_types: FrameType.blank_frame_type + FrameType.by_name.map{|a|
      {id: a.id, name: a.name, amounts: squares.map{|s| s.v2_format("frame_type", a.id)}}}}
    frame["portrait"] = {frame_types: FrameType.blank_frame_type + FrameType.by_name.map{|a|
      {id: a.id, name: a.name, amounts: portraits.map{|s| s.v2_format("frame_type", a.id)}}}}
    frame["landscape"] = {frame_types: FrameType.blank_frame_type + FrameType.by_name.map{|a|
      {id: a.id, name: a.name, amounts: landscapes.map{|s| s.v2_format("frame_type", a.id)}}}}

    # Installation Amounts
    installs = {installation_amounts: InstallationRange.by_min.map{|i| i.v2_format}}

    # Size Details
    size_dtls = Hash.new
    size_dtls["square"] = size["square"].merge(material["square"]).merge(frame["square"]).merge(installs)
    size_dtls["portrait"] = size["portrait"].merge(material["portrait"]).merge(frame["portrait"]).merge(installs)
    size_dtls["landscape"] = size["landscape"].merge(material["landscape"]).merge(frame["landscape"]).merge(installs)

    render json: {status: true,
      data: @artworks.reject{|a| a.orientation.blank?}.map{|a|
      a.v2_format(current_user.try(:id)).merge(size_dtls["#{a.orientation}"])}}
  end
  def show
    artwork = Artwork.find_by(id: params[:id], active: true)
    unless artwork.blank?
      if params[:cart_item_id].blank?
        render json: {status: true, data: artwork.v2_format(current_user.try(:id),
          "show")}
      else
        if CartItem.exists? params[:cart_item_id]
          render json: {status: true, data: artwork.v2_format(current_user.try(:id),
            "show", params[:cart_item_id])}
        else
          render json: {status: false, message: "No Cart Item found"}
        end
      end
    else
      render json: {status: false, message: "No Artwork Found"}
    end
  end
  def new
    categories = Category.all.map{|c| c.v2_format("filter")}
    render json: {status: true, data: categories}
  end
  def create
    artwork = Artwork.new artwork_params
    if artwork.save
      render json: {status: true, data: artwork.v2_format(current_user.id)}
    else
      render json: {status: false, message: artwork.errors.full_messages.first}
    end
  end
  def edit
    artwork = Artwork.find_by id: params[:id], user_id: current_user.try(:id),
      active: true
    unless artwork.blank?
      render json: {status: true, data: artwork.v2_format(nil, "edit")}
    else
      render json: {status: false, message: "No artwork found"}
    end
  end
  def update
    artwork = Artwork.find_by id: params[:id], user_id: current_user.try(:id),
      active: true
    unless artwork.blank?
      if artwork.update_attributes artwork_params
        render json: {status: true, data: artwork.v2_format(current_user.try(:id))}
      else
        render json: {status: false, message: artwork.errors.full_messages.first}
      end
    else
      render json: {status: false, message: "No artwork found"}
    end
  end
  def destroy
    artwork = Artwork.find_by id: params[:id], user_id: current_user.try(:id)
    unless artwork.blank?
      if artwork.can_be_deleted?
        begin
          artwork.destroy
        rescue
          artwork.active = false
          artwork.save
        end
      else
        artwork.active = false
        artwork.save
      end
      render json: {status: true, message: "Artwork Deleted"}
    else
      render json: {status: false, message: "No artwork found"}
    end
  end
  private
  def format_image image_data
    data = nil
    unless image_data.blank?
      data = StringIO.new(Base64.decode64(image_data))
      data.class.class_eval {attr_accessor :original_filename, :content_type}
      data.original_filename = "User/Image" + Time.now.to_s.gsub(" ","_")
      data.content_type = "image/jpg"
    end
    data
  end
  def format_tags tag_names
    data = nil
    tag_names = tag_names.split(",")
    tag_names.count.to_i.times do |t|
      next if tag_names[t].blank?
      new_value = tag_names[t].gsub("*", "").humanize
      tag_names[t] = Tag.where(name: new_value).first_or_create(name: new_value).id
    end
    data = tag_names - [""]
    data
  end
  def artwork_params
    if action_name == "create"
    params[:artwork][:artwork_images_attributes] = \
      [{image: format_image(params[:artwork][:image])}] unless \
      params[:artwork][:image].blank?
    else
    params[:artwork][:artwork_images_attributes] = \
      [{id: ArtworkImage.find_by(artwork_id: params[:id]).try(:id),
        image: format_image(params[:artwork][:image])}] unless \
      params[:artwork][:image].blank?
    end
    params[:artwork][:mark_up_prices_attributes] = \
      [{type: "Price", input_value: params[:artwork][:price]}] unless \
      params[:artwork][:price].blank?
    params[:artwork][:tag_ids] = format_tags(params[:artwork][:tags]) unless \
      params[:artwork][:tags].blank?
    params[:artwork][:name] = params[:artwork][:title]
    params[:artwork][:user_id] = current_user.id
    params[:artwork][:modified_user_id] = current_user.id
    params.required(:artwork).permit(:id, :name, :description, :category_id,
      :user_id, :privacy, :modified_user_id, tag_ids: [],
      artwork_images_attributes: [:id, :image], mark_up_prices_attributes: [:id,
      :type, :input_value])
  end
end