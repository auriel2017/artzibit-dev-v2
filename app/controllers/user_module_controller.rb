class UserModuleController < ApplicationController
  require 'base64'
  acts_as_token_authentication_handler_for User
  before_action :authenticate_user!
  private
  def authenticate_user!
    unless current_user
      return render json: {status: false, message: "You need to sign in or sign up before continuing."}
    else
      unless current_user.active
        return render json: {status: false, message: "Inactive users cannot
          access the app"}
      end
    end
  end
end