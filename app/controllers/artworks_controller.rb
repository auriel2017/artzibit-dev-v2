class ArtworksController < ApplicationController
  before_action :authenticate_user!
  layout "full_width"
  # before_action :validate_image, only: :create
  def index
    @all_artworks = current_user.artworks.actives
    if params[:scope] == "public"
      @artworks = current_user.artworks.public_artworks.actives
    elsif params[:scope] == "private"
      @artworks = current_user.artworks.private_artworks.actives
    else
      @artworks = @all_artworks.order("id desc")
    end
  end
  def show
    @artwork = Artwork.find_by id: params[:id]
  end
  def new
    @artwork = Artwork.new
    @bg_image = "microsite-bg-half-green.jpg"
    @color = "#777";
    @square_fixed_sizes = FixedSize.where(size_type: "square").order(:id)
    @landscape_fixed_sizes = FixedSize.where(size_type: "landscape").order(:id)
    @portrait_fixed_sizes = FixedSize.where(size_type: "portrait").order(:id)
  end
  def create
    @bg_image = "microsite-bg-half-green.jpg"
    @color = "#777";
    @artwork = Artwork.new artwork_params
    if @artwork.save
      flash[:notice] = "Successfully created user."
      redirect_to artworks_path
    else
      render :new
    end
  end
  def edit
    preview_crop_params
    case @art.orientation
      when "portrait"
        @portrait_fixed_sizes = FixedSize.where(size_type: "portrait").order(:id)
        @orientation = "portrait"
      when "landscape"
        @landscape_fixed_sizes = FixedSize.where(size_type: "landscape").order(:id)
        @orientation = "landscape"
      else
        @square_fixed_sizes = FixedSize.where(size_type: "square").order(:id)
        @orientation = "square"
    end
  end
  def reprocess
    @artwork = Artwork.find(params[:id])
    @artwork.artwork_images.each do |art_image|
      art_image.image.reprocess!
    end
    redirect_to artworks_path
  end
  def update
    @artwork = Artwork.find(params[:id])
    preview_crop_params
    if @artwork.update_attributes artwork_params
      unless artwork_image_params.blank?
        @artwork.update_attributes artwork_image_params
        @artwork.artwork_images.each do |art_image|
          art_image.image.reprocess!
        end
      end
      flash[:notice] = "Successfully updated user."
      redirect_to artworks_path
    else
      render :action => 'edit'
    end
  end
  def destroy
    @artwork = Artwork.find(params[:id])
    @artwork.destroy
    redirect_to artworks_path
  end
  private
  def preview_crop_params
    @artwork = Artwork.find params[:id]
    @art = @artwork.artwork_images.first
    @art_image = @art.image
    width = @art_image.width
    height = @art_image.height
    case @art.orientation
      when "portrait"
        @set_select = [0, 0, 200, 300]
        @width_cood = 200
        @length_cood = 300
        @aspect_ratio = 0.67
        height_aspect_ratio = height.to_f/width.to_f
        @height_var = 400 * height_aspect_ratio
        @width_var = 400
      when "landscape"
        @set_select = [0, 0, 300, 200]
        @width_cood = 300
        @length_cood = 200
        @aspect_ratio = 1.5

        height_aspect_ratio = height.to_f/width.to_f
        @height_var = 400 * height_aspect_ratio
        @width_var = 400

      else
        @set_select = [0, 0, 100, 100]
        @width_var = 400
        @height_var = 400
        @width_cood = 100
        @length_cood = 100
        @aspect_ratio = 1
    end
  end
  def artwork_image_params
    params.required(:artwork).permit(:id, artwork_images_attributes: [:id, :crop_x, :crop_y, :crop_w, :crop_h])
  end
  def artwork_params
    if action_name == "create"
      params.required(:artwork).permit(:id, :name, :category_id, :artwork_style_id,
        :user_id, :description, :privacy, :limited_edition, :quantity, artwork_images_attributes: [:image],
        mark_up_prices_attributes: [:percentage])
    else
      params.required(:artwork).permit(:id, :name, :description, :category_id, :artwork_style_id, :cropping,
        artwork_images_attributes: [:id, :image, :crop_x, :crop_y, :crop_w, :crop_h],
        mark_up_prices_attributes: [:id, :percentage])
    end
  end
end