class PageController < ApplicationController
	layout :determine_layout
  # before_action :authenticate_user!, only: [:profile]
  def home
    @test_user = TestUser.new
    @user = User.new

    # redirect to artworks controller
    # unless current_user.blank?
    #   redirect_to artworks_path
    # end

  end

  def about

  end

  def terms_of_service

  end

  def faq

  end

  def contact

  end

  def favorites

  end

  def cart

  end


  def profile

  end

  def manual_payment
  end

  

  private

  def determine_layout
    layout = "application"
    layouts = [
      {name: 'home', actions: %w(home)},
      {name: 'full_width', actions: %w(favorites cart profile)},
      {name: 'plain', actions: %w(manual_payment)},
    ]

    layouts.each do |item|
      if item[:actions].include?(action_name)
        layout = item[:name]
        break
      end
    end

    layout
  end

end
