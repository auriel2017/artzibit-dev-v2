class OrdersController < ApplicationController
  def show
    @order = Order.find params[:id]
    @from = ["From", "1 Fusionopolis Pt, 301-28/29 Gelexis", "Singapore 138522",
      "+6574246884"]
    @to = ["To", "7 Jalan Kilang 02-01", "Singapore 138522",
      "+6574246884"]
  end
end