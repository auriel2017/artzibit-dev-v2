class ArtworkApprovalDtlsController < ApplicationController
  def show
    @approval = ArtworkApprovalDtl.find_by(approve_token: params[:token])
    unless @approval.blank?
      @approval.status = params[:status]
      @approval.processed_at = Time.now
      @approval.save
    end
  end
end