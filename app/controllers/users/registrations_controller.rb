class Users::RegistrationsController < Devise::RegistrationsController
  # layout "half_screen", except: [:edit]
  # layout "application", only: [:edit]
  # before_filter :configure_sign_up_params, only: [:create]
  # before_filter :configure_account_update_params, only: [:update]
  before_filter :configure_permitted_parameters, if: :devise_controller?

  # GET /resource/sign_up
  def new
    @bg_image = "microsite-bg-half-pink.jpg"
    @color = "#777";
    super
  end

  # POST /resource
  def create
    @bg_image = "microsite-bg-half-pink.jpg"
    @color = "#777";
    super
    # set_flash_message(:alert, resource.errors.full_messages.first) unless resource.errors.blank?
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  def update
    resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    resource_updated = update_resource(resource, account_update_params)
    if resource_updated
      sign_in(User.find(params[:user][:id]), scope: :user, bypass: true)
      redirect_to artworks_path
    else
      set_flash_message(:alert, resource.errors.full_messages.first) unless resource.errors.blank?
      render :action => 'edit'
      # render :edit
    end
  end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :middle_name, :contact_no, :name, :artist_type, :website_portfolio,
      :by_pass_activation_code, :username, :country])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :middle_name, :contact_no, :username, :name,
      :email, :current_password, :password, :password_confirmation, :artist_type,
      :website_portfolio])
  end

  def update_resource(resource, params)
    if params[:password].blank?
      resource.update_without_password(params)
    else
      resource.update_with_password(params)
    end
  end

  # If you have extra params to permit, append them to the sanitizer.

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
