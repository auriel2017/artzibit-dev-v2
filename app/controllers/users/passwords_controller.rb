class Users::PasswordsController < Devise::PasswordsController
  # GET /resource/password/new
  def new
    @bg_image = "microsite-bg-half-pink.jpg"
    @color = "#777";
    super
  end

  # POST /resource/password
  # def create
  #   super
  # end

  def create
    @bg_image = "microsite-bg-half-pink.jpg"
    @color = "#777";
    super
  end

  # GET /resource/password/edit?reset_password_token=abcdef
  # def edit
  #   super
  # end

  # PUT /resource/password
  # def update
  #   super
  # end

  # protected
  protected
  # def after_resetting_password_path_for(resource)
  #   super(resource)
  # end

  # The path used after sending reset password instructions
  # def after_sending_reset_password_instructions_path_for(resource_name)
    # super(resource_name)
  # end
end
