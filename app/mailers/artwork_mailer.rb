class ArtworkMailer < ActionMailer::Base
  require 'open-uri'
  default from: Devise.mailer_sender
  def send_submission artwork
    @artwork = artwork
    attachments.inline['ig.png'] = {
      data: File.read(Rails.root.join('app/assets/images/ig.png')),
      mime_type: 'image/png'
    }
    attachments.inline['image.png'] = {
      data: open("#{@artwork.featured_image}").read,
      # data: open(Net::HTTP.get(URI.parse(@artwork.featured_image))),
      mime_type: 'image/png'
    }
    mail to: artwork.user.email, subject: "Artwork Submission - Artzibit"
  end
end