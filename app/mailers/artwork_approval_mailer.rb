class ArtworkApprovalMailer < ActionMailer::Base
  default from: Devise.mailer_sender
  def send_approval approval, admin_user
    @artwork_approval_dtl = approval
    @artwork_approval_hdr = approval.artwork_approval_hdr
    attachments.inline['ig.png'] = {
      data: File.read(Rails.root.join('app/assets/images/ig.png')),
      mime_type: 'image/png'
    }
    attachments.inline['image.png'] = {
    #   data: open("https:" + @artwork_approval_hdr.artwork.featured_artwork_image.image.url(:medium)).read,
      data: open("#{@artwork_approval_hdr.artwork.featured_image}").read,
      mime_type: 'image/png'
    }
    mail to: admin_user.email, subject: "Artzibit Approval"
  end
  def send_approve_artwork approval, admin_user
    @artwork_approval_dtl = approval
    @artwork_approval_hdr = approval.artwork_approval_hdr
    attachments.inline['ig.png'] = {
      data: File.read(Rails.root.join('app/assets/images/ig.png')),
      mime_type: 'image/png'
    }
    # attachments.inline['image.png'] = {
    #   data: open("https:" + @artwork_approval_hdr.artwork.featured_artwork_image.image.url(:medium)).read,
    #   mime_type: 'image/png'
    # }
    mail to: admin_user.email, subject: "Your artwork has been approved"
  end
  def send_reject_artwork approval, admin_user
    @artwork_approval_dtl = approval
    @artwork_approval_hdr = approval.artwork_approval_hdr
    attachments.inline['ig.png'] = {
      data: File.read(Rails.root.join('app/assets/images/ig.png')),
      mime_type: 'image/png'
    }
    # attachments.inline['image.png'] = {
    #   data: open("https:" + @artwork_approval_hdr.artwork.featured_artwork_image.image.url(:medium)).read,
    #   mime_type: 'image/png'
    # }
    mail to: admin_user.email, subject: "Your artwork has been rejected"
  end
end