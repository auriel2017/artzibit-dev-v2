class SupportMailer < ActionMailer::Base
  default from: Devise.mailer_sender
  def send_support support
    if Rails.env.development?
      email = ["aldrin@absolute-living.com"]
    else
      email = ["contact@uobsummit.com"]
    end
    @user = support.user
    @user_message = support
    attachments.inline['ig.png'] = {
      data: File.read(Rails.root.join('app/assets/images/ig.png')),
      mime_type: 'image/png'
    }
    mail to: email, subject: "App Support - Enquiry from #{@user.email}"
  end
end