class OrderMailer < ActionMailer::Base
  default from: Devise.mailer_sender
  def send_receipt order
    @order = Order.find order.id
    @name = @order.user.email
    attachments.inline['header_email.jpg'] = {
      data: File.read(Rails.root.join('app/assets/images/header_email.jpg')),
      mime_type: 'image/png'
    }
    attachments.inline['ig.png'] = {
      data: File.read(Rails.root.join('app/assets/images/ig.png')),
      mime_type: 'image/png'
    }
    mail to: @name, subject: "Artzibit Receipt"
  end
  def notify_printer order, printer_email
    @order = Order.find order.id
    attachments.inline['header_email.jpg'] = {
      data: File.read(Rails.root.join('app/assets/images/header_email.jpg')),
      mime_type: 'image/png'
    }
    attachments.inline['ig.png'] = {
      data: File.read(Rails.root.join('app/assets/images/ig.png')),
      mime_type: 'image/png'
    }
    mail to: printer_email, subject: "New order for Artzibit"
  end
  def notify_assignment order, sender_email
    @order = Order.find order.id
    attachments.inline['header_email.jpg'] = {
      data: File.read(Rails.root.join('app/assets/images/header_email.jpg')),
      mime_type: 'image/png'
    }
    attachments.inline['ig.png'] = {
      data: File.read(Rails.root.join('app/assets/images/ig.png')),
      mime_type: 'image/png'
    }
    mail to: sender_email, subject: "New assigned order for Artzibit"
  end
end
