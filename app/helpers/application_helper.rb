module ApplicationHelper
  def total_artists
    User.all.artists.count
  end

  def photographers
    User.all.photographers.count
  end

  def visual_artists
    User.all.visual_artists.count
  end

  def total_artworks
    Artwork.all.count
  end

  def navbar_white_link_active controller_name, action_name, link_name
    case controller_name
      when "artworks"
        if action_name == "index"
          link = "profile"
        else
          link = "upload"
        end
      when "users"
        link = "profile"
      when "sessions"
        link = "sign_in"
      when "registrations"
        link = "sign_up"
      else
        link = "profile"
    end
    if link_name == link
      "active"
    else
      ""
    end
  end

  def custom_error_messages resource, alert = ""
    unless resource.errors.empty?
      messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
      sentence = I18n.t('errors.messages.not_saved',
        count: resource.errors.count,
        resource: resource.class.model_name.human.downcase)
    else
      messages = alert
    end
    html = <<-HTML
    <div class="alert alert-danger">
      <span type="button" class="close" data-dismiss="alert">x</span>
      <h4>#{sentence}</h4>
      #{messages}
    </div>
    HTML

    html.html_safe
  end

  def show_form_errors(object, field_name)
    if object.errors.any?
      if !object.errors.messages[field_name].blank?
        capitalize = field_name.to_s.split(/[_ ]/).map(&:capitalize).join(" ")
        "#{capitalize} #{object.errors.messages[field_name].join(", ")}"
      end
    end
  end 


end
